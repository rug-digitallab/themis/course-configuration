rootProject.name = "configuration-manager"

pluginManagement {
    plugins {
        val digitalLabGradlePluginVersion: String by settings
        val kspPluginVersion: String by settings

        id("nl.rug.digitallab.gradle.plugin.quarkus.project") version digitalLabGradlePluginVersion
        id("com.google.devtools.ksp") version kspPluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Gradle Plugin
        gradlePluginPortal()
    }
}

include("manager")
