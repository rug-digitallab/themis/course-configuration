package nl.rug.digitallab.configuration.parsing

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.approvaltests.ApprovalTestSpec
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.configuration.config.TestResourceMappingConfig
import nl.rug.digitallab.configuration.helpers.ModelPrinter
import nl.rug.digitallab.configuration.logging.usingLogsLogger
import org.approvaltests.Approvals
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.platform.commons.annotation.Testable
import java.nio.file.Path
import kotlin.io.path.*

@QuarkusTest
class CourseParserTest {
    @Inject
    private lateinit var parser: CourseParser

    @Inject
    private lateinit var resourceMapping: TestResourceMappingConfig

    @TestFactory
    fun `Test course samples`(): List<DynamicTest> {
        val courseTestsRoot = getResource(resourceMapping.test.courses).asPath()

        // Find each course to test
        return courseTestsRoot
            .listDirectoryEntries()
            .filter { it.isDirectory() }
            .map {
                // For each test course, make a dynamic test
                CourseParseTestSpec(it, it.relativeTo(courseTestsRoot).pathString)
            }
            .map { DynamicTest.dynamicTest(it.testName) { testParseCourse(it) } }
    }

    @Testable
    private fun testParseCourse(test: CourseParseTestSpec) = usingLogsLogger {
        Assertions.assertDoesNotThrow {
            // Parse the course
            val parsedCourse = parser.parseCourse(test.coursePath)

            // Set up the approval tests: file location and simple other settings
            val approvalSpec = ApprovalTestSpec.from(test.coursePath, "parsed")

            Approvals.verify(ModelPrinter(parsedCourse).plainText, approvalSpec.toOptions())
        }
    }

    private data class CourseParseTestSpec(
        val coursePath: Path,
        val approvalsDirectory: String,
    ) {
        val testName: String =
            "Course ${coursePath.name} should be parsed correctly"
    }
}
