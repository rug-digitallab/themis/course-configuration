package nl.rug.digitallab.configuration

import arrow.optics.dsl.at
import arrow.optics.typeclasses.At
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.configuration.models.Property.Companion.invoke
import nl.rug.digitallab.configuration.models.schemas.Visibility
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Profile
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.environments
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.Duration

@QuarkusTest
class ConfigurationTest {
    @Inject
    private lateinit var configuration: Configuration

    @Test
    fun test() {
        val foo = Profile.environments
        val bar = Profile.environments().at(At.map(), "compilation")

        // Very simple, naive test to make sure it does not crash
        val course = configuration.fromFiles(getResource("tests/courses/simple").asPath())

        Assertions.assertEquals(
            Visibility.Type.NEVER,
            course()?.themis()?.folders()?.get(0)()?.assignments()?.get(0)()?.visibility()?.visibilityType()
        )
        Assertions.assertEquals(
            Duration.ofSeconds(10),
            course()?.themis()?.folders()?.get(0)()?.profiles()?.get("c")()?.environments()?.get("compilation")()?.limits()?.time()
        )
        Assertions.assertEquals(
            "container",
            course()?.themis()?.folders()?.get(0)()?.profiles()?.get("c")()?.environments()?.get("compilation")()?.type()
        )
    }
}
