package nl.rug.digitallab.configuration.inheritance.merging

import arrow.core.curried
import arrow.optics.optics
import nl.rug.digitallab.configuration.helpers.Reference
import nl.rug.digitallab.configuration.helpers.ReferenceOr
import nl.rug.digitallab.configuration.helpers.Value
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.doMergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.logging.Logger
import nl.rug.digitallab.configuration.logging.TrackingLogger
import nl.rug.digitallab.configuration.logging.entries.TracingEntry
import nl.rug.digitallab.configuration.logging.merging.RelayingMergeScope
import nl.rug.digitallab.configuration.logging.usingTrackingLogger
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class MergerTest {
    @Test
    fun `Default override merge should inherit upon null value`() = withMergeLogger {
        val spec = MergeSpec("used", null, "used")

        assertMergeSpec(overrideOrInherit(), spec)
        assertInheritLogs(1)
    }

    @Test
    fun `Default override merge should override inherited value`() = withMergeLogger {
        val spec = MergeSpec("ignored", "used", "used")

        assertMergeSpec(overrideOrInherit(), spec)
        assertOverrideLogs(1)
    }

    @Test
    fun `Custom override merge should adhere to decision function`() = withMergeLogger {
        val specInherit = MergeSpec("used", "", "used")
        val specOverride = MergeSpec("ignored", "used", "used")
        val merger = overrideOrInherit<String> { it.isNotBlank() }

        assertMergeSpec(merger, specInherit)
        assertInheritLogs(1)

        assertMergeSpec(merger, specOverride)
        assertOverrideLogs(1)
    }

    @Test
    fun `Numeric mergers should appropriately pick the minimum or maximum value`() = withMergeLogger {
        val minSpecs = listOf(
            MergeSpec(5, 3, 3),
            MergeSpec(5, 6, 5),
            MergeSpec(5, null, 5),
            MergeSpec(null, 5, 5),
        )

        val maxSpecs = listOf(
            MergeSpec(5, 3, 5),
            MergeSpec(5, 6, 6),
            MergeSpec(5, null, 5),
            MergeSpec(null, 5, 5),
        )

        minSpecs.forEach {
            assertMergeSpec(min(), it)
        }

        maxSpecs.forEach {
            assertMergeSpec(max(), it)
        }

        assertInheritLogs(4)
        assertOverrideLogs(4)
    }

    @Test
    fun `Type merge should use the merge implementation of the companion object`() = withMergeLogger {
        val spec = MergeSpec("foo"(), "bar"(), "merged"())

        assertMergeSpec(valuesOfType(MergeType), spec)
        assertMergeLogs(1)
        assertTransformLogs(1)
    }

    @Test
    fun `Merge-By-Property should transform entities appropriately`() = withMergeLogger {
        // Kotlin type resolving does not like it when we put this as a literal in the
        // list below, so we need to put this in a separate variable first.
        val customMergeByProperty: Merger<MergeType> = { doMergeByProperty<MergeType> { MergeType.name set "merged" } }

        val mergers = mapOf(
            mergeByProperty<MergeType> { MergeType.name set "merged" }
                    to MergeSpec("foo"(), "bar"(), "merged"()),

            mergeByProperty<MergeType> { MergeType.name use inherited }
                    to MergeSpec("foo"(), "bar"(), "foo"()),

            mergeByProperty<MergeType> { MergeType.name use current }
                    to MergeSpec("foo"(), "bar"(), "bar"()),

            mergeByProperty<MergeType> { MergeType.name use { current } }
                    to MergeSpec("foo"(), "bar"(), "bar"()),

            mergeByProperty<MergeType> { MergeType.name merge overrideOrInherit { it.isNotBlank() } }
                    to MergeSpec("foo"(), ""(), "foo"()),

            mergeByProperty<MergeType> { MergeType.name merge overrideOrInherit { it.isNotBlank() } }
                    to MergeSpec("foo"(), "bar"(), "bar"()),

            customMergeByProperty
                    to MergeSpec("foo"(), "bar"(), "merged"()),
        )

        mergers.forEach { (merger, spec) ->
            assertMergeSpec(merger, spec)
        }

        assertMergeLogs(7)
        assertInheritLogs(2)
        assertOverrideLogs(2)
        assertTransformLogs(2)
    }

    @Test
    fun `Merge-By-Property should track explanations`() = withMergeLogger {
        val spec = MergeSpec("foo"(), "bar"(), "merged"())

        assertMergeSpec(MergeType.merger, spec)

        Assertions.assertEquals(MergeType.mergeReason, logEntries.filterIsInstance<TracingEntry.Transform<*>>()[0].message)

        assertTransformLogs(1)
        assertMergeLogs(1)
    }

    @Test
    fun `List mergers should merge all and only identical keys`() = withMergeLogger {
        val spec = MergeSpec(
            inherited = listOf("foo"().toProp(), "bar"().toProp()),
            current =   listOf("bar"().toProp(), "foobar"().toProp()),
            expected =  listOf("merged"().toProp(), "foobar"().toProp(), "foo"().toProp()),
        )

        assertMergeSpec(listsByKeyWith(MergeType.name, valuesOfType(MergeType)), spec)
        assertMergeLogs(2) // lists were merged, as well as the "foobar" entry
        assertTransformLogs(1) // the "foobar" entry was merged, and during its merge, the name was transformed
        assertInheritLogs(1) // the "foo" entry was inherited
        assertCloneLogs(1) // the "name" property inside the inherited "foo" entry was cloned
    }

    @Test
    fun `Map mergers should merge all and only identical keys`() = withMergeLogger {
        val spec = MergeSpec(
            inherited = mapOf(
                "foo" to "foo"().toProp(),
                "bar" to "bar"().toProp(),
            ),
            current = mapOf(
                "bar" to "bar"().toProp(),
                "foobar" to "foobar"().toProp(),
            ),
            expected = mapOf(
                "foo" to "foo"().toProp(),
                "bar" to "merged"().toProp(),
                "foobar" to "foobar"().toProp(),
            )
        )

        assertMergeSpec(mapsWith(valuesOfType(MergeType)), spec)
        assertMergeLogs(2) // maps were merged, as well as the "foobar" entry
        assertTransformLogs(1) // the "foobar" entry was merged, and during its merge, the name was transformed
        assertInheritLogs(1) // the "foo" entry was inherited
        assertCloneLogs(1) // the "name" property inside the inherited "foo" entry was cloned
    }

    @Test
    fun `Map mergers should remove explicit nulls`() = withMergeLogger {
        val spec = MergeSpec(
            inherited = mapOf("foo" to "foo"().toProp(), "bar" to "bar"().toProp()),
            current = mapOf("bar" to null.toProp()),
            expected = mapOf("foo" to "foo"().toProp()),
        )

        assertMergeSpec(mapsWith(valuesOfType(MergeType)), spec)
        assertMergeLogs(1) // maps were merged
        assertInheritLogs(1) // "foo" entry was inherited
        assertCloneLogs(1) // "name" property inside "foo" entry was cloned
    }

    @Test
    fun `ReferenceOr mergers should fail on References`() = withMergeLogger {
        val specs = listOf<MergeSpec<ReferenceOr<String>>>(
            MergeSpec(Value(""), Reference(""), Value("")),
            MergeSpec(Reference(""), Value(""), Value("")),
        )

        specs.forEach {
            assertMergeSpec(referencesWith(overrideOrInherit()), it)
        }

        assertInheritLogs(1)
        assertOverrideLogs(1)
    }

    @Test
    fun `ReferenceOr mergers should merge values`() = withMergeLogger {
        val spec = MergeSpec(
            inherited = Value("foo"()),
            current = Value("bar"()),
            expected = Value("merged"()),
        )

        assertMergeSpec(referencesWith(valuesOfType(MergeType)), spec)
        assertMergeLogs(1)
        assertTransformLogs(1) // the entry was merged, and during its merge, the name was transformed
    }

    @optics
    data class MergeType(
        val name: Property<String> = "".toProp(),
    ) {
        companion object : Mergeable<MergeType> {
            val mergeReason = "Merging two entities will change the name"

            override val merger = mergeByProperty<MergeType> {
                mergeReason.asExplanation {
                    name set "merged"
                }
            }
        }
    }

    private operator fun String.invoke(): MergeType = MergeType(this.toProp())

    data class MergeSpec<out T>(val inherited: T, val current: T, val expected: T)

    private fun withMergeLogger(block: context(MergingEnvironment, TrackingLogger) () -> Unit) =
        usingTrackingLogger(block.curried()(MergingEnvironment))

    context(Logger)
    private fun <T> MergingEnvironment.runMergeSpec(merger: Merger<T>, spec: MergeSpec<T>): T {
        return RelayingMergeScope.withTracing(spec.inherited.toProp(), spec.current.toProp(), merger)
    }

    context(Logger)
    private fun <T> MergingEnvironment.assertMergeSpec(merger: Merger<T>, spec: MergeSpec<T>)
         = assertMergeSpec(Assertions::assertEquals, merger, spec)

    context(Logger)
    private fun <T> MergingEnvironment.assertMergeSpec(assertion: (Any?, Any?) -> Unit, merger: Merger<T>, spec: MergeSpec<T>) {
        assertion(spec.expected, runMergeSpec(merger, spec))
    }
}
