package nl.rug.digitallab.configuration.helpers

import io.exoquery.pprint.PPrinter
import io.exoquery.pprint.PPrinterConfig
import io.exoquery.pprint.Tree
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.Property

/**
 * Simple [PPrinter] customisation for printing any configuration models
 * during testing. In particular, this makes sure [EntityPath] instances
 * are handled properly.
 */
object ModelPrinter : PPrinter(PPrinterConfig(defaultHeight = 5000)) {
    override fun treeify(x: Any?, elementName: String?, escapeUnicode: Boolean, showFieldNames: Boolean): Tree {
        return when(x) {
            is EntityPath -> Tree.Literal(x.toString())
            is Property<*> -> treeify(x.get(), elementName, escapeUnicode, showFieldNames)
            else -> super.treeify(x, elementName, escapeUnicode, showFieldNames)
        }
    }
}
