package nl.rug.digitallab.configuration.parsing

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.kotlin.quantities.*
import nl.rug.digitallab.configuration.config.TestResourceMappingConfig
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.RuntimeFile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import java.time.Instant
import java.util.stream.Stream

@QuarkusTest
class DeserializerTests {
    @Inject
    private lateinit var yamlMapper: YAMLMapper

    @Inject
    private lateinit var resourceMapping: TestResourceMappingConfig

    @TestFactory
    fun `Parsing datetime formats to Instants`(): Stream<DynamicTest> =
        Stream.of(
            "date-time/valid-custom-minute.json" to Instant.parse("2021-12-31T23:01:00Z"),
            "date-time/valid-custom-second.json" to Instant.parse("2021-12-31T23:00:01Z"),
            "date-time/valid-rfc-+2.json"        to Instant.parse("2021-12-31T22:00:00Z"),
            "date-time/valid-rfc--3.15.json"     to Instant.parse("2022-01-01T03:15:00Z"),
            "date-time/valid-rfc-utc.json"       to Instant.parse("2022-01-01T00:00:00Z"),
        )
        .map {
            DynamicTest.dynamicTest("Datetime in ${it.first.substringAfterLast("/")} should equal ${it.second}") {
                val testPath = getResource(resourceMapping.test.schemas).asPath().resolve(it.first)
                val parsedDate = yamlMapper.readValue(testPath.toFile(), Instant::class.java)
                Assertions.assertEquals(it.second, parsedDate)
            }
        }

    @TestFactory
    fun `Parsing file representations to Files`(): Stream<DynamicTest> =
        Stream.of(
            "themis/runtime/file/valid-complete.json"    to "\$some-reference",
            "themis/runtime/file/valid-defaultpath.json" to "\$some-reference",
            "themis/runtime/file/valid-reference.json"   to "\$some-reference",
        )
        .map {
            DynamicTest.dynamicTest("File representation in ${it.first.substringAfterLast("/")} should be ${it.second}") {
                val testPath = getResource(resourceMapping.test.schemas).asPath().resolve(it.first)
                val parsedFile = yamlMapper.readValue(testPath.toFile(), RuntimeFile::class.java)
                Assertions.assertEquals(it.second, parsedFile.source())
            }
        }

    @TestFactory
    fun `Parsing file sizes to ByteSizes`(): Stream<DynamicTest> =
        Stream.of(
            "themis/runtime/filesize/valid-string-iec.json"         to 100.MiB,
            "themis/runtime/filesize/valid-string-separators.json"  to 100_000_000.B,
            "themis/runtime/filesize/valid-string-si.json"          to 100.KB,
            "themis/runtime/filesize/valid-string-unix.json"        to 100.MB,
        )
        .map {
            DynamicTest.dynamicTest("Size representation in ${it.first.substringAfterLast("/")} should be ${it.second}") {
                val testPath = getResource(resourceMapping.test.schemas).asPath().resolve(it.first)
                val parsedFile = yamlMapper.readValue(testPath.toFile(), ByteSize::class.java)
                Assertions.assertEquals(it.second, parsedFile)
            }
        }
}
