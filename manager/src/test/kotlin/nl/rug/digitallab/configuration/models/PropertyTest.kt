package nl.rug.digitallab.configuration.models

import arrow.optics.Iso
import arrow.optics.dsl.every
import arrow.optics.dsl.notNull
import arrow.optics.optics
import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.configuration.helpers.get
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.Property.Companion.unsafeCast
import nl.rug.digitallab.configuration.models.Property.Companion.with
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class PropertyTest {
    @Test
    fun `Two fresh properties should have different identities`() {
        val first = null.toProp()
        val second = null.toProp()

        Assertions.assertNotEquals(first.identity, second.identity)
    }

    @Test
    fun `Setting a property value does not update in-place but keeps identity`() {
        val prop = "foo".toProp()
        val bar = prop.set("bar")

        Assertions.assertNotEquals(prop.get(), bar.get())
        Assertions.assertEquals(prop.identity, bar.identity)
    }

    @Test
    fun `Property equality is determined by their contents`() {
        val first = "foo".toProp()
        val second = "foo".toProp()

        Assertions.assertNotEquals(first.identity, second.identity)
        Assertions.assertEquals(first, second)
    }

    @Test
    fun `Properties can safely be cast to other types`() {
        // Since Property has <out T>, we can always cast to a supertype safely
        // natively in Kotlin. So Property<T> can be cast to Property<T?>.
        // But the other way is unsafe.
        val foo: Property<String?> = "foo".toProp()
        val invalidCast: Property<Number>? = foo.unsafeCast()
        val validCast: Property<String>? = foo.unsafeCast()

        Assertions.assertNull(invalidCast)
        Assertions.assertNotNull(validCast)
        Assertions.assertEquals(foo, validCast)
    }

    @Test
    fun `Lenses can be applied inside properties`() {
        // Get a lens from anything to Property<TestType>, we choose Id() here to keep it simple
        // This serves as the base lens, on top of which the lifted lens will be applied. So
        // this lens from Property<TT> to Property<TT> will be augmented with the property lens,
        // yielding a Lens from Property<TT> to Property<String>
        val selfLens = Iso.id<Property<TestType>>()
        val lens = selfLens.with(TestType.string)

        // Deduplication of test values
        val string = "foo"
        val newString = "new"

        // Instance to perform our tests on
        val testType = TestType(string, null, emptyList())
        val newTestType = testType.copy(string = newString)

        val property = testType.toProp()

        // Ensure reads and writes work properly
        Assertions.assertEquals(string, property[lens].get())
        Assertions.assertEquals(newTestType, lens.set(property, newString.toProp()).get())
    }

    @Test
    fun `Optionals can be applied inside properties`() {
        // Get a lens from anything to Property<TestType>, we choose Id() here to keep it simple
        // This serves as the base lens, on top of which the lifted optional will be applied. So
        // this lens from Property<TT> to Property<TT> will be augmented with the property optional,
        // yielding an Optional from Property<TT> to Property<String>
        val selfLens = Iso.id<Property<TestType>>()
        val optional = selfLens.with(TestType.nullable.notNull)

        // Deduplication of test values. We need to test both null and non-null cases
        val nonNull = "nonNull"
        val new = "new"

        // Instance to perform our tests on
        val testTypeNull = TestType("", null, emptyList())
        val testTypeNonNull = TestType("", nonNull, emptyList())
        val newTestType = testTypeNonNull.copy(nullable = new)
        val propertyNull = testTypeNull.toProp()
        val propertyNonNull = testTypeNonNull.toProp()

        // Ensure reads work properly - they should read a null when not present
        Assertions.assertNull(propertyNull[optional])
        Assertions.assertEquals(nonNull, propertyNonNull[optional]?.get())

        // Three write tests to make sure the writes work as expected
        // Nulls should not be written to
        Assertions.assertEquals(testTypeNull, optional.set(propertyNull, new.toProp()).get())
        // Non-nulls should be written to
        Assertions.assertEquals(newTestType, optional.set(propertyNonNull, new.toProp()).get())
    }

    @Test
    fun `Traversals can be applied inside properties`() {
        // Get a lens from anything to Property<TestType>, we choose Id() here to keep it simple
        // This serves as the base lens, on top of which the lifted lens will be applied. So
        // this lens from Property<TT> to Property<TT> will be augmented with the property traversal,
        // yielding a Traversal from Property<TT> to Property<String>
        val selfLens = Iso.id<Property<TestType>>()
        val traversal = selfLens.with(TestType.list.every)

        // Deduplication of test values
        val list = listOf("bar", "foobar")
        val newList = listOf("new", "new")
        val augmentedList = listOf("bar2", "foobar2")

        // Instance to perform our tests on
        val testType = TestType("", null, list)
        val property = testType.toProp()

        // Ensure reads property retrieve all instances in the list
        Assertions.assertEquals(list, property[traversal].map { it.get() })

        // Three write tests to make sure the writes work as expected
        Assertions.assertEquals(newList, traversal.set(property, "new".toProp()).get().list)
        Assertions.assertEquals(augmentedList, traversal.modify(property) { it.map { s -> s + "2" } }.get().list)
    }

    @optics
    data class TestType(
        val string: String,
        val nullable: String?,
        val list: List<String>,
    ) {
        companion object
    }
}
