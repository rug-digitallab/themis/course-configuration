package nl.rug.digitallab.configuration.logging

import nl.rug.digitallab.configuration.logging.Logger.Type
import nl.rug.digitallab.configuration.logging.entries.LogEntry
import org.jboss.logging.Logger

/**
 * Simple [ConfigurationLogger] implementation that directly relays all
 * log messages to JVM-native logging in [Logger].
 */
class LogsLogger : ConfigurationLogger {
    override fun log(entry: LogEntry<*>) {
        Logger.getLogger(ConfigurationLogger::class.java, entry.location.toString()).logv(
            when(entry.type) {
                Type.ERROR -> Logger.Level.ERROR
                Type.WARNING -> Logger.Level.WARN
                Type.DEBUG -> Logger.Level.DEBUG
                Type.EXPLAIN -> Logger.Level.INFO
            },
            entry.message,
            arrayOf(entry)
        )
    }
}

/**
 * Helper function to quickly instantiate a [LogsLogger] and make
 * it contextually available in the [block]. In the future, this
 * might no longer be necessary if it becomes possible to use
 * scoped properties:
 * https://github.com/Kotlin/KEEP/blob/master/proposals/context-receivers.md#scope-properties
 */
fun usingLogsLogger(block: context(ConfigurationLogger) () -> Unit) {
    val logger = LogsLogger()

    // We should be able to wrap this call in a with() block, but that
    // scenario seems to be bugged in Kotlin atm. (KT-60953)
    // https://youtrack.jetbrains.com/issue/KT-60953/Context-receivers-are-required-to-be-passed-as-an-explicit-argument-in-some-scenarios
    block(logger)
}
