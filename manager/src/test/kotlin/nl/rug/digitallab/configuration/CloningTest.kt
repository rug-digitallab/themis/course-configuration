package nl.rug.digitallab.configuration

import com.fasterxml.jackson.annotation.JsonIgnore
import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.configuration.Cloning.clone
import nl.rug.digitallab.configuration.logging.usingLogsLogger
import nl.rug.digitallab.configuration.logging.usingTrackingLogger
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.ThrowingSupplier
import java.math.BigDecimal

@QuarkusTest
class CloningTest {
    @Test
    fun `Cloning will produce new instances`() = usingLogsLogger {
        data class Foo(val name: String)

        val instance = Foo("Hello, World")
        val clone = instance.clone()

        Assertions.assertEquals(instance, clone)
        Assertions.assertNotSame(instance, clone)
    }

    @Test
    fun `Cloning will perform a deep clone`() = usingLogsLogger {
        data class Inner(val name: String)
        data class Outer(val name: String, val inner: Inner)

        val instance = Outer("foo", Inner("bar"))
        val clone = instance.clone()

        Assertions.assertEquals(instance, clone)
        Assertions.assertNotSame(instance, clone)
        Assertions.assertEquals(instance.inner, clone.inner)
        Assertions.assertNotSame(instance.inner, clone.inner)
    }

    @Test
    fun `Cloning will maintain a consistent BigDecimal representation`() = usingLogsLogger {
        val instance = BigDecimal.TEN
        val clone = instance.clone()

        Assertions.assertTrue(instance == clone)
        Assertions.assertTrue(instance.compareTo(clone) == 0)
    }

    @Test
    fun `Cloning will not persist Property identities`() = usingLogsLogger {
        val instance = "foo".toProp()
        val clone = instance.clone()

        Assertions.assertEquals(instance(), clone())
        Assertions.assertNotEquals(instance.identity, clone.identity)
    }

    @Test
    fun `Cloning entities with properties will create log entries`() = usingTrackingLogger {
        data class Foo(val name: Property<String>)

        val instance = Foo("foo".toProp())
        val clone = instance.clone()

        Assertions.assertEquals(instance.name(), clone.name())
        Assertions.assertNotEquals(instance.name.identity, clone.name.identity)
        Assertions.assertEquals(instance, clone)
        assertCloneLogs(1)
    }

    @Test
    fun `Cloning will ignore any annotations on entities`() = usingLogsLogger {
        class Foo(@JsonIgnore val ignored: String = "default")

        val instance = Foo("different")
        val clone = instance.clone()

        Assertions.assertEquals(instance.ignored, clone.ignored)
    }

    @Test
    fun `Cloning will also clone private properties`() = usingLogsLogger {
        class Foo(private val ignored: String = "default") {
            fun public(): String = ignored
        }

        val instance = Foo("different")
        val clone = instance.clone()

        Assertions.assertEquals(instance.public(), clone.public())
    }

    @Test
    fun `Cloning will ignore getter-only properties`() = usingLogsLogger {
        class Foo(val foo: String) {
            val bar get() = foo
        }

        val instance = Foo("foo")
        val clone = Assertions.assertDoesNotThrow(ThrowingSupplier { instance.clone() })

        Assertions.assertEquals(clone.foo, clone.bar)
    }
}
