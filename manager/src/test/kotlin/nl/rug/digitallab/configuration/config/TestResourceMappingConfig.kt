package nl.rug.digitallab.configuration.config

import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithName

@ConfigMapping(prefix = "resources")
interface TestResourceMappingConfig : ResourceMappingConfig {
    @get:WithName("test")
    val test: TestMapping

    interface TestMapping {
        @get:WithName("schemas")
        val schemas: String

        @get:WithName("courses")
        val courses: String
    }
}