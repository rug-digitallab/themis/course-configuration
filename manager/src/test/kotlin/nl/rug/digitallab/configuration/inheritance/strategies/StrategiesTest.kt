package nl.rug.digitallab.configuration.inheritance.strategies

import arrow.optics.Optional
import arrow.optics.dsl.index
import arrow.optics.optics
import nl.rug.digitallab.configuration.Cloning.clone
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.valuesOfType
import nl.rug.digitallab.configuration.inheritance.traversal
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.entries.SimpleLogEntry
import nl.rug.digitallab.configuration.logging.entries.StrategyLogEntry
import nl.rug.digitallab.configuration.logging.strategies.StrategyLogger
import nl.rug.digitallab.configuration.logging.usingLogsLogger
import nl.rug.digitallab.configuration.logging.usingTrackingLogger
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.invoke
import nl.rug.digitallab.configuration.models.Property.Companion.notNull
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.Property.Companion.value
import nl.rug.digitallab.configuration.models.div
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class StrategiesTest {
    @Test
    fun `Strategies should not modify objects when not required`() = usingLogsLogger {
        val source = "upper" { - "lower" }
        val expected = source.clone()

        val result = TestStrategy().test(source)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `Strategies should propagate logs`() = usingTrackingLogger {
        val source = "" {}

        TestStrategy().test(source)

        Assertions.assertEquals(3 + 2, logEntries.size)
        Assertions.assertTrue(logEntries.filterIsInstance<StrategyLogEntry<TraversalType, *>>().any { it.finalValue == source() })
        assertLogsOfType<StrategyLogEntry<TraversalType, *>>(3)
        assertLogsOfType<SimpleLogEntry<*>>(2)
    }

    @Test
    fun `Overriding Strategy should carry values down the tree`() = usingLogsLogger {
        val source = "upper" { - "" }
        val expected = "upper" { - "upper" }

        val result = traversalForProperty(TraversalType.name).withOverride(
            "the name is not empty",
            "the name is empty",
        ) { it.isNotBlank() }.apply(source)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `Overriding Strategy should not incorrectly override values`() = usingLogsLogger {
        val source = "upper" { - "lower" }
        val expected = source.clone()

        val result = traversalForProperty(TraversalType.name).withOverride(
            "the name is not empty",
            "the name is empty",
        ) { it.isNotBlank() }.apply(source)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `Overriding Strategy should carry down new values further`() = usingLogsLogger {
        val source =
            "upper" {
                - "middle" {
                    - ""
                }
            }
        val expected =
            "upper" {
                - "middle" {
                    - "middle"
                }
            }

        val result = traversalForProperty(TraversalType.name).withOverride(
            "the name is not empty",
            "the name is empty",
        ) { it.isNotBlank() }.apply(source)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `Overriding Strategy should not cross branches`() = usingLogsLogger {
        val source =
            "upper" {
                - "first" {
                    - ""
                }
                - "second" {
                    - ""
                }
                - ""
            }
        val expected =
            "upper" {
                - "first" {
                    - "first"
                }
                - "second" {
                    - "second"
                }
                - "upper"
            }

        val result = traversalForProperty(TraversalType.name).withOverride(
            "the name is not empty",
            "the name is empty",
        ) { it.isNotBlank() }.apply(source)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `Merging Strategy should merge values properly`() = usingLogsLogger {
        val source =
            "" {
                ! EntityPath("foo")
                - "" {
                    ! EntityPath("bar")
                }
            }
        val expected =
            "" {
                ! EntityPath("foo")
                - "" {
                    ! ("foo" / "bar")
                }
            }

        val result = traversalForProperty(TraversalType.path).withMerge { valuesOfType(EntityPath) }.apply(source)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `Validation Strategy should accurately validate units`() = usingLogsLogger {
        val invalidSource = "" {}
        val validSource = "foo" {}

        val invalidResult = traversalForProperty(TraversalType.name).withValidation { it.isNotBlank() }.validate(invalidSource)
        val validResult = traversalForProperty(TraversalType.name).withValidation { it.isNotBlank() }.validate(validSource)
        Assertions.assertFalse(invalidResult)
        Assertions.assertTrue(validResult)
    }

    @Test
    fun `Validation Strategy should accurately validate trees`() = usingLogsLogger {
        val invalidSource =
            "valid" {
                - "" // invalid here
                - "valid"
            }
        val validSource =
            "valid" {
                - "valid"
                - "valid"
            }

        val invalidResult = traversalForProperty(TraversalType.name).withValidation { it.isNotBlank() }.validate(invalidSource)
        val validResult = traversalForProperty(TraversalType.name).withValidation { it.isNotBlank() }.validate(validSource)
        Assertions.assertFalse(invalidResult)
        Assertions.assertTrue(validResult)
    }

    @Test
    fun `Visiting Strategy should propagate changed values properly`() = usingLogsLogger {
        val source = "former" { - "also former" }
        val expected = "new" { - "new" }

        val result = traversalForProperty(TraversalType.name).withVisit { "new" }.apply(source)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `Strategies should not visit non-existing targets`() = usingLogsLogger {
        val source = "foo" {}

        val result = traversalForProperty(TraversalType.more.value.index(0).value.name).withVisit { "new" }.apply(source)
        Assertions.assertEquals(source, result)
    }

    @Test
    fun `Strategies should not confuse nulls and non-existing targets`() = usingLogsLogger {
        val shallowNull = "foo" { + null }
        val shallowNonNull = "foo" { + "new" }

        val deepNull = "foo" { - "item" { + null } }
        val deepNonNull = "foo" { - "item" { + "new" }}

        // Visiting a nullable property should allow us to write a value to make it non-null
        Assertions.assertEquals(shallowNonNull, traversalForProperty(TraversalType.nullable).withVisit { "new" }.apply(shallowNull))
        // but visiting a nullable property as optional should skip visiting altogether, and not allow us to write since now
        // the property is considered to be nonexisting
        Assertions.assertEquals(shallowNull, traversalForProperty(TraversalType.nullable.notNull).withVisit { "new" }.apply(shallowNull))
        // Furthermore, visiting the nullable property should allow us to set a null value
        Assertions.assertEquals(shallowNull, traversalForProperty(TraversalType.nullable).withVisit { null }.apply(shallowNonNull))

        // These same properties should all hold when the target property is nested deeper in
        // the structure, such that you get an optional lens with nullable focus.
        Assertions.assertEquals(deepNonNull, traversalForProperty(TraversalType.more().index(0).value.nullable).withVisit { "new" }.apply(deepNull))
        Assertions.assertEquals(deepNull, traversalForProperty(TraversalType.more.value.index(0).value.nullable.notNull).withVisit { "new" }.apply(deepNull))
        Assertions.assertEquals(deepNull, traversalForProperty(TraversalType.more().index(0).value.nullable).withVisit { null }.apply(deepNonNull))
    }

    private fun <P> traversalForProperty(property: Optional<TraversalType, Property<P>>)
        = traversal {
            inspect(property)
            visitSelf(TraversalType.more)
        }

    /**
     * This is a private extension operator method on String, that allows us to "invoke" a string
     * with a lambda function (so << "foo" { ... } >>), which is a shorthand for instantiating
     * [TraversalType] trees quickly. It invokes its [TraversalType.Builder] for furher work.
     */
    private operator fun String.invoke(buildFunction: TraversalType.Builder.() -> Unit): Property<TraversalType>
        = TraversalType.Builder.build(this, buildFunction).toProp()

    @optics
    data class TraversalType (
        val name: Property<String> = "".toProp(),
        val more: ListProperty<TraversalType> = Property(emptyList()),
        val nullable: Property<String?> = null.toProp(),

        override val path: Property<EntityPath> = EntityPath("").toProp(),
    ) : ITraversable {
        override fun toNodeString(): String = "TraversalType \"$name\""

        companion object

        // We define a builder for this simple traversal object type, to simplify the syntax
        // and clarify the structure of the tree we build.
        // The rough idea is that in the builder, lines with ! indicate the path, and lines
        // with - indicate the "more items" list entries.
        @DslMarker annotation class TestBuilderMarker

        @TestBuilderMarker
        class Builder {
            // Keep track of the local instance
            var instance: TraversalType = TraversalType()

            /**
             * Set the path of the current object, implemented
             * as a unary not on a Path instance.
             */
            operator fun EntityPath.not() { instance = instance.copy(path = this.toProp()) }

            /**
             * Add an item to the current object, represented by just
             * its name in a string. We implement a unary minus on such
             * a String to easily add it to the list of items.
             */
            operator fun String.unaryMinus() {
                instance = instance.copy(more = (instance.more() + TraversalType(this.toProp()).toProp()).toProp())
            }

            /**
             * Set the current string as the [nullable] property
             */
            operator fun String?.unaryPlus() {
                instance = instance.copy(nullable = this.toProp())
            }

            /**
             * Add an item to the current object, implemented as a unary
             * minus on the [TraversalType] itself - this instance will be
             * obtained from the [String.invoke] extension, so that we can write
             * - "some string" { ... }
             * This will first try to invoke the string with the lambda, and
             * its result will then be passed to the unary minus. So, this
             * is equivalent to
             * "some string".invoke({ ... }).unaryMinus()
             */
            operator fun TraversalType.unaryMinus() {
                instance = instance.copy(more = (instance.more() + this.toProp()).toProp())
            }

            /**
             * Create a new [TraversalType] instance with the current string
             * as its name, implemented as an "invoke" of a String with a
             * lambda as its single parameter.
             */
            operator fun String.invoke(buildFunction: Builder.() -> Unit): TraversalType
                = build(this, buildFunction)

            companion object {
                /**
                 * Create an instance of [TraversalType] with the specified
                 * name and build function.
                 */
                fun build(name: String, buildFunction: Builder.() -> Unit): TraversalType {
                    val builder = Builder()

                    // Build
                    builder.instance = builder.instance.copy(name = name.toProp())
                    builder.buildFunction()

                    return builder.instance
                }
            }
        }
    }

    class TestStrategy : Strategy<TraversalType, String, TestStrategy.Accumulator>() {
        override val strategyName: String get() = TestStrategy::class.simpleName.orEmpty()
        override val traversal = traversal { inspect(TraversalType.name); visitSelf(TraversalType.more) }

        context(StrategyLogger<String>)
        override fun visit(value: String, accumulator: Accumulator): String {
            strategyError("Error message")
            strategyDebug("Debug message")
            strategyWarning("Warning message")

            return value
        }

        context(ConfigurationLogger)
        fun test(source: Property<TraversalType>): Property<TraversalType> = execute(source, Accumulator())

        data class Accumulator(val const: String = "some") : Strategy.Accumulator<Accumulator, String>() {
            override fun fork(): Accumulator = copy()
            override fun merge(child: Accumulator) { /* no-op */ }
        }
    }
}
