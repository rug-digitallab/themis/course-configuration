package nl.rug.digitallab.configuration.inheritance

import arrow.optics.PIso
import arrow.optics.copy
import arrow.optics.optics
import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.configuration.helpers.Reference
import nl.rug.digitallab.configuration.helpers.ReferenceOr
import nl.rug.digitallab.configuration.helpers.Value
import nl.rug.digitallab.configuration.helpers.get
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.MapProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.filterNotNull
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.Property.Companion.set
import nl.rug.digitallab.configuration.models.Property.Companion.value
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@Suppress("UNCHECKED_CAST")
@QuarkusTest
class TraversalTest {
    @Test
    fun `Builder should use Iso for the root`() {
        val traversal = traversal<TraversalType, Nothing> {}
        val instance = TraversalType().toProp()

        Assertions.assertInstanceOf(PIso::class.java, traversal.items)
        Assertions.assertEquals(listOf(instance), instance[traversal.items])
    }

    @Test
    fun `Builder should take the correct property`() {
        val traversal = traversal {
            inspect(TraversalType.name)
        } as TraversalStep<Property<TraversalType>, TraversalType, String>

        val instance = TraversalType("test".toProp())

        Assertions.assertNotNull(traversal.property)
        Assertions.assertEquals(instance.name, instance[traversal.property!!])
        Assertions.assertEquals(0, traversal.nextSteps.size)
    }

    @Test
    fun `Builder should accept lack of property`() {
        val traversal = traversal<TraversalType, Nothing> {}
        Assertions.assertEquals(null, traversal.property)
        Assertions.assertEquals(0, traversal.nextSteps.size)
    }

    @Test
    fun `Builder should allow for nullable properties`() {
        val nullableTraversal = traversal {
            inspect(TraversalType.nullable)
        } as TraversalStep<Property<TraversalType>, TraversalType, String?>

        val nullInstance = TraversalType()
        val valInstance = TraversalType(nullable = "foo".toProp())

        Assertions.assertNotNull(nullableTraversal.property)
        Assertions.assertEquals(null, nullInstance[nullableTraversal.property!!.value])
        Assertions.assertEquals("foo", valInstance[nullableTraversal.property!!.value])
        Assertions.assertEquals(valInstance, nullInstance.copy { nullableTraversal.property!! set "foo" })
    }

    @Test
    fun `Builder should correctly visit lists`() {
        val traversal = traversal {
            inspect(TraversalType.name)
            visit(TraversalType.more) {
                inspect(TraversalType.name)
            }
        } as TraversalStep<Property<TraversalType>, TraversalType, String>

        val instance = TraversalType(
            name = "outer".toProp(),
            more = listOf(TraversalType(name = "inner".toProp()).toProp()).toProp()
        )

        Assertions.assertEquals(1, traversal.nextSteps.size)
        Assertions.assertIterableEquals(instance.more(), instance[traversal.nextSteps[0].items])
    }

    @Test
    fun `Builder should correctly visit maps`() {
        val traversal = traversal {
            inspect(TraversalType.name)
            visit(TraversalType.moreMap) {
                inspect(TraversalType.name)
            }
        } as TraversalStep<Property<TraversalType>, TraversalType, String>

        val instance = TraversalType(
            name = "outer".toProp(),
            moreMap = mapOf("foo" to TraversalType(name = "inner".toProp()).toProp()).toProp()
        )

        Assertions.assertEquals(1, traversal.nextSteps.size)
        Assertions.assertIterableEquals(instance.moreMap().values, instance[traversal.nextSteps[0].items])
    }

    @Test
    fun `Builder should correctly visit maps with nulls`() {
        val traversal = traversal {
            inspect(TraversalType.name)
            visit(TraversalType.moreMapNulls) {
                inspect(TraversalType.name)
            }
        } as TraversalStep<Property<TraversalType>, TraversalType, String>

        val instance = TraversalType(
            name = "outer".toProp(),
            moreMapNulls = mapOf("foo" to null.toProp(), "bar" to TraversalType(name = "inner".toProp()).toProp()).toProp()
        )

        Assertions.assertEquals(1, traversal.nextSteps.size)
        Assertions.assertIterableEquals(instance.moreMapNulls().values.filterNotNull(), instance[traversal.nextSteps[0].items])
    }

    @Test
    fun `Builder should correctly visit references`() {
        val traversal = traversal {
            inspect(TraversalType.name)
            visit(TraversalType.moreRef) {
                inspect(TraversalType.name)
            }
        } as TraversalStep<Property<TraversalType>, TraversalType, String>

        val referenceInstance = TraversalType(name = "outer".toProp(), moreRef = Reference<TraversalType>("ref").toProp())
        val valueInstance = TraversalType(name = "outer".toProp(), moreRef = Value(TraversalType(name = "inner".toProp())).toProp())

        Assertions.assertEquals(1, traversal.nextSteps.size)
        Assertions.assertEquals(emptyList<TraversalType>(), referenceInstance[traversal.nextSteps[0].items])
        Assertions.assertEquals(listOf(TraversalType(name = "inner".toProp()).toProp()), valueInstance[traversal.nextSteps[0].items])
    }

    @Test
    fun `Builder should correctly self-reference`() {
        val traversal = traversal {
            inspect(TraversalType.name)
            visitSelf(TraversalType.more)
        } as TraversalStep<Property<TraversalType>, TraversalType, String>

        val instance = TraversalType(
            name = "outer".toProp(),
            more = listOf(
                TraversalType(
                    name = "middle".toProp(),
                    more = listOf(TraversalType(name = "inner".toProp()).toProp()).toProp()
                ).toProp()
            ).toProp()
        ).toProp()

        Assertions.assertInstanceOf(PIso::class.java, traversal.items)
        Assertions.assertIterableEquals(listOf(instance), instance[traversal.items])
        Assertions.assertEquals("outer", instance()[traversal.property!!.value])

        Assertions.assertEquals(1, traversal.nextSteps.size)
        Assertions.assertIterableEquals(instance().more(), instance()[traversal.nextSteps[0].items])

        Assertions.assertEquals(1, traversal.nextSteps[0].nextSteps.size)
        Assertions.assertSame(traversal.nextSteps[0], traversal.nextSteps[0].nextSteps[0])
    }

    @optics
    data class TraversalType (
        val name: Property<String> = "".toProp(),
        val more: ListProperty<TraversalType> = Property(emptyList()),
        val moreMap: Property<Map<String, Property<TraversalType>>> = Property(emptyMap()),
        val moreMapNulls: MapProperty<TraversalType> = Property(emptyMap()),
        val moreRef: Property<ReferenceOr<TraversalType>> = Property(Reference("")),
        val nullable: Property<String?> = null.toProp(),

        override val path: Property<EntityPath> = EntityPath("").toProp(),
    ) : ITraversable {
        override fun toNodeString(): String = "TraversalType \"$name\""

        companion object
    }
}
