package nl.rug.digitallab.configuration.parsing

import com.google.common.reflect.ClassPath
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.approvaltests.ApprovalTestSpec
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.config.TestResourceMappingConfig
import nl.rug.digitallab.configuration.helpers.ModelPrinter
import nl.rug.digitallab.configuration.logging.deserialization.DeserializationLogger
import nl.rug.digitallab.configuration.logging.deserialization.RelayingDeserializationLogger
import nl.rug.digitallab.configuration.logging.usingLogsLogger
import org.approvaltests.Approvals
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.platform.commons.annotation.Testable
import java.io.IOException
import java.nio.file.Path
import kotlin.io.path.extension
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation

@QuarkusTest
class EntityParserTest {
    @Inject
    private lateinit var parser: EntityParser

    @Inject
    private lateinit var resourceMapping: TestResourceMappingConfig

    @Inject
    private lateinit var logger: Logger

    private val acceptedExtensions = listOf("json", "yaml", "yml")

    @TestFactory
    fun `Test Samples`(): List<DynamicTest> {
        val schemaTestsRoot = getResource(resourceMapping.test.schemas).asPath()

        // Find the entity types we want to test: all models that are annotated with the
        // @JsonSchema annotation.
        val testTypes = ClassPath
            .from(Thread.currentThread().contextClassLoader)
            .getTopLevelClassesRecursive("nl.rug.digitallab.configuration.models")
            // While "it.load()" is also possible, this will use a different classloader than
            // the one specified above, and thus it is impossible to retrieve the annotation
            // data since the two JsonSchema annotations are "different types"
            .map { Class.forName(it.name).kotlin }
            .filter { it.hasAnnotation<JsonSchema>() }

        // For each test type, we find the schema name and the accompanying tests
        return testTypes
            .associateBy { it.findAnnotation<JsonSchema>()!!.ref }
            .flatMap { (schema, type) ->
                // Try to find the "test folder" corresponding to the schema path
                try {
                    // The schema name is relative to the schemas root. Every schema
                    // "foo/bar/foobar.schema.json" will have tests in a folder
                    // "foo/bar/foobar/", relative to the tests root.
                    val schemaTestPath = schema.substringBeforeLast(".schema.json")

                    schemaTestsRoot.resolve(schemaTestPath)
                        .listDirectoryEntries()
                        .filter { it.extension in acceptedExtensions }
                        .filter { it.name.startsWith("valid") }

                        // Transform all test files into a test spec
                        .map { ParsingTestSpec(type, it) }
                } catch (_: IOException) {
                    logger.warn("Could not find any tests for type ${type.qualifiedName}")

                    emptyList()
                }
            }
            .map { DynamicTest.dynamicTest(it.testName) { testSamples(it) } }
    }

    @Testable // To help Approval Tests find the testing class
    private fun testSamples(test: ParsingTestSpec) {
        Assertions.assertDoesNotThrow { usingDeserializationLogger {
            // Parse the entity
            val parsedEntity = parser.parseAndValidateFile(test.sampleFile, test.type)

            val approvalSpec = ApprovalTestSpec.from(test.sampleFile, "parsed")
            Approvals.verify(ModelPrinter(parsedEntity).plainText, approvalSpec.toOptions())
        }}
    }

    private fun usingDeserializationLogger(block: context(DeserializationLogger) () -> Unit) = usingLogsLogger {
        block(object : DeserializationLogger by RelayingDeserializationLogger() {})
    }

    private data class ParsingTestSpec(
        val type: KClass<out Any>,
        val sampleFile: Path,
    ) {
        val testName: String =
            "Sample ${sampleFile.name} should parse to type ${type.simpleName} correctly"
    }
}
