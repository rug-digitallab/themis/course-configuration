package nl.rug.digitallab.configuration.schemas

import com.fasterxml.jackson.databind.ObjectMapper
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.SpecVersionDetector
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.configuration.config.TestResourceMappingConfig
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import java.net.URI
import java.nio.file.Path
import java.util.stream.Stream
import kotlin.io.path.*
import kotlin.streams.asStream

@QuarkusTest
class SchemaValidityTest {
    @Inject
    private lateinit var resourceMapping: TestResourceMappingConfig

    @OptIn(ExperimentalPathApi::class)
    @TestFactory
    fun `test schema validity`(): Stream<DynamicTest> {
        val schemaRoot = getResource(resourceMapping.schemas).asPath()

        return schemaRoot
            .walk()
            .filter { it.name.endsWith(".schema.json") }
            .map {
                DynamicTest.dynamicTest("schema ${it.relativeTo(schemaRoot).pathString} should be valid") {
                    testSchema(it)
                }
            }
            .asStream()
    }

    private fun testSchema(schemaFile: Path) {
        // Obtain an instance of the schema to validate and its metaschema URI
        val schema = ObjectMapper().readTree(schemaFile.toFile())
        val metaSchemaUri = schema.get("\$schema").asText()

        // Obtain an instance of the metaschema
        val metaSchema = JsonSchemaFactory.getInstance(SpecVersionDetector.detect(schema))
            .getSchema(URI(metaSchemaUri))

        // Perform validation of the schema
        val result = metaSchema.validate(schema)

        // Expect no errors
        Assertions.assertEquals(0, result.size, result.toString())
    }
}
