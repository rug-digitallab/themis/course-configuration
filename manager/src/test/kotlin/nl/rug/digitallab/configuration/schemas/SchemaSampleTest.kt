package nl.rug.digitallab.configuration.schemas

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.SchemaValidatorsConfig
import com.networknt.schema.SpecVersionDetector
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.approvaltests.ApprovalTestSpec
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.configuration.config.TestResourceMappingConfig
import nl.rug.digitallab.configuration.jackson.SchemaFactoryCustomizer
import org.approvaltests.Approvals
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.platform.commons.annotation.Testable
import java.io.IOException
import java.nio.file.Path
import java.util.stream.Stream
import kotlin.io.path.*
import kotlin.streams.asStream

@QuarkusTest
class SchemaSampleTest {
    @Inject
    private lateinit var schemaFactoryCustomizer: SchemaFactoryCustomizer

    @Inject
    private lateinit var schemaValidatorsConfig: SchemaValidatorsConfig

    @Inject
    private lateinit var resourceMapping: TestResourceMappingConfig

    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var mapper: YAMLMapper

    private val acceptedExtensions = listOf("json", "yaml", "yml")

    @OptIn(ExperimentalPathApi::class)
    @TestFactory
    fun `Test schema samples`(): Stream<DynamicTest> {
        val schemaRoot = getResource(resourceMapping.schemas).asPath()
        val schemaTestsRoot = getResource(resourceMapping.test.schemas).asPath()

        // For each schema in the "schemas" folder, we find all defined tests in the test resources
        return schemaRoot
            .walk()
            .filter { it.name.endsWith(".schema.json") }
            .flatMap { schema ->
                // Try to find the "test folder" corresponding to the schema path
                try {
                    // The tests for the schema with path "resource://schemas/foo/bar/foobar.schema.json" are in the resource
                    // folder "resource://tests/schemas/foo/bar/foobar/" and have extension "json"
                    val schemaDirectory = schema.relativeTo(schemaRoot).pathString
                        .substringBeforeLast(".schema.json")

                    schemaTestsRoot.resolve(schemaDirectory)
                        .listDirectoryEntries()
                        .filter { it.extension in acceptedExtensions }

                        // Transform all results into an arguments pair for our test
                        .map { ValidationTestSpec(schema, it, it.name.startsWith("valid")) }

                } catch (_: IOException) {
                    logger.info("Could not find any tests for schema ${schema.name}")

                    emptyList()
                }
            }
            .map { DynamicTest.dynamicTest(it.testName(schemaRoot)) { testSamples(it) } }
            .asStream()
    }

    @Testable // To help Approval Tests find the testing class
    private fun testSamples(test: ValidationTestSpec) {
        // Create instances of the schema and the sample json
        val schemaNode = mapper.readTree(test.schemaFile.toFile())
        val schema = JsonSchemaFactory
            .getInstance(SpecVersionDetector.detect(schemaNode), schemaFactoryCustomizer)
            .getSchema(schemaNode, schemaValidatorsConfig)

        // Load sample
        val sampleNode = mapper.readTree(test.sampleFile.toFile())

        // Perform the test
        val result = schema.validate(sampleNode)

        if(test.isValid) {
            // For valid tests, only assert validity
            Assertions.assertEquals(0, result.size, result.toString())
        } else {
            // For invalid tests, assert validity and then check the validation messages
            Assertions.assertNotEquals(0, result.size)

            // Some slight pretty printing of the validation messages
            val validationSummary = """
                |Validation Messages for ${test.sampleFile.name}:
                |[
                |    ${result.joinToString("\n|    ")}
                |]
            """.trimMargin()

            val approvalSpec = ApprovalTestSpec.from(test.sampleFile, "errors")
            Approvals.verify(validationSummary, approvalSpec.toOptions())
        }
    }

    private data class ValidationTestSpec(
        val schemaFile: Path,
        val sampleFile: Path,
        val isValid: Boolean,
    ) {
        fun testName(schemaRoot: Path): String =
            "Sample ${sampleFile.name} should ${if (isValid) "" else "not "}" +
                "be valid for schema ${schemaFile.relativeTo(schemaRoot).pathString}"
    }
}
