package nl.rug.digitallab.configuration.logging

import nl.rug.digitallab.configuration.logging.entries.LogEntry
import nl.rug.digitallab.configuration.logging.entries.TracingEntry
import org.junit.jupiter.api.Assertions

/**
 * Simple [ConfigurationLogger] implementation that stores all
 * log messages, making them available for future inspection. It
 * also brings some methods to perform assertions on the log entries,
 * so easily check that the required log entries are created.
 */
class TrackingLogger : ConfigurationLogger {
    val logEntries: MutableList<LogEntry<*>> = mutableListOf()

    override fun log(entry: LogEntry<*>) {
        logEntries.add(entry)
    }

    /**
     * Assert that there are [count] number of log entries of
     * the given type, and "consume" them. This way, tests can
     * check that exactly the right number of log entries were
     * created.
     *
     * @param T The type of the log entry, subtype of [LogEntry]
     * @param count The number of log messages of the given type
     */
    inline fun <reified T : LogEntry<*>> assertLogsOfType(count: Number) {
        val logsOfType = logEntries.filterIsInstance<T>()

        Assertions.assertEquals(count, logsOfType.count())
        logEntries.removeAll(logsOfType)
    }

    fun assertLogsEmpty() {
        Assertions.assertEquals(0, logEntries.count())
    }

    fun assertOverrideLogs(count: Number) = assertLogsOfType<TracingEntry.Override<*>>(count)
    fun assertInheritLogs(count: Number) = assertLogsOfType<TracingEntry.Inherit<*>>(count)
    fun assertMergeLogs(count: Number) = assertLogsOfType<TracingEntry.Merge<*>>(count)
    fun assertCloneLogs(count: Number) = assertLogsOfType<TracingEntry.Clone<*>>(count)
    fun assertTransformLogs(count: Number) = assertLogsOfType<TracingEntry.Transform<*>>(count)

    fun assertNoOtherLogs(message: String) {
        Assertions.assertEquals(emptyList<LogEntry<*>>(), logEntries, message)
    }
}

/**
 * Helper function to quickly instantiate a [TrackingLogger] and
 * make it contextually available in the [block]. It checks that
 * all log entries have been "consumed" by assertions in available
 * in [TrackingLogger].
 */
fun usingTrackingLogger(block: context(TrackingLogger) () -> Unit) {
    val logger = TrackingLogger()

    // We should be able to wrap this call in a with() block, but that
    // scenario seems to be bugged in Kotlin atm. (KT-60953)
    // https://youtrack.jetbrains.com/issue/KT-60953/Context-receivers-are-required-to-be-passed-as-an-explicit-argument-in-some-scenarios
    block(logger)

    // We expect all the logs to be "consumed" by assertions
    logger.assertNoOtherLogs("Expected logs to be empty at end of test execution")
}
