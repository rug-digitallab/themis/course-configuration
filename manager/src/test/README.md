# JSON Schema testing

Tests for the JSON Schemas require a specific structure to ensure they are picked up by the testing suite.
Currently, the structure is as follows:

- For each schema at path `./foo/bar/entity.schema.json` relative to the schemas directory in the repository root,
  tests should be defined in the directory `./resources/tests/schemas/foo/bar/entity` (relative to this tests root).
- Tests are specified as JSON or YAML files, each containing an entity to be validated and/or parsed.
- Tests must be prefixed with either `valid-` or `invalid-` to indicate whether they should satisfy or violate the schema.
- Tests prefixed with `valid-` are validated against their schema, and are then parsed to the appropriate Kotlin class
  (annotated with the `@JsonSchema` annotation).
- Tests prefixed with `invalid-` are validated against their schema, and the validation messages are stored to ensure
  consistency in test outputs.
