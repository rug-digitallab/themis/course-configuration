Rubric(
  name = "RPG Rubric",
  description = "This is the the rubric for the RPG assignment. Which is the first assignment for the course OOP.",
  categories = List(
    Category(
      name = "Functionality - Basics",
      description = "Do not give less than 0 points for any of the following parts.",
      criteria = List(
        Criterion(
          name = "Interaction menu",
          description = "The program allows user interaction through the CLI. As mentioned in the assignment, they should have some basic interactions: (0- look around, 1 - look for a way out etc). This should work properly, and all the functionality is in there.",
          allowOverride = false,
          buckets = List(
            Bucket(
              name = "No interaction menu",
              description = "The program does not allow user interaction through the CLI.",
              points = 0
            ),
            Bucket(
              name = "Interaction menu, but not working properly",
              description = "The program allows user interaction through the CLI, but it does not work properly.",
              points = 0.5
            ),
            Bucket(
              name = "Interaction menu, all functionality",
              description = "The program allows user interaction through the CLI, and it works properly.",
              points = 1
            )
          ),
          manualPoints = null,
          constantPoints = null,
          externalPoints = null
        ),
        Criterion(
          name = "Inspectable Rooms + Doors + NPCs",
          description = "The program allows the user to inspect rooms, doors and NPCs. This should work properly, and all the functionality is in there.",
          allowOverride = true,
          buckets = List(
            Bucket(
              name = "No inspectable rooms, doors or NPCs",
              description = "The program does not allow the user to inspect rooms, doors and NPCs.",
              points = 0
            ),
            Bucket(
              name = "Inspectable rooms, doors or NPCs, but not working properly",
              description = "The program allows the user to inspect rooms, doors and NPCs, but not done using an interface.",
              points = 0.1
            ),
            Bucket(
              name = "Inspectable rooms, doors or NPCs, all functionality",
              description = "The program allows the user to inspect rooms, doors and NPCs, but inspecting a room does not even print the number of doors in the room.",
              points = 0.15
            ),
            Bucket(
              name = "Inspectable rooms, doors or NPCs, all functionality",
              description = "The program allows the user to inspect rooms, doors and NPCs. And is implemented using the interface.",
              points = 0.25
            )
          ),
          manualPoints = null,
          constantPoints = null,
          externalPoints = null
        )
      ),
      scalePointsTo = null
    )
  ),
  scalePointsTo = 10
)
