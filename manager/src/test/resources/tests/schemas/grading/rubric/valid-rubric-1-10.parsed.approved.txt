Rubric(
  name = "Free Point rubric",
  description = """This rubric has a simple assignment that is graded on a scale of 1-10, so the student will always get 1 free point.
Normally, grades below 1 are not valid. Some teachers opt to just cap grades at 1 (so a 0 becomes a 1), but others actually explicitly grade on a scale from 1-10.""",
  categories = List(
    Category(
      name = "Assignment",
      description = "",
      criteria = List(
        Criterion(
          name = "Assignment",
          description = "",
          allowOverride = true,
          buckets = null,
          manualPoints = 9,
          constantPoints = null,
          externalPoints = null
        )
      ),
      scalePointsTo = null
    ),
    Category(
      name = "Free point",
      description = "",
      criteria = List(
        Criterion(
          name = "Free point",
          description = "",
          allowOverride = true,
          buckets = null,
          manualPoints = null,
          constantPoints = 1,
          externalPoints = null
        )
      ),
      scalePointsTo = null
    )
  ),
  scalePointsTo = null
)
