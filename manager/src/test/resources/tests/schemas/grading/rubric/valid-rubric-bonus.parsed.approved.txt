Rubric(
  name = "Rubric with bonus",
  description = """This rubric has a few categories that correspond to separate assignments, and each of these assignments is graded on a scale of 10 points, with equal weight. In addition, there is the option to get up to 1 bonus point on the lab as a whole, after the averaging of all assignments.
Grading like this is often used for programming labs with multiple smaller assignments where a bonus option is offered. This setup can also be used for a "global" style grade (then the assignments are all averaged to, say, 7 points and the style is a "3 point bonus").""",
  categories = List(
    Category(
      name = "Assignment 1",
      description = "",
      criteria = List(
        Criterion(
          name = "Correctness",
          description = "",
          allowOverride = true,
          buckets = null,
          manualPoints = 10,
          constantPoints = null,
          externalPoints = null
        )
      ),
      scalePointsTo = 4
    ),
    Category(
      name = "Assignment 2",
      description = "",
      criteria = List(
        Criterion(
          name = "Correctness",
          description = "",
          allowOverride = true,
          buckets = null,
          manualPoints = 10,
          constantPoints = null,
          externalPoints = null
        )
      ),
      scalePointsTo = 6
    ),
    Category(
      name = "Bonus",
      description = "",
      criteria = List(
        Criterion(
          name = "Bonus",
          description = "",
          allowOverride = true,
          buckets = null,
          manualPoints = 1,
          constantPoints = null,
          externalPoints = null
        )
      ),
      scalePointsTo = null
    )
  ),
  scalePointsTo = null
)
