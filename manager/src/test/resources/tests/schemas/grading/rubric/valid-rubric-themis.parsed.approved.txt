Rubric(
  name = "Themis Rubric",
  description = """This rubric uses external scores as determined by Themis. These are the typical "correctness" points.
Grading like this will be very common.""",
  categories = List(
    Category(
      name = "Assignment 1",
      description = "",
      criteria = List(
        Criterion(
          name = "Correctness",
          description = "",
          allowOverride = true,
          buckets = null,
          manualPoints = null,
          constantPoints = null,
          externalPoints = ThemisPointsSource(assignmentPath = "foo/bar", scalePointsTo = 7)
        ),
        Criterion(
          name = "Style",
          description = "",
          allowOverride = true,
          buckets = List(
            Bucket(name = "Bad style", description = "", points = 0),
            Bucket(name = "Could use improvement", description = "", points = 1),
            Bucket(name = "Good", description = "", points = 2),
            Bucket(name = "Perfect", description = "", points = 3)
          ),
          manualPoints = null,
          constantPoints = null,
          externalPoints = null
        )
      ),
      scalePointsTo = null
    ),
    Category(
      name = "Assignment 2",
      description = "",
      criteria = List(
        Criterion(
          name = "Correctness",
          description = "",
          allowOverride = true,
          buckets = null,
          manualPoints = null,
          constantPoints = null,
          externalPoints = ThemisPointsSource(assignmentPath = "bar/foo", scalePointsTo = 7)
        ),
        Criterion(
          name = "Style",
          description = "",
          allowOverride = true,
          buckets = List(
            Bucket(name = "Bad style", description = "", points = 0),
            Bucket(name = "Could use improvement", description = "", points = 1),
            Bucket(name = "Good", description = "", points = 2),
            Bucket(name = "Perfect", description = "", points = 3)
          ),
          manualPoints = null,
          constantPoints = null,
          externalPoints = null
        )
      ),
      scalePointsTo = null
    )
  ),
  scalePointsTo = 10
)
