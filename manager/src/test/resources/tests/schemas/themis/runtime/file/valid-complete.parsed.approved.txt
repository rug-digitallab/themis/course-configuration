RuntimeFile(
  source = "\$some-reference",
  targetPath = "/our/destination",
  priority = ORDER,
  permissions = Permissions(
    ownerUser = 1000,
    ownerGroup = 1000,
    user = Set(READ, WRITE, EXEC),
    group = Set(READ),
    world = Set()
  ),
  path = Path($file.)
)