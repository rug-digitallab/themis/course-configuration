Validation Messages for invalid-permissions.json:
[
    $: must be valid to one and only one schema, but 0 are valid
    $: object found, string expected
    $.permissions.user[2]: does not have a value in the enumeration ["read", "write", "exec"]
]
