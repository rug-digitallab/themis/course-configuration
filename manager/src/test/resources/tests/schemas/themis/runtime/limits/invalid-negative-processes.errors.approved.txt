Validation Messages for invalid-negative-processes.json:
[
    $.processes: must be valid to one and only one schema, but 0 are valid
    $.processes: must have a minimum value of 1
    $.processes: integer found, string expected
    $.processes: must be the constant value 'default'
]