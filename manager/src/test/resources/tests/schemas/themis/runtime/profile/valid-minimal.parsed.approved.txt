Profile(
  name = "",
  environments = Map(
    "testing" -> Environment(
      name = "",
      type = "container",
      lifetime = JUDGEMENT,
      environmentVariables = Map(),
      files = List(),
      limits = Limits(
        time = null,
        cores = null,
        processes = null,
        memory = null,
        disk = null,
        output = null,
        path = Path($limits.)
      ),
      path = Path($.)
    )
  ),
  stages = Map(
    "Compilation" -> Stage(
      name = "",
      order = null,
      scheduling = ONCE,
      shouldContinue = "\$compile.exitcode == 0",
      testGroup = null,
      testStrategy = null,
      statusConditions = StatusConditions(
        compilationError = null,
        wrongOutput = null,
        runtimeError = null
      ),
      actions = Map(
        "compile" -> Action(
          name = "",
          order = null,
          environment = "testing",
          command = "gcc",
          arguments = List("*.c"),
          input = null,
          outputs = null,
          environmentVariables = Map(),
          files = List(),
          limits = Limits(
            time = null,
            cores = null,
            processes = null,
            memory = null,
            disk = null,
            output = null,
            path = Path($limits.)
          ),
          path = Path($.)
        )
      ),
      path = Path($.)
    )
  ),
  tests = Map(),
  path = Path($profile.)
)
