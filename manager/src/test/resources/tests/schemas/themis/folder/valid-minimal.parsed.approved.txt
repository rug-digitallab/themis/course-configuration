Folder(
  name = "Week 1",
  visibility = null,
  softDeadline = null,
  hardDeadline = null,
  profiles = Map(),
  tests = Map(),
  slug = "folder",
  assignments = List(),
  folders = List(),
  path = Path(folder$.)
)