Folder(
  name = "Week 2",
  visibility = Visibility(visibilityType = TIMED, start = 2021-12-31T23:00:00Z, end = null),
  softDeadline = 2023-06-30T22:00:00Z,
  hardDeadline = 2023-06-30T23:00:00Z,
  profiles = Map(
    "default" -> Profile(
      name = "",
      environments = Map(
        "compilation" -> Environment(
          name = "",
          type = "container",
          lifetime = STAGE,
          environmentVariables = Map(),
          files = List(),
          limits = Limits(
            time = null,
            cores = null,
            processes = null,
            memory = null,
            disk = null,
            output = null,
            path = Path($limits.)
          ),
          path = Path($.)
        ),
        "testing" -> Environment(
          name = "",
          type = "container",
          lifetime = STAGE,
          environmentVariables = Map(),
          files = List(),
          limits = Limits(
            time = null,
            cores = null,
            processes = null,
            memory = null,
            disk = null,
            output = null,
            path = Path($limits.)
          ),
          path = Path($.)
        ),
        "checking" -> Environment(
          name = "",
          type = "container",
          lifetime = JUDGEMENT,
          environmentVariables = Map(),
          files = List(),
          limits = Limits(
            time = null,
            cores = null,
            processes = null,
            memory = null,
            disk = null,
            output = null,
            path = Path($limits.)
          ),
          path = Path($.)
        )
      ),
      stages = Map(
        "Compilation" -> Stage(
          name = "",
          order = null,
          scheduling = ONCE,
          shouldContinue = "\$compile.exitcode == 0",
          testGroup = null,
          testStrategy = null,
          statusConditions = StatusConditions(
            compilationError = null,
            wrongOutput = null,
            runtimeError = null
          ),
          actions = Map(
            "compile" -> Action(
              name = "",
              order = null,
              environment = "compilation",
              command = "gcc",
              arguments = List("*.c"),
              input = null,
              outputs = null,
              environmentVariables = Map(),
              files = List(),
              limits = Limits(
                time = null,
                cores = null,
                processes = null,
                memory = null,
                disk = null,
                output = null,
                path = Path($limits.)
              ),
              path = Path($.)
            )
          ),
          path = Path($.)
        ),
        "Execution" -> Stage(
          name = "",
          order = null,
          scheduling = TESTS,
          shouldContinue = "\$run.exitcode == 0 && \$diff.exitcode == 0",
          testGroup = "optional",
          testStrategy = SEQUENCE,
          statusConditions = StatusConditions(
            compilationError = null,
            wrongOutput = null,
            runtimeError = null
          ),
          actions = Map(
            "run" -> Action(
              name = "",
              order = null,
              environment = "testing",
              command = "./a.out",
              arguments = null,
              input = "\$test.input",
              outputs = List(
                Output(file = "stdout", maxSize = null),
                Output(file = "exitcode", maxSize = null),
                Output(file = "stderr", maxSize = null)
              ),
              environmentVariables = Map(),
              files = List(),
              limits = Limits(
                time = null,
                cores = null,
                processes = null,
                memory = null,
                disk = null,
                output = null,
                path = Path($limits.)
              ),
              path = Path($.)
            ),
            "diff" -> Action(
              name = "",
              order = null,
              environment = "checking",
              command = "diff",
              arguments = List("\$test.output", "\$run.stdout"),
              input = null,
              outputs = List(
                Output(file = "stdout", maxSize = null),
                Output(file = "exitcode", maxSize = null)
              ),
              environmentVariables = Map(),
              files = List(),
              limits = Limits(
                time = null,
                cores = null,
                processes = null,
                memory = null,
                disk = null,
                output = null,
                path = Path($limits.)
              ),
              path = Path($.)
            )
          ),
          path = Path($.)
        )
      ),
      tests = Map(),
      path = Path($profile.)
    ),
    "special" -> null
  ),
  tests = Map(
    "10" -> Test(
      name = "",
      visibility = TestVisibility(
        name = true,
        hint = true,
        status = false,
        resources = List(),
        limits = false
      ),
      hint = "This is a very nice hint that is visible",
      note = "Teachers should know that this test will always pass",
      group = "optional",
      runtimeOverrides = Map(
        "myProfile" -> Stage(
          name = "",
          order = null,
          scheduling = null,
          shouldContinue = null,
          testGroup = null,
          testStrategy = null,
          statusConditions = StatusConditions(
            compilationError = null,
            wrongOutput = null,
            runtimeError = null
          ),
          actions = Map(
            "Execute code" -> Action(
              name = "",
              order = null,
              environment = null,
              command = "valgrind ./a.out",
              arguments = null,
              input = null,
              outputs = null,
              environmentVariables = Map(),
              files = List(),
              limits = Limits(
                time = null,
                cores = null,
                processes = null,
                memory = null,
                disk = null,
                output = null,
                path = Path($limits.)
              ),
              path = Path($.)
            )
          ),
          path = Path($.)
        )
      ),
      path = Path($.)
    )
  ),
  slug = "folder",
  assignments = List(),
  folders = List(),
  path = Path(folder$.)
)
