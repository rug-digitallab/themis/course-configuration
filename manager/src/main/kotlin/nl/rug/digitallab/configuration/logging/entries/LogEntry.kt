package nl.rug.digitallab.configuration.logging.entries

import nl.rug.digitallab.configuration.logging.Logger

/**
 * Interface for any log entry during configuration logging.
 * We always need a log type, some location identifier that
 * can be used to group messages, and a message string itself.
 *
 * Implementations of this interface can store additional
 * information for more specialised contexts.
 */
interface LogEntry<Location> {
    val type: Logger.Type
    val location: Location
    val message: String
}
