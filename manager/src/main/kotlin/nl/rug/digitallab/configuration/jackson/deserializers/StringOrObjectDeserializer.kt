package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import nl.rug.digitallab.configuration.helpers.handleUnexpectedToken
import kotlin.reflect.KClass

/**
 * Generic [WrappingDeserializer] to deserialize entities that can either
 * be represented by an object in JSON, or by just a single [String] that
 * then corresponds to one of the fields in the target entity.
 *
 * @param wrappedDeserializer The original deserializer for the target type
 * @param type The type to deserialize
 * @param construct A function to construct instances of the target entity
 *      given just the deserialized string.
 */
abstract class StringOrObjectDeserializer<T : Any> (
    wrappedDeserializer: JsonDeserializer<*>,
    type: KClass<T>, // do not mark as val, since the super type already has a field type
    val construct: (String) -> T
) : WrappingDeserializer<T>(wrappedDeserializer, type) {
    @Suppress("UNCHECKED_CAST")
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): T = when(p.currentToken) {
        JsonToken.VALUE_STRING -> construct(p.valueAsString)
        JsonToken.START_OBJECT -> wrappedDeserializer.deserialize(p, ctxt) as T
        else -> ctxt.handleUnexpectedToken(type, p)
    }
}