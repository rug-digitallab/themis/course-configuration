package nl.rug.digitallab.configuration.inheritance.rulesets

import arrow.optics.dsl.notNull
import nl.rug.digitallab.configuration.inheritance.merging.mapsWith
import nl.rug.digitallab.configuration.inheritance.merging.valuesOfType
import nl.rug.digitallab.configuration.inheritance.strategies.withMerge
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.invoke
import nl.rug.digitallab.configuration.models.schemas.Course
import nl.rug.digitallab.configuration.models.schemas.themis
import nl.rug.digitallab.configuration.models.schemas.themis.Assignment
import nl.rug.digitallab.configuration.models.schemas.themis.Folder
import nl.rug.digitallab.configuration.models.schemas.themis.profiles
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Profile
import nl.rug.digitallab.configuration.models.schemas.traverseThemis

/**
 * This ruleset handles the inheritance and merging of all Themis profiles,
 * inheriting from course level to underlying folder and assignment levels.
 */
class ProfileRuleset : Ruleset<Course>() {
    context(ConfigurationLogger)
    override fun applyImplementation(root: Property<Course>): Property<Course> {
        // Put the immutable function parameter in a local mutable variable
        var course = root

        val profilesTraversal = traverseThemis(
            courseBuilder = { inspect(Course.themis().notNull.profiles) },
            folderBuilder = { inspect(Folder.profiles) },
            assignmentBuilder = { inspect(Assignment.profiles) },
        )

        course = profilesTraversal.withMerge { mapsWith(valuesOfType(Profile)) }.apply(course)

        return course
    }
}
