package nl.rug.digitallab.configuration.models.schemas.grading

import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.Property
import java.math.BigDecimal

@JsonSchema("./grading/rubric/category.schema.json")
data class Category (
    val name: Property<String>,

    @JsonProperty(defaultValue = "\"\"")
    val description: Property<String>,
    val criteria: ListProperty<Criterion>,

    @JsonProperty(defaultValue = "null")
    val scalePointsTo: Property<BigDecimal?>,
)
