package nl.rug.digitallab.configuration.inheritance

import arrow.optics.Every
import arrow.optics.Iso
import arrow.optics.Lens
import arrow.optics.Optional
import arrow.optics.dsl.every
import nl.rug.digitallab.configuration.helpers.ReferenceOr
import nl.rug.digitallab.configuration.helpers.referenceOr
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.notNull
import nl.rug.digitallab.configuration.models.Property.Companion.value
import nl.rug.digitallab.configuration.models.Property.Companion.with
import arrow.optics.Traversal as OpticsTraversal

typealias Traversal<RootT, PropertyT> = TraversalStep<Property<RootT>, *, PropertyT>

/**
 * Class representing a traversal procedure over a hierarchy of objects,
 * while visiting properties of type [PropertyT]. It's a self-recursive
 * class, allowing us to navigate through such an object hierarchy.
 *
 * Each instance of this class represents a "slice" of the traversal
 * operation: starting from some container type [SourceT], it has information
 * on how to access a set of child objects [items] of type [ItemT].
 * For each such [ItemT], the traversal might visit some [property],
 * and the traversal might navigate further down the hierarchy with a
 * number of [nextSteps].
 *
 * The layout of this class is not directly intuitive: it would make
 * sense to say "for type X, visit property foo, and then iterate down
 * via collections a, b, c". However, since the types of items in these
 * collections a, b, and c might not be the same, it is very hard to
 * then encode what properties in types A, B, and C to access, respectively.
 * Instead, this class essentially says "for type [SourceT], iterate down
 * via collection [items], and for each such [ItemT], visit property
 * [property]".
 *
 * To solve the chicken-and-egg problem that is the start of
 * instantiation: for the very first level of a Traversal, an identity
 * navigation can be used to navigate from [SourceT] to [SourceT], to allow
 * access to properties inside [SourceT].
 *
 * The implementation and usage of this class make heavy use of Lenses,
 * concepts from Functional Programming implemented in the Arrow Optics
 * library. Read up on them here: https://arrow-kt.io/learn/immutable-data/intro/
 *
 * NOTE: For people well-versed in Arrow Optics, the names of this class and
 * the [Traversal] type alias might be slightly confusing since it is the
 * same as the "Traversal" optic. However, this name is more convenient
 * elsewhere in the codebase, where optics aren't interacted with directly
 * and Traversal makes more sense
 */
class TraversalStep<SourceT, ItemT, PropertyT> (
    // Navigation from container type [SourceT] to child items
    val items: OpticsTraversal<SourceT, Property<ItemT>>,

    // Property to visit inside [ItemT]
    val property: Optional<ItemT, Property<PropertyT>>?,

    // The list of next steps to take from here
    var nextSteps: List<TraversalStep<ItemT, *, PropertyT>>,
) {
    @DslMarker
    annotation class TraversalBuilderMarker

    /**
     * Builder class to simplify the construction of [TraversalStep] instances.
     *
     * The implementation takes inspiration from Kotlin DSLs, with `builder`
     * functions. This way, the end-user can construct [Traversal]s without
     * being concerned with its implementation details - particularly the
     * fact that the [ItemT] accessor is not located intuitively. The DSL
     * defined here makes that a lot easier.
     */
    @TraversalBuilderMarker
    class Builder<SourceT, ItemT, PropertyT> private constructor(
        // A builder is constructed using the accessor from the root
        // container; this way the user configures the behaviour for
        // a type [Item], hiding the navigation property behaviour.
        private val items: OpticsTraversal<SourceT, Property<ItemT>>,
    ) {
        private var property: Optional<ItemT, Property<PropertyT>>? = null
        private var nextSteps: MutableList<TraversalStep<ItemT, *, PropertyT>> = mutableListOf()

        // To support self-referential traversals (where a type has
        // a property that is a list of itself), we store a list of
        // "self-referential properties" separately, that we will only
        // process upon final construction of the TraversalStep.
        private val selfProperties: MutableList<OpticsTraversal<ItemT, Property<ItemT>>> = mutableListOf()

        /**
         * Instruct the [TraversalStep] to inspect the given [property]
         * within [ItemT]. Only one property can be inspected in each
         * scope.
         *
         * @param property The [Optional] of the property to inspect
         */
        fun inspect(property: Optional<ItemT, Property<PropertyT>>) {
            this.property = property
        }

        /**
         * Instruct the [TraversalStep] to recursively visit items in
         * the given [property] (optics) traversal. Configure the behaviour
         * for each such item in the [buildFunction] function. This is an
         * internal helper method implementing the logic; the various
         * public [visit] methods are used to automatically adapt the optic
         * to fit the desired shape.
         *
         * @param property The property to recurse into
         * @param buildFunction Configure the behaviour for visiting [VisitT] objects
         */
        private fun <VisitT> visitImpl(
            property: OpticsTraversal<ItemT, Property<VisitT>>,
            buildFunction: Builder<*, VisitT, PropertyT>.() -> Unit,
        ) {
            val builder = Builder<ItemT, VisitT, PropertyT>(property)
            builder.buildFunction()
            nextSteps.add(builder.toTraversal())
        }

        /**
         * Instruct the [TraversalStep] to recursively visit items in
         * the given [property] list. Configure the behaviour for
         * each such item in the [buildFunction] function.
         *
         * @param property The property to recurse into
         * @param buildFunction Configure the behaviour for visiting [VisitT] objects
         */
        // While Kotlin can distinguish between all visit overloads at compile-time,
        // in JVM these methods will have identical signatures due to type erasure. To
        // still provide Kotlin users with a transparent and uniform API, we override the
        // JVM name of this method to explicitly distinguish between overloads.
        @JvmName("visitList")
        fun <VisitT : ITraversable> visit(
            property: Optional<ItemT, ListProperty<VisitT>>,
            buildFunction: Builder<*, VisitT, PropertyT>.() -> Unit,
        ) = visitImpl(property.value.every, buildFunction)

        /**
         * Instruct the [TraversalStep] to recursively visit items in
         * the given [property] map. Configure the behaviour for
         * each such item in the [buildFunction] function.
         *
         * @param property The property to recurse into
         * @param buildFunction Configure the behaviour for visiting [VisitT] objects
         */
        @JvmName("visitMap")
        fun <VisitT : ITraversable, KeyT> visit(
            property: Optional<ItemT, Property<Map<KeyT, Property<VisitT>>>>,
            buildFunction: Builder<*, VisitT, PropertyT>.() -> Unit
        ) = visitImpl(property.value.every, buildFunction)

        /**
         * Instruct the [TraversalStep] to recursively visit items in
         * the given [property] map, skipping null values.
         * Configure the behaviour for each such item in the
         * [buildFunction] function.
         *
         * @param property The property to recurse into
         * @param buildFunction Configure the behaviour for visiting non-null [VisitT] objects
         */
        @JvmName("visitMapWithNulls")
        fun <VisitT : ITraversable, KeyT> visit(
            property: Optional<ItemT, Property<Map<KeyT, Property<VisitT?>>>>,
            buildFunction: Builder<*, VisitT, PropertyT>.() -> Unit
        ) = visitImpl(property.value.every.notNull, buildFunction)

        /**
         * Instruct the [TraversalStep] to recursively visit items in
         * the given [property] ReferenceOr. Configure the behaviour for
         * such an item in the [buildFunction] function.
         *
         * @param property The property to recurse into
         * @param buildFunction Configure the behaviour for visiting [VisitT] objects
         */
        @JvmName("visitReference")
        fun <VisitT : ITraversable> visit(
            property: Optional<ItemT, Property<ReferenceOr<VisitT>>>,
            buildFunction: Builder<*, VisitT, PropertyT>.() -> Unit
        ) = visitImpl(property.with(Every.referenceOr()), buildFunction)

        /**
         * Instruct the [TraversalStep] to recursively visit the item in
         * the given [property]. Configure the behaviour
         * for such an item in the [buildFunction] function.
         *
         * @param property The property to recurse into
         * @param buildFunction Configure the behaviour for visiting [VisitT] objects
         */
        @JvmName("visitOptional")
        fun <VisitT : ITraversable> visit(
            property: Optional<ItemT, Property<VisitT>>,
            buildFunction: Builder<*, VisitT, PropertyT>.() -> Unit
        ) = visitImpl(property, buildFunction)

        /**
         * Instruct the [TraversalStep] to recursively visit the item in
         * the given [property] when present. Configure the behaviour
         * for such an item in the [buildFunction] function.
         *
         * @param property The property to recurse into
         * @param buildFunction Configure the behaviour for visiting [VisitT] objects
         */
        @JvmName("visitNullable")
        fun <VisitT : ITraversable> visit(
            property: Lens<ItemT, Property<VisitT?>>,
            buildFunction: Builder<*, VisitT, PropertyT>.() -> Unit
        ) = visitImpl(property.notNull, buildFunction)

        /**
         * Instruct the [TraversalStep] to recursively visit items in
         * the given [property] list, which are items of the same
         * type as [ItemT], and for which the visiting behaviour should
         * be identical.
         *
         * This will result in a self-referential [TraversalStep] that
         * will be able to traverse self-recursive object hierarchies
         * of any depth, up to stack overflow of course.
         *
         * @param property The property to recurse into
         */
        fun visitSelf(property: Optional<ItemT, Property<List<Property<ItemT>>>>) {
            selfProperties += property.value.every
        }

        /**
         * The function that actually converts this [Builder] into
         * a [TraversalStep].
         */
        private fun toTraversal(): TraversalStep<SourceT, ItemT, PropertyT> {
            // We first deal with the self-referential properties. For them, we
            // create a new TraversalStep instance with the right navigation property
            // and the current list of next steps (not a copy - so the SAME list)
            // Then at the end, we add all these new traversals to the current
            // list of "next steps" - which updates all such lists in all new
            // Traversals and thereby creating a self-referential structure.
            nextSteps.addAll(selfProperties.map {
                TraversalStep(
                    items = it,
                    property = property,
                    nextSteps = nextSteps
                )
            })

            // Construct the Traversal
            return TraversalStep(
                items = items,
                property = property,
                nextSteps = nextSteps
            )
        }

        companion object {
            /**
             * Access point for working with the [Builder]. This function
             * will create a [TraversalStep] with the configured behaviour given
             * in [buildFunction]. The traversal will start at the [RootT] level.
             *
             * Tip: as long as you don't leave the [buildFunction] entirely empty,
             * the compiler will be able to deduce the type parameters.
             *
             * For code readability, it might be better to use the [traversal]
             * helper method.
             *
             * @param buildFunction Configure the behaviour of the [TraversalStep]
             */
            fun <RootT, PropertyT> build(
                buildFunction: Builder<Property<RootT>, RootT, PropertyT>.() -> Unit
            ): TraversalStep<Property<RootT>, RootT, PropertyT> {
                // Since we want to start at Root level, our "accessor" is
                // simply the Identity map through an Iso (isomorphic optic)
                val builder = Builder<Property<RootT>, RootT, PropertyT>(Iso.id())
                builder.buildFunction()
                return builder.toTraversal()
            }
        }
    }
}

/**
 * Convenient type alias for a function that builds a Traversal level
 */
typealias TraversalVisit<ItemT, PropertyT> = TraversalStep.Builder<*, ItemT, PropertyT>.() -> Unit

/**
 * Access point for working with the [TraversalStep.Builder]. This function
 * will create a [Traversal] with the configured behaviour given
 * in [buildFunction]. The traversal will start at the [RootT] level.
 *
 * Tip: as long as you don't leave the [buildFunction] entirely empty,
 * the compiler will be able to deduce the type parameters.
 *
 * @param buildFunction Configure the behaviour of the [TraversalStep]
 */
fun <RootT, PropertyT> traversal(
    buildFunction: TraversalStep.Builder<Property<RootT>, RootT, PropertyT>.() -> Unit
): Traversal<RootT, PropertyT> = TraversalStep.Builder.build(buildFunction)
