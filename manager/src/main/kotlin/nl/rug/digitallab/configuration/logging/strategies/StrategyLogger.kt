package nl.rug.digitallab.configuration.logging.strategies

import nl.rug.digitallab.configuration.logging.Logger

/**
 * Interface specifying the logging functions available during a traversal.
 * These differ from the normal [ConfigurationLogging] machinery, as these
 * require the user to specify a lot of context and state, whereas we want
 * to keep the strategy code as functional and stateless as possible.
 *
 * The solution is this custom logger interface, which will augment the
 * log messages with all context and state (e.g. current entity being visited)
 * in the background.
 */
interface StrategyLogger<T> : Logger {
    /**
     * Log an error that occurred during the strategy execution. These are
     * non-recoverable and indicate that something has gone drastically wrong.
     * As soon as an error occurs, no further processing of the configuration
     * will be done.
     *
     * For recoverable issues, use [strategyWarning] instead.
     *
     * @param message User-facing error message
     */
    fun strategyError(message: String)

    /**
     * Log a warning that occurred during the strategy execution. These are
     * recoverable issues and only warn the user of potential undesirable
     * or unintended effects of their configuration - allowing the user
     * to correct their configuration.
     *
     * For unrecoverable issues, use [strategyError] instead.
     *
     * @param message User-facing warning message
     */
    fun strategyWarning(message: String)

    /**
     * Log a debugging entry, explaining technical aspects of the code (flow)
     * to aid debugging for developers. These messages are normally not
     * user-facing - unless the user asks to show them.
     *
     * @param message The debugging message
     */
    fun strategyDebug(message: String)

    /**
     * Log a tracing entry for a modified value, serving as "explanation" of
     * what happened to properties during execution. This message is user-facing
     * and will be used to debug or analyse the final configuration.
     *
     * @param message The explanation of why the value changed
     */
    fun traceTransform(message: String)
}
