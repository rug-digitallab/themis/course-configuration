package nl.rug.digitallab.configuration.annotations

/**
 * Annotation used for linking a schema file to an entity,
 * which can then be used to first validate the JSON object
 * to deserialize against the schema, before deserializing
 * into Kotlin objects.
 *
 * @param ref The filepath of the schema, relative to the
 *      root schemas directory
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class JsonSchema(val ref: String)
