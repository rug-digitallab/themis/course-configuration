package nl.rug.digitallab.configuration.logging.deserialization

import com.fasterxml.jackson.core.JsonLocation
import java.io.File
import java.nio.file.Path

/**
 * Simple library-independent representation of the source of a particular
 * property, for use in tracing.
 */
data class PropertySourceLocation(
    val file: Path,
    val line: Int,
    val column: Int,
)

/**
 * Helper method to convert a Jackson-provided [JsonLocation] to an
 * instance of [PropertySourceLocation] for tracing purposes.
 *
 * @return Converted [PropertySourceLocation] instance
 */
fun JsonLocation.toPropertySourceLocation(): PropertySourceLocation {
    val file = this.contentReference().rawContent
    require(file is File) { "Cannot track non-file JsonLocations" }

    return PropertySourceLocation(
        file = file.toPath(),
        line = this.lineNr,
        column = this.columnNr,
    )
}
