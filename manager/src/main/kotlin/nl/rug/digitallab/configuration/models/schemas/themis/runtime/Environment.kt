package nl.rug.digitallab.configuration.models.schemas.themis.runtime

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.*
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.MapProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp

@optics
@JsonSchema("./themis/runtime/environment.schema.json")
data class Environment (
    // Since Environments only ever occur inside a map, this name will be taken from the map key
    @JsonIgnore
    val name: Property<String> = "".toProp(),

    // For these, null means to override property, while a value
    // (also empty string) overrides the inherited value.
    @JsonProperty(defaultValue = "null") val type: Property<String?>,
    @JsonProperty(defaultValue = "null") val lifetime: Property<Lifetime?>,

    // Environment variables and files have no distinction between emptiness and
    // absence - the inherited set will always be expanded upon
    @JsonProperty(defaultValue = "{}") val environmentVariables: MapProperty<String>,
    @JsonProperty(defaultValue = "[]") val files: ListProperty<RuntimeFile>,

    // There is no semantic difference between absence and emptiness
    // of limits, since each limit is specified separately.
    @JsonProperty(defaultValue = "{}") val limits: Property<Limits>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("").toProp(), // this needs to be updated once the environment name is known
) : ITraversable {
    override fun toNodeString(): String = "Environment \"$name\""

    enum class Lifetime {
        JUDGEMENT, STAGE
    }

    companion object : Mergeable<Environment> {
        override val merger = mergeByProperty<Environment> {
            name        use current
            type        merge overrideOrInherit()
            lifetime    merge overrideOrInherit()

            environmentVariables merge mapsWith(overrideOrInherit())
            files       merge listsByKeyWith(RuntimeFile.targetPath, valuesOfType(RuntimeFile))
            limits      merge valuesOfType(Limits)

            path        use current
        }
    }
}
