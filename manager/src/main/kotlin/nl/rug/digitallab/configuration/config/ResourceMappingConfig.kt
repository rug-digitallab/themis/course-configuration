package nl.rug.digitallab.configuration.config

import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithName

@ConfigMapping(prefix = "resources")
interface ResourceMappingConfig {
    @get:WithName("schemas")
    val schemas: String
}