package nl.rug.digitallab.configuration

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.ContextualDeserializer
import com.fasterxml.jackson.databind.introspect.Annotated
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.addDeserializer
import com.fasterxml.jackson.module.kotlin.addSerializer
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import nl.rug.digitallab.configuration.helpers.handleUnexpectedToken
import nl.rug.digitallab.configuration.jackson.addValue
import nl.rug.digitallab.configuration.jackson.findInjectable
import nl.rug.digitallab.configuration.logging.Logger
import nl.rug.digitallab.configuration.logging.entries.TracingEntry
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import java.math.BigDecimal
import java.util.UUID

/**
 * Implementation of custom deep clone behaviour, which will deep
 * clone a given object except for [Property] identities.
 * In addition, it will trace the clone actions in a provided log.
 *
 * The implementation uses Jackson, as it turns out to have
 * significantly higher performance than native Java serialization
 * or alternative cloning routes - except for handwritten clone
 * functions everywhere, which is not desirable.
 *
 * To ensure proper behaviour and prevent conflicts with normal
 * Jackson serialization/deserialization behaviour on entities,
 * this uses a custom [ObjectMapper] that ignores all annotations.
 * The logic also uses a custom serializer/deserializer to handle
 * [Property] instances appropriately.
 */
object Cloning {
    /**
     * Custom annotation introspector for Jackson to ignore any
     * annotations that could impact (de)serialization behaviour
     * during cloning. We cannot simply set the USE_ANNOTATIONS
     * feature to false, since then the Kotlin module no longer
     * functions.
     */
    private object CloningAnnotationIntrospector : JacksonAnnotationIntrospector() {
        private val forbiddenAnnotations: List<Class<*>> = listOf(
            JsonIgnore::class.java,
            JsonIgnoreProperties::class.java,
            JsonIgnoreType::class.java,
            JsonProperty::class.java,
            JsonUnwrapped::class.java,
            JacksonInject::class.java,
            JsonCreator::class.java,
            JsonSetter::class.java,
            JsonGetter::class.java,
        )

        override fun <A : Annotation?> _findAnnotation(ann: Annotated, annoClass: Class<A>)
            = when (annoClass) {
                in forbiddenAnnotations -> null
                else -> super._findAnnotation(ann, annoClass)
            }

        override fun _hasAnnotation(ann: Annotated, annoClass: Class<out Annotation>)
            = when (annoClass) {
                in forbiddenAnnotations -> false
                else -> super._hasAnnotation(ann, annoClass)
            }
    }

    /**
     * Custom serializer for [Property] instances: will write the value of
     * the property as well as its identity to two separate fields.
     */
    private class TracingPropertySerializer : JsonSerializer<Property<*>>() {
        override fun serialize(value: Property<*>, gen: JsonGenerator, serializers: SerializerProvider) {
            gen.writeStartObject()
            gen.writeStringField("clonedIdentity", value.identity.toString())
            gen.writeObjectField("value", value.get())
            gen.writeEndObject()
        }
    }

    /**
     * Custom deserializer for [Property] instances, specific for cloning.
     * Expects a format as written by [TracingPropertySerializer], and
     * constructs a new instance of [Property] with a fresh identity. The
     * identity written to JSON will be used for a tracing log entry, to
     * log the fact that the new [Property] was a clone of the old one.
     */
    private class TracingPropertyDeserializer() : JsonDeserializer<Property<*>>(), ContextualDeserializer {
        private lateinit var valueType: JavaType

        constructor(valueType: JavaType) : this() {
            this.valueType = valueType
        }

        // We explicitly disallow nulls or absent values, since all values should have
        // been written during serialization.
        override fun getNullValue(ctxt: DeserializationContext) =
            ctxt.handleUnexpectedToken(Property::class, ctxt.parser)

        override fun getAbsentValue(ctxt: DeserializationContext) =
            ctxt.handleUnexpectedToken(Property::class, ctxt.parser)

        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Property<*> {
            // First, we read the JSON as a tree to obtain the cloned identity
            val tree = p.readValueAsTree<JsonNode>()
            val identity = UUID.fromString(tree["clonedIdentity"].asText())

            // Next, we try to parse the value. We handle the null case separately,
            // since readTreeAsValue does not handle that.
            val result =
                if(tree["value"].isNull)
                    null.toProp()
                else
                    ctxt.readTreeAsValue<Any>(tree["value"], valueType).toProp()

            ctxt.findLogger()?.log(TracingEntry.Clone(
                result.identity,
                result.get(),
                identity,
                "Cloned object",
            ))

            return result
        }

        override fun createContextual(ctxt: DeserializationContext, property: BeanProperty?): JsonDeserializer<*> {
            val wrapperType = ctxt.contextualType
            val valueType = wrapperType.containedType(0)
            return TracingPropertyDeserializer(valueType)
        }

        /**
         * Retrieve a [Logger] instance from the current injection context
         */
        private fun DeserializationContext.findLogger(): Logger? = this.findInjectable()
    }

    private val mapper: ObjectMapper by lazy {
        ObjectMapper()
            // Our custom mapper will be able to see all fields, even private ones, but
            // we ignore all other methods (such as computed properties).
            // This allows us to create exact replicas of the state, including internal
            // details.
            .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)

            // As the next step, we override the entire introspector chain. At this point,
            // this consists of only the default Jackson introspector that we replace for
            // our custom extension. Later, we will register some modules that will add
            // introspectors to the chain - so resetting the entire chain afterward would
            // break these modules.
            .setAnnotationIntrospector(CloningAnnotationIntrospector)

            // Register appropriate standard modules
            .registerKotlinModule()
            .registerModule(Jdk8Module())
            .registerModule(JavaTimeModule())

            // Register our own module to add the custom property (de)serializers
            .registerModule(object : SimpleModule() {
                init {
                    addSerializer(Property::class, TracingPropertySerializer())
                    addDeserializer(Property::class, TracingPropertyDeserializer())
                }
            })

            // Finally, we override the behaviour for (de)serialization of BigDecimal
            // instances - the default behaviour will cause the representation of
            // the original and cloned instances to be different.
            .also {
                it.configOverride(BigDecimal::class.java)
                    .setFormat(JsonFormat.Value.forShape(JsonFormat.Shape.STRING))
            }
    }

    /**
     * Creates a deep clone of the given [value], while assigning
     * new identities to [Property] instances. In addition, during
     * cloning, appropriate tracing log entries are created to
     * keep track of which properties were cloned from where. This
     * improves traceability of the configuration.
     */
    context(Logger)
    fun <T> cloneObject(value: T, clazz: Class<T>): T {
        mapper.injectableValues = InjectableValues.Std().addValue<Logger>(this@Logger)
        return mapper.convertValue(value, clazz)
    }

    /**
     * Creates a deep clone of the given [value], while assigning
     * new identities to [Property] instances. In addition, during
     * cloning, appropriate tracing log entries are created to
     * keep track of which properties were cloned from where. This
     * improves traceability of the configuration.
     */
    context(Logger)
    inline fun <reified T> T.clone(): T = cloneObject(this, T::class.java)

    /**
     * Creates a deep clone of the given [value], while assigning
     * new identities to [Property] instances. In addition, during
     * cloning, appropriate tracing log entries are created to
     * keep track of which properties were cloned from where. This
     * improves traceability of the configuration.
     */
    context(Logger)
    inline fun <reified T> Property<T>.clone(): Property<T> = this.get().clone().toProp()
}
