package nl.rug.digitallab.configuration.models.schemas.themis.runtime

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.*
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.models.*
import nl.rug.digitallab.configuration.models.Property.Companion.toProp

@optics
@JsonSchema("./themis/runtime/action.schema.json")
data class Action (
    // Since Actions only ever occur inside a map, this name will be taken from the map key
    @JsonIgnore
    val name: Property<String> = "".toProp(),

    // For these, null means to inherit the property, while a value
    // (also empty string) overrides the inherited value.
    @JsonProperty(defaultValue = "null") val order: Property<Int?>,
    @JsonProperty(defaultValue = "null") val environment: Property<String?>,
    @JsonProperty(defaultValue = "null") val command: Property<String?>,
    @JsonProperty(defaultValue = "null") val arguments: NullableListProperty<String>,
    @JsonProperty(defaultValue = "null") val input: Property<String?>,
    @JsonProperty(defaultValue = "null") val outputs: NullableListProperty<Output>,

    // Environment variables and files have no distinction between emptiness and
    // absence - the inherited set will always be expanded upon
    @JsonProperty(defaultValue = "{}") val environmentVariables: MapProperty<String>,
    @JsonProperty(defaultValue = "[]") val files: ListProperty<RuntimeFile>,

    // There is no semantic difference between absence or emptiness of the limits,
    // since each limit is specified separately.
    @JsonProperty(defaultValue = "{}") val limits: Property<Limits>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("").toProp(),
) : ITraversable {
    override fun toNodeString(): String = "Action \"$name\""

    data class Output (
        val file: String,
        val maxSize: ByteSize? = null
    )

    companion object : Mergeable<Action> {
        override val merger = mergeByProperty<Action> {
            name            use current

            order           merge overrideOrInherit()
            environment     merge overrideOrInherit()
            command         merge overrideOrInherit()
            arguments       merge overrideOrInherit()
            input           merge overrideOrInherit()
            outputs         merge overrideOrInherit()

            environmentVariables merge mapsWith(overrideOrInherit())
            files           merge listsByKeyWith(RuntimeFile.targetPath, valuesOfType(RuntimeFile))

            limits          merge valuesOfType(Limits)

            path            use current
        }
    }
}
