# Configuration Models

The classes in this package are the in-code representation of the JSON schemas as defined in `./schemas` in the root of this repository. They are not auto-generated, since the models are different from the schemas to be more compatible with the implementation of inheritance. In addition, these models define metadata governing the inheritance approach and logging.

## Guidelines

We shortly describe the conventions in this code for mapping the JSON schemas to code models:

- Every schema corresponds to a separate model class; and each class is annotated with the `@JsonSchema` annotation that includes the path to the schema definition relative to the root schema directory.

- Each model is implemented as a `data class` since this is immutable data. Changes in the models, e.g. during inheritance, will always result in a new instance of the class. This way, "snapshots" of the configuration during processing can be maintained.

- Each model has the `@optics` annotation and explicitly includes a `companion object` (possibly empty - but its declaration is required due to a Kotlin limitation). This enables the use of Arrow Optics, which is heavily used for validation and inheritance.

- Every "entity" should implement `ITraversable` and should thus define a `path` property. Classes that represent a composite value are not "entities" - think of "Visibility" which is a composite value but represents one logical value/concept.

  A good rule of thumb for this is whether we want to investigate a property of the class in isolation: for "Visibility", we only ever want to consider the entire class as a whole, while for "Limits", we want to be able to validate and inspect the individual limits in isolation.

- Entities that are only ever declared in a map inside the JSON schemas should include some form of "identity" property, such as `name`. This property should be annotated `@JsonIgnore` and its value should be determined implicitly from, for example, the map key using a traversal in the `PathConstructorRuleset`.

- Properties always need to be of some type `Property<T>` - this wrapper class enabled us to do the tracing during inheritance. For lists, there is `ListProperty<T>` (alias for `Property<List<Property<T>>>`), and for maps, there is `MapProperty<T>`.

  - This type `T` can be nullable; in maps this is always the case

- Properties can have default values - there are not specified by providing a default value to the constructor parameter, but instead an annotation `@JsonProperty(defaultValue = "")` is placed on the property. The default value here is a JSON string representing the default value. This allows us to explicitly keep track of default value instantiation, which would be impossible for normal default values in the constructor.

- There are some guidelines about when to use a complex default value, when to use a nullable type `T`, or when to use both:

  - A _non-nullable, non-default_ property is used for _required_ schema properties.

  - A _nullable, null-default_ property is used for an _optional_ schema property, where there is a _semantic difference_ between absence and emptiness of the value. For example, in a list, providing `[]` could mean to clear an inherited list, whereas not specifying the property at all would correspond to just inheriting it.

  - Alternatively, a _non-nullable, defaulted_ property is used for an _optional_ schema property where there is no distinction between absence and emptiness.
