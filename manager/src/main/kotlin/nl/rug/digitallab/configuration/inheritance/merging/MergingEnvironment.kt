package nl.rug.digitallab.configuration.inheritance.merging

import nl.rug.digitallab.configuration.Cloning.clone
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger

@DslMarker
annotation class MergingEnvironmentMarker

/**
 * The [MergingEnvironment] interface is mainly intended to hide
 * various merging-related functions from non-merge use cases.
 * This limits pollution of the IDE autocomplete, and it makes
 * extra clear that methods are intended for use in the context
 * of merging.
 *
 * In particular, (extension) methods of this interface should
 * all have a default implementation, and its names should be
 * natural to follow the word "merge" - these functions are
 * typically called to produce a [Merger] to be consumed by
 * the [MergeBuilder] where they are the second parameter to
 * the [MergeBuilder.merge] infix function.
 *
 * For example: [mapsWith] merges maps, and [listsByKeyWith] merges
 * lists. Most of these extension functions can be seen as
 * implementing the [Mergeable] type class on a generic type,
 * such that it becomes a kind of Monad.
 *
 * It is typically not needed to instantiate this interface
 * manually or use the default instance, as that is handled
 * by the merging strategy.
 */
@MergingEnvironmentMarker
interface MergingEnvironment {
    // The companion object implements the interface itself,
    // as there are no methods to be implemented. This allows
    // an instance to be obtained by simply using the interface
    // name.
    companion object : MergingEnvironment
}

/**
 * Merge function deciding between inheriting a value, or
 * overriding the inherited value with the "current" value.
 *
 * By default, the decision function checks whether the "current"
 * value is non-null - when it is, the current value will
 * override the inherited value, otherwise, the inherited value
 * will be cloned and then used.
 *
 * This behaviour can be customized by providing a custom
 * [shouldOverride] decision function. This function will decide
 * whether the given value for "current" should override any
 * inherited value. It does not have access to the inherited value.
 *
 * This method will log appropriate tracing entries depending
 * on the [shouldOverride] decision function, the entries will
 * include the reason strings provided.
 *
 * @param shouldOverride Decision function deciding whether the
 *      given "current" value should override any inherited value.
 * @param overrideReason Message explaining why the inherited value
 *      was overridden. This message will follow the word "because".
 * @param inheritReason Message explaining why the inherited value
 *      was chosen. This message will follow the word "because".
 * @return [Merger] implementation for [ItemT]
 */
inline fun <reified ItemT> MergingEnvironment.overrideOrInherit(
    overrideReason: String = "a value was specified to override",
    inheritReason: String = "no current value was specified",
    crossinline shouldOverride: (value: ItemT) -> Boolean = { it != null }
): Merger<ItemT> = {
    if (shouldOverride(current)) {
        traceOverride("Overriding value, because $overrideReason")
        current
    } else {
        traceInherit("Inheriting value from parent, because $inheritReason")
        inherited.clone()
    }
}

/**
 * Merge function deciding between an inherited and current value
 * by choosing the maximum value (according to the [Comparable]
 * implementation).
 *
 * This method will ignore any null values, and will choose
 * the maximum value when both the current and inherited values
 * are present. The method will create appropriate tracing entries
 * with an explanation of the decision.
 *
 * @return [Merger] implementation for [ItemT]
 */
inline fun <reified ItemT : Comparable<ItemT>> MergingEnvironment.max(): Merger<ItemT?> = {
    when {
        current == null     -> inherited.clone().also { traceInherit("Inheriting value, since no current value was specified") }
        inherited == null   -> current          .also { traceOverride("Overriding value, since no value was inherited") }
        inherited > current -> inherited.clone().also { traceInherit("Inheriting value, since $inherited is larger than $current") }
        else                -> current          .also { traceOverride("Overriding value, since $current is larger than $inherited") }
    }
}

/**
 * Merge function deciding between an inherited and current value
 * by choosing the minimum value (according to the [Comparable]
 * implementation).
 *
 * This method will ignore any null values, and will choose
 * the minimum value when both the current and inherited values
 * are present. The method will create appropriate tracing entries
 * with an explanation of the decision.
 *
 * @return [Merger] implementation for [ItemT]
 */
inline fun <reified ItemT : Comparable<ItemT>> MergingEnvironment.min(): Merger<ItemT?> = {
    when {
        current == null     -> inherited.clone().also { traceInherit("Inheriting value, since no current value was specified") }
        inherited == null   -> current          .also { traceOverride("Overriding value, since no value was inherited") }
        inherited < current -> inherited.clone().also { traceInherit("Inheriting value, since $inherited is smaller than $current") }
        else                -> current          .also { traceOverride("Overriding value, since $current is smaller than $inherited") }
    }
}
