package nl.rug.digitallab.configuration.logging.strategies

import nl.rug.digitallab.configuration.inheritance.strategies.Strategy
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.Logger
import nl.rug.digitallab.configuration.logging.entries.LogEntry
import nl.rug.digitallab.configuration.logging.entries.StrategyLogEntry
import nl.rug.digitallab.configuration.logging.entries.TracingEntry
import nl.rug.digitallab.configuration.models.Property
import java.util.UUID

/**
 * The RelayingStrategyLogger is an implementation of the specialised
 * StrategyLogger interface that converts all log entries into messages
 * for the [ConfigurationLogger].
 *
 * This implementation will obtain the "context" of the current strategy
 * execution level (so, an accumulator and the current entity), after which
 * it will augment all provided log messages with this context before
 * passing the logs on to the [ConfigurationLogger]. This way, code using
 * the [StrategyLogger] does not have to be "aware" of the current context,
 * allowing it to remain functional.
 */
context(Logger)
class RelayingStrategyLogger <
    ItemT,
    AccumulatorT : Strategy.Accumulator<AccumulatorT, *>,
    PropertyT
> private constructor (
    private val location: UUID,
    private val initialValue: ItemT,
    private val accumulator: AccumulatorT,
) : StrategyLogger<PropertyT>, Logger by this {
    // We cannot log entries immediately, since the final value
    // of the item, after strategy execution, needs to be stored
    // along with the log entry. Therefore, we keep track of a list
    // of log entry "constructors" that only miss the final value.
    private val logEntryCreators = mutableListOf<(ItemT) -> LogEntry<*>>()

    /**
     * Helper function for various types of [StrategyLogEntry]
     */
    private fun configLogStrategy(type: Logger.Type, message: String) {
        logEntryCreators.add {
            StrategyLogEntry(
                type,
                location,
                initialValue,
                it,
                accumulator,
                message
            )
        }
    }

    override fun strategyError(message: String) = configLogStrategy(Logger.Type.ERROR, message)
    override fun strategyWarning(message: String) = configLogStrategy(Logger.Type.WARNING, message)
    override fun strategyDebug(message: String) = configLogStrategy(Logger.Type.DEBUG, message)

    override fun traceTransform(message: String) {
        logEntryCreators.add {
            TracingEntry.Transform(
                location,
                initialValue,
                it,
                message
            )
        }
    }

    companion object {
        /**
         * This function instantiates a new [RelayingStrategyLogger] instance
         * with the given context, and then performs the code in [block], passing
         * the [StrategyLogger] into its context. After executing [block],
         * the returned entity will be used to update the context of all
         * explanation log entries.
         *
         * @param item The item before modification
         * @param accumulator The accumulator state
         * @param block The code to execute with the [StrategyLogger] available. Returns a modified [item]
         */
        context(Logger)
        fun <
            ItemT,
            PropertyT,
            AccumulatorT : Strategy.Accumulator<AccumulatorT, *>,
        > withContext(
            item: Property<ItemT>,
            accumulator: AccumulatorT,
            block: context(StrategyLogger<PropertyT>) (ItemT) -> ItemT
        ): Property<ItemT> {
            val logger = RelayingStrategyLogger<ItemT, AccumulatorT, PropertyT>(item.identity, item(), accumulator)

            // We should be able to wrap this call in a with() block, but that
            // scenario seems to be bugged in Kotlin atm. (KT-60953)
            // https://youtrack.jetbrains.com/issue/KT-60953/Context-receivers-are-required-to-be-passed-as-an-explicit-argument-in-some-scenarios
            val result = block(logger, item())

            // We update the explanation context
            logger.logEntryCreators.forEach { log(it(result)) }

            return item.set(result)
        }
    }
}
