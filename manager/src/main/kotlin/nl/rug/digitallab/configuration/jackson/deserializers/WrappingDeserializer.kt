package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.databind.BeanProperty
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.ContextualDeserializer
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import kotlin.reflect.KClass

/**
 * Utility class to wrap an existing [JsonDeserializer] and augment
 * or adapt its behaviour for a specific target entity.
 *
 * Implementations of this class need to implement [deserialize]
 * themselves, and have access to the "wrapped" deserializer through
 * [wrappedDeserializer].
 *
 * @param T The type to deserialize
 * @param wrappedDeserializer The original deserializer for [T]
 * @param type The type to deserialize
 */
abstract class WrappingDeserializer<T : Any> (
    protected val wrappedDeserializer: JsonDeserializer<*>,
    val type: KClass<T>
) : StdDeserializer<T>(type.java), ResolvableDeserializer, ContextualDeserializer {
    // We need to implement both ResolvableDeserializer and ContextualDeserializer
    // to make this wrapping work properly - we just propagate the calls to the
    // wrapped deserializers.
    override fun resolve(ctxt: DeserializationContext) {
        if (wrappedDeserializer is ResolvableDeserializer)
            wrappedDeserializer.resolve(ctxt)
    }

    override fun createContextual(ctxt: DeserializationContext, property: BeanProperty?) =
        if (wrappedDeserializer is ContextualDeserializer)
            withDeserializer(wrappedDeserializer.createContextual(ctxt, property))
        else
            this

    override fun isCachable() = wrappedDeserializer.isCachable

    /**
     * This method is used to create a new instance of this [WrappingDeserializer]
     * with the given wrapped deserializer. Implementations should not change the
     * current instance, rather they should create a new instance with the given
     * deserializer.
     */
    protected abstract fun withDeserializer(deserializer: JsonDeserializer<*>): WrappingDeserializer<T>
}
