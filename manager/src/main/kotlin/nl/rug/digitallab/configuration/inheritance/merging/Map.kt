package nl.rug.digitallab.configuration.inheritance.merging

import nl.rug.digitallab.configuration.Cloning.clone
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.models.ItemMap
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.Property.Companion.withValueOrNull

/**
 * Merge function implementing a maps merger that will merge
 * entries by their key:
 * - By default, all entries from the inherited map are used and cloned
 * - Entries in the current map with a new key are added
 * - Entries in the current map with an existing key have their
 *      values merged using [mergeItems]
 * - Entries in the current map with an explicit null value are removed.
 *
 * This method will create a single merge tracing entry at the level
 * of the map, with a short log of "actions taken" during the map merge.
 * In addition, for each inherited entry, an inherit tracing entry is
 * created for the entry itself. For each merged entry, [mergeItems]
 * is responsible for creating appropriate tracing entries on the entry.
 * Erased entries (i.e. [null] was provided) cannot have a tracing
 * entry at the entry level (since it will be erased).
 *
 * @param mergeItems The method used to merge two items with the same key
 */
inline fun <reified ItemT> MergingEnvironment.mapsWith(
    noinline mergeItems: Merger<ItemT>,
): Merger<ItemMap<ItemT>> = merge@{
    // Create mutable merged map containing all results, based on current values
    val merged = current.toMutableMap()
    val mergeLog = mutableListOf<String>()

    // For each inherited entry, merge it into the current map
    for ((name, itemNullable) in inherited) {
        val item = itemNullable.withValueOrNull

        // In case the inherited value is null, we can skip
        if(item == null) {
            mergeLog.add("Not inheriting $name, since it has value 'null'")
            continue
        }

        // In case the key does not exist in the local map, we inherit a clone of the entry
        if(!merged.containsKey(name)) {
            // We need to create a new, empty item to serve as the inheriting "target"
            val newItem = null.toProp()

            // We inherit the item, but to properly add the tracing entry, we have to
            // scope the current merger to the item being inherited.
            merged[name] = newItem.set(scopeMerge(item, newItem) {
                traceInherit("Inherited new item $name")
                return@scopeMerge inherited.clone()
            })

            mergeLog.add("Inherited new item $name")
            continue
        }

        // In case the key exists, but the local value is `null`, we can just skip
        val currentItem = merged[name]?.withValueOrNull
        if(currentItem == null) {
            mergeLog.add("Item $name was removed, since 'null' was specified")
            continue
        }

        // Otherwise, the key exists in the local map and is not null, so we need
        // to merge the items into a new value
        val mergeResult = scopeMerge(item, currentItem, mergeItems)
        merged[name] = currentItem.set(mergeResult)

        mergeLog.add("Merged $name, since both local and inherited entries were present")
    }

    traceMerge("Merged lists with the following actions: ${mergeLog.joinToString(", ")}")

    // At the end, we clear any explicit nulls from the map since we
    // don't use it any further
    return@merge merged.filterValues { it.hasValue() }
}
