package nl.rug.digitallab.configuration.logging.entries

import nl.rug.digitallab.configuration.inheritance.strategies.Strategy
import nl.rug.digitallab.configuration.logging.Logger
import nl.rug.digitallab.configuration.models.EntityPath
import java.util.UUID

/**
 * [LogEntry] implementation for log items during [Strategy] execution.
 * It stores the current entity and accumulator value, capturing the
 * "state" during traversal. By default, the "location" is the [EntityPath]
 * of the entity.
 */
data class StrategyLogEntry<ItemT, AccumulatorT : Strategy.Accumulator<AccumulatorT, *>> (
    override val type: Logger.Type,
    override val location: UUID,
    val initialValue: ItemT,
    val finalValue: ItemT,
    val accumulator: AccumulatorT,
    override val message: String,
) : LogEntry<UUID>
