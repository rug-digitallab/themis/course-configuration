package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.databind.JsonDeserializer
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.RuntimeFile

/**
 * Custom deserializer for [RuntimeFile] items that accepts either full
 * objects, or just strings that represent only the file source
 *
 * @param wrappedDeserializer The original deserializer for [RuntimeFile]
 */
class FileDeserializer (
    wrappedDeserializer: JsonDeserializer<*>
) : StringOrObjectDeserializer<RuntimeFile>(
    wrappedDeserializer,
    RuntimeFile::class,
    {
        RuntimeFile(
            it.toProp(),
            priority = RuntimeFile.Priority.ORDER.toProp(),
            permissions = RuntimeFile.Permissions(null.toProp(), null.toProp(), null.toProp(), null.toProp(), null.toProp()).toProp(),
        )
    }
) {
    override fun withDeserializer(deserializer: JsonDeserializer<*>) =
        FileDeserializer(deserializer)
}
