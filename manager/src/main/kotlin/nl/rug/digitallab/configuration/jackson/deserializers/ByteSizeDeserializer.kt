package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.configuration.helpers.handleUnexpectedToken

class ByteSizeDeserializer (
    wrappedDeserializer: JsonDeserializer<*>
) : WrappingDeserializer<ByteSize>(wrappedDeserializer, ByteSize::class) {
    override fun withDeserializer(deserializer: JsonDeserializer<*>) =
        ByteSizeDeserializer(deserializer)

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): ByteSize {
        if(p.currentToken != JsonToken.VALUE_STRING)
            ctxt.handleUnexpectedToken(ByteSize::class, p)

        val size = ByteSize.fromString(p.valueAsString)
        if(size == (-1).B)
            ctxt.handleWeirdStringValue(ByteSize::class.java, p.valueAsString,"Invalid size format")

        return size
    }
}
