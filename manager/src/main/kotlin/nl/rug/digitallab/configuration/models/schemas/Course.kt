package nl.rug.digitallab.configuration.models.schemas

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.OptBoolean
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.schemas.grading.GradingCourse
import nl.rug.digitallab.configuration.models.schemas.repoman.RepomanCourse
import nl.rug.digitallab.configuration.models.schemas.themis.ThemisCourse
import java.util.EnumSet

@optics
@JsonSchema("./course.schema.json")
data class Course (
    // Required properties
    val name: Property<String>,
    val visibility: Property<Visibility>,

    @JsonProperty(defaultValue = "[]") val services: Property<EnumSet<Service>>,
    @JsonProperty(defaultValue = "null") val themis: Property<ThemisCourse?>,
    @JsonProperty(defaultValue = "null") val repoman: Property<RepomanCourse?>,
    @JsonProperty(defaultValue = "null") val grading: Property<GradingCourse?>,

    // The slug is implicitly determined from the directory name,
    // and is provided using Jackson "injection"
    @JacksonInject(value = "directory", useInput = OptBoolean.FALSE)
    val slug: Property<String>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("course[${slug()}]").toProp(),
) : ITraversable {
    override fun toNodeString(): String = "Course \"$name\""

    enum class Service {
        THEMIS, REPOMAN, VIRTUALLAB, GRADING
    }

    companion object
}
