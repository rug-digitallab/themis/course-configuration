package nl.rug.digitallab.configuration.models.schemas.themis.runtime

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.Mergeable
import nl.rug.digitallab.configuration.inheritance.merging.mapsWith
import nl.rug.digitallab.configuration.inheritance.merging.valuesOfType
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.MapProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp

@optics
@JsonSchema("./themis/runtime/profile.schema.json")
data class Profile (
    // Profiles only ever occur inside a map, so this name will be taken
    // from the map key
    @JsonIgnore
    val name: Property<String> = "".toProp(),

    // For all of these, there is no distinction between absence and emptiness.
    // Clearing entries from these maps is achieved by explicitly specifying
    // null for a key.
    @JsonProperty(defaultValue = "{}") val environments: MapProperty<Environment>,
    @JsonProperty(defaultValue = "{}") val stages: MapProperty<Stage>,
    @JsonProperty(defaultValue = "{}") val tests: MapProperty<Test>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("\$profile").toProp(),
) : ITraversable {
    override fun toNodeString(): String = "Profile \"$name\""

    companion object : Mergeable<Profile> {
        override val merger = mergeByProperty<Profile> {
            name            use current

            environments    merge mapsWith(valuesOfType(Environment))
            stages          merge mapsWith(valuesOfType(Stage))
            tests           merge mapsWith(valuesOfType(Test))

            path            use current
        }
    }
}
