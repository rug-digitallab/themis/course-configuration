package nl.rug.digitallab.configuration.logging

import nl.rug.digitallab.configuration.logging.entries.ExceptionLogEntry
import nl.rug.digitallab.configuration.logging.entries.SimpleLogEntry

/**
 * Logging interface for generic logging messages, not specialized to a
 * particular use case.
 */
interface ConfigurationLogger : Logger {
    /**
     * Log an error that occurred during configuration processing execution.
     * These are non-recoverable and indicate that something has gone
     * drastically wrong. As soon as an error occurs, no further processing
     * of the configuration will be done.
     *
     * For recoverable issues, use [configWarning] instead.
     *
     * @param location The logical location where the error occurred. For example,
     *          a filename or configuration path is recommended as this is user-facing.
     * @param message User-facing error message
     */
    fun <LocationT> configError(location: LocationT, message: String)
            = log(SimpleLogEntry(Logger.Type.ERROR, location, message))

    /**
     * Log a warning that occurred during configuration processing execution.
     * These are recoverable issues and only warn the user of potential
     * undesirable or unintended effects of their configuration - allowing
     * the user to correct their configuration.
     *
     * For unrecoverable issues, use [configError] instead.
     *
     * @param location The logical location where the warning occurred. For example,
     *          a filename or configuration path is recommended as this is user-facing.
     * @param message User-facing warning message
     */
    fun <LocationT> configWarning(location: LocationT, message: String)
            = log(SimpleLogEntry(Logger.Type.WARNING, location, message))

    /**
     * Log a debugging entry, explaining technical aspects of the code (flow)
     * to aid debugging for developers. These messages are normally not
     * user-facing - unless the user asks to show them.
     *
     * @param location The logical location of the debugging notice.
     * @param message The debugging message
     */
    fun <LocationT> configDebug(location: LocationT, message: String)
            = log(SimpleLogEntry(Logger.Type.DEBUG, location, message))

    /**
     * Helper function to create a log entry from an exception
     * without constructing an [ExceptionLogEntry] explicitly.
     *
     * It will set the log type to [Logger.Type.ERROR],
     * and use the message from the provided exception.
     */
    fun <LocationT> ConfigurationLogger.configLogException(
        location: LocationT,
        exception: Exception,
        message: String = exception.message.orEmpty(),
    ) = log(
        ExceptionLogEntry(
            type = Logger.Type.ERROR,
            location = location,
            exception = exception,
            message = message,
        )
    )
}
