package nl.rug.digitallab.configuration.inheritance.rulesets

import arrow.optics.Lens
import nl.rug.digitallab.configuration.inheritance.Traversal
import nl.rug.digitallab.configuration.inheritance.TraversalVisit
import nl.rug.digitallab.configuration.inheritance.merging.valuesOfType
import nl.rug.digitallab.configuration.inheritance.strategies.withMerge
import nl.rug.digitallab.configuration.inheritance.strategies.withVisit
import nl.rug.digitallab.configuration.inheritance.traversal
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.MutedLogger
import nl.rug.digitallab.configuration.models.*
import nl.rug.digitallab.configuration.models.Property.Companion.copyProperty
import nl.rug.digitallab.configuration.models.Property.Companion.set
import nl.rug.digitallab.configuration.models.schemas.*
import nl.rug.digitallab.configuration.models.schemas.repoman.RepomanCourse
import nl.rug.digitallab.configuration.models.schemas.repoman.path
import nl.rug.digitallab.configuration.models.schemas.themis.*
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.*

/**
 * This ruleset is intended to run as the very last ruleset on
 * a fresh [Course] instance. It makes sure all entities have
 * a proper [EntityPath] set, which can be used to match human-
 * readable paths to properties in our model.
 *
 * In the future, this will be extended to match paths with
 * properties.
 */
class PathConstructorRuleset : Ruleset<Course>() {
    // Builder function to visit all paths in a Stage definition
    private val pathStageVisit: TraversalVisit<Stage, EntityPath> = {
        inspect(Stage.path)
        visit(Stage.actions) {
            inspect(Action.path)
            visit(Action.files) { inspect(RuntimeFile.path) }
            visit(Action.limits) { inspect(Limits.path) }
        }
    }

    // Builder function to visit all paths in a Tests definition
    private val pathTestVisit: TraversalVisit<Test, EntityPath> = {
        inspect(Test.path)
        visit(Test.runtimeOverrides, pathStageVisit)
    }

    // Builder function to visit all paths in a Runtime definition
    private val pathProfileVisit: TraversalVisit<Profile, EntityPath> = {
        inspect(Profile.path)
        visit(Profile.environments) {
            inspect(Environment.path)
            visit(Environment.files) { inspect(RuntimeFile.path) }
            visit(Environment.limits) { inspect(Limits.path) }
        }

        visit(Profile.stages, pathStageVisit)
        visit(Profile.tests, pathTestVisit)
    }

    // Traversal instance that visits all paths in a Course configuration
    private val pathTraversal = traversal {
        inspect(Course.path)
        visit(Course.themis) {
            inspect(ThemisCourse.path)
            visit(ThemisCourse.profiles, pathProfileVisit)
            visit(ThemisCourse.tests, pathTestVisit)
            visit(ThemisCourse.assignments) {
                inspect(Assignment.path)
                visit(Assignment.profiles, pathProfileVisit)
                visit(Assignment.tests, pathTestVisit)
            }
            visit(ThemisCourse.folders) {
                inspect(Folder.path)
                visit(Folder.profiles, pathProfileVisit)
                visit(Folder.tests, pathTestVisit)
                visit(Folder.assignments) {
                    inspect(Assignment.path)
                    visit(Assignment.profiles, pathProfileVisit)
                    visit(Assignment.tests, pathTestVisit)
                }
                visitSelf(Folder.folders)
            }
        }
        visit(Course.repoman) {
            inspect(RepomanCourse.path)
        }
    }

    context(ConfigurationLogger)
    override fun applyImplementation(root: Property<Course>): Property<Course> {
        // Put the immutable function parameter in a local mutable variable
        var course = root

        configDebug(this@PathConstructorRuleset::class, "Initialising all entity paths, temporarily muting logging")

        with(MutedLogger()) {
            /**
             * Helper function that visits all maps in the [Traversal], setting
             * the [EntityPath] of each value to an initial value representing
             * the fact that these values are located inside a map.
             *
             * For example, calling this on a traversal over a Map<String, Action>
             * will set the path of each Action to be `actions[key]`.
             *
             * @param name The name of the maps being traversed
             * @param nameLens Lens to the name field of [T]
             * @param pathLens Lens to the path field of [T]
             *
             * @return [VisitingStrategy] to populate all paths in maps
             */
            fun <T : Any> Traversal<Course, ItemMap<T>>.populateMapPaths(
                name: String,
                nameLens: Lens<T, Property<String>>,
                pathLens: Lens<T, Property<EntityPath>>
            ) = withVisit { map ->
                map.mapValues { (key, value) ->
                    value.copyProperty {
                        nameLens set key
                        pathLens set EntityPath("\$$name[${key}]")
                    }
                }
            }

            // First, we visit all Maps in the configuration tree, and
            // ensure the contained elements have a name and path set
            // to the key of the respective Map item. This is not something
            // that the JSON parser can do for us.
            course = traverseProfiles { inspect(Profile.tests) }
                .populateMapPaths("tests", Test.name, Test.path)
                .apply(course)
            course = traverseProfiles { inspect(Profile.stages) }
                .populateMapPaths("stages", Stage.name, Stage.path)
                .apply(course)
            course = traverseProfiles { inspect(Profile.environments) }
                .populateMapPaths("environments", Environment.name, Environment.path)
                .apply(course)
            course = traverseStages { inspect(Stage.actions) }
                .populateMapPaths("actions", Action.name, Action.path)
                .apply(course)

            course = traverseThemis(
                courseBuilder = { visit(Course.themis) { inspect(ThemisCourse.profiles) } },
                folderBuilder = { inspect(Folder.profiles) },
                assignmentBuilder = { inspect(Assignment.profiles) },
            ).populateMapPaths("profiles", Profile.name, Profile.path)
                .apply(course)

            course = traverseThemis(
                courseBuilder = { visit(Course.themis) { inspect(ThemisCourse.tests) } },
                folderBuilder = { inspect(Folder.tests) },
                assignmentBuilder = { inspect(Assignment.tests) },
            ).populateMapPaths("tests", Test.name, Test.path)
                .apply(course)

            course = traverseThemis(
                courseBuilder = { visit(Course.themis) { visit(ThemisCourse.tests) { inspect(Test.runtimeOverrides) } } },
                folderBuilder = { visit(Folder.tests) { inspect(Test.runtimeOverrides) } },
                assignmentBuilder = { visit(Assignment.tests) { inspect(Test.runtimeOverrides) } },
            ).populateMapPaths("runtimeOverrides", Stage.name, Stage.path)
                .apply(course)

            // Then, we apply the path merging strategy that will
            // form all the final path values. This will concatenate all
            // paths in the configuration tree.
            val strategy = pathTraversal.withMerge { valuesOfType(EntityPath) }
            course = strategy.apply(course)
        }

        configDebug(this@PathConstructorRuleset::class, "Finished path initialisation, logging resumed")

        return course
    }
}
