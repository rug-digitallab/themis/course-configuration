package nl.rug.digitallab.configuration.models.schemas

import nl.rug.digitallab.configuration.inheritance.Traversal
import nl.rug.digitallab.configuration.inheritance.TraversalVisit
import nl.rug.digitallab.configuration.inheritance.traversal
import nl.rug.digitallab.configuration.models.schemas.themis.*
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.*
import kotlin.experimental.ExperimentalTypeInference

/**
 * Function that will build a [Traversal] that visits all [Course]s, [Folder]s,
 * and [Assignment]s. For each such instance, it will perform the traversal
 * actions specified in the respective builder.
 *
 * The visiting order is such that it will visit entities top-down, breadth-first:
 * starting from the [Course] node, first visiting all its [Assignment]s, then
 * recursing through all [Folder]s. In each [Folder], it will first visit all
 * [Assignment]s before recursing to the next [Folder]s.
 *
 * Note that the parameters are all annotated with [BuilderInference], which changes
 * Kotlin type inference behaviour to better fit Builder contexts. It allows Kotlin
 * to deduce the [PropertyT] type from the lambda body, saving us from explicitly
 * having to specify the generic type parameter for each function call.
 *
 * @param courseBuilder Builder function for the traversal actions to perform
 *                      on each [Course] instance
 * @param folderBuilder Builder function for the traversal actions to perform
 *                      on each [Folder] instance
 * @param assignmentBuilder Builder function for the traversal actions to perform
 *                          on each [Assignment] instance
 * @return A [Traversal] instance
 */
@OptIn(ExperimentalTypeInference::class)
fun <PropertyT> traverseThemis(
    @BuilderInference courseBuilder: TraversalVisit<Course, PropertyT>,
    @BuilderInference folderBuilder: TraversalVisit<Folder, PropertyT>,
    @BuilderInference assignmentBuilder: TraversalVisit<Assignment, PropertyT>,
): Traversal<Course, PropertyT> = traversal {
    courseBuilder()
    visit(Course.themis) {
        visit(ThemisCourse.assignments, assignmentBuilder)
        visit(ThemisCourse.folders) {
            folderBuilder()
            visit(Folder.assignments, assignmentBuilder)
            visitSelf(Folder.folders)
        }
    }
}

/**
 * Function that will build a [Traversal] that visits all [Assignment]s, and
 * for each such [Assignment], it will perform the traversal actions specified
 * in [traversalBuilder].
 *
 * The visiting order is such that it will visit [Assignment]s top-down, breadth-first:
 * starting from the [Course] node recursing through all [Folder]s. In each [Folder],
 * it will first visit all [Assignment]s before recursing to the next [Folder]s.
 *
 * @param traversalBuilder Builder function for the traversal actions to perform
 *                         on each [Assignment] instance
 * @return A [Traversal] instance
 */
fun <PropertyT> traverseAssignments(traversalBuilder: TraversalVisit<Assignment, PropertyT>)
    = traverseThemis({}, {}, traversalBuilder)

/**
 * Function that will build a [Traversal] that visits all [Profile]s, and
 * for each such [Profile], it will perform the traversal actions specified
 * in [traversalBuilder].
 *
 * The visiting order is such that it will visit [Profile]s top-down, breadth-first:
 * starting from the [Course] node, then visiting the top-level [Assignment]s, and
 * then recursing through all [Folder]s. In each [Folder], it will first visit all
 * [Assignment]s before recursing to the next [Folder]s.
 *
 * @param traversalBuilder Builder function for the traversal actions to perform
 *                         on each [Profile] instance
 * @return A [Traversal] instance
 */
fun <PropertyT> traverseProfiles(traversalBuilder: TraversalVisit<Profile, PropertyT>) = traversal {
    visit(Course.themis) {
        visit(ThemisCourse.profiles, traversalBuilder)
        visit(ThemisCourse.assignments) { visit(Assignment.profiles, traversalBuilder) }
        visit(ThemisCourse.folders) {
            visit(Folder.profiles, traversalBuilder)
            visit(Folder.assignments) { visit(Assignment.profiles, traversalBuilder) }
            visitSelf(Folder.folders)
        }
    }
}

/**
 * Function that will build a [Traversal] that visits all [Stage]s, and
 * for each such [Stage], it will perform the traversal actions specified
 * in [traversalBuilder].
 *
 * The visiting order is such that it will visit [Stage]s top-down, breadth-first:
 * starting from the [Course] node, then visiting the top-level [Assignment]s, and
 * then recursing through all [Folder]s. In each [Folder], it will first visit all
 * [Assignment]s before recursing to the next [Folder]s.
 *
 * Note that this will visit [Stage]s both in a [Profile], as well as the
 * overrides in a [Test].
 *
 * @param traversalBuilder Builder function for the traversal actions to perform
 *                         on each [Profile) instance
 * @return A [Traversal] instance
 */
fun <PropertyT> traverseStages(traversalBuilder: TraversalVisit<Stage, PropertyT>) = traversal {
    visit(Course.themis) {
        // Course level
        visit(ThemisCourse.profiles) { visit(Profile.stages, traversalBuilder) }
        visit(ThemisCourse.tests) { visit(Test.runtimeOverrides, traversalBuilder) }

        // Assignment level
        visit(ThemisCourse.assignments) {
            visit(Assignment.profiles) { visit(Profile.stages, traversalBuilder) }
            visit(Assignment.tests) { visit(Test.runtimeOverrides, traversalBuilder) }
        }

        // Folder level
        visit(ThemisCourse.folders) {
            visit(Folder.profiles) { visit(Profile.stages, traversalBuilder) }
            visit(Folder.tests) { visit(Test.runtimeOverrides, traversalBuilder) }

            visit(Folder.assignments) {
                visit(Assignment.profiles) { visit(Profile.stages, traversalBuilder) }
                visit(Assignment.tests) { visit(Test.runtimeOverrides, traversalBuilder) }
            }

            visitSelf(Folder.folders)
        }
    }
}
