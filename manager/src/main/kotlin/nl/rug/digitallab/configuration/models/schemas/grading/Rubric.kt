package nl.rug.digitallab.configuration.models.schemas.grading

import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.Property
import java.math.BigDecimal

@JsonSchema("./grading/rubric.schema.json")
data class Rubric (
    val name: Property<String>,
    @JsonProperty(defaultValue = "null")
    val description: Property<String?>,
    val categories: ListProperty<Category>,
    @JsonProperty(defaultValue = "null")
    val scalePointsTo: Property<BigDecimal?>,
)
