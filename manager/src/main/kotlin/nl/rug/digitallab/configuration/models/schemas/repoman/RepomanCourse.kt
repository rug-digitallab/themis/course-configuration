package nl.rug.digitallab.configuration.models.schemas.repoman

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp

@optics
@JsonSchema("./repoman/course.schema.json")
data class RepomanCourse (
    val platform: Property<Platform>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("repoman").toProp(),
) : ITraversable {
    override fun toNodeString(): String = "RepoMan settings for course"

    enum class Platform {
        GITLAB
    }

    companion object
}
