package nl.rug.digitallab.configuration.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.cfg.MapperBuilder
import io.quarkus.arc.Unremovable
import io.quarkus.jackson.ObjectMapperCustomizer
import jakarta.enterprise.context.Dependent
import nl.rug.digitallab.common.quarkus.jackson.customizers.BuilderCustomizer
import nl.rug.digitallab.configuration.jackson.deserializers.DigitalLabDeserializers

/**
 * An implementation of an [ObjectMapperCustomizer] to augment
 * instances of an [ObjectMapper] for use inside this configuration
 * project.
 *
 * This class is automatically used and injected by Jakarta and Quarkus
 */
@Dependent
@Unremovable
class ObjectMapperCustomization<BuilderT : MapperBuilder<*, BuilderT>> : BuilderCustomizer<BuilderT> {
    override fun customize(builder: BuilderT) {
        // Include custom deserializers
        builder.addModule(DigitalLabDeserializers)

        // Include custom problem handler to allow template properties
        builder.addHandler(TemplatePropertyHandler)
    }
}
