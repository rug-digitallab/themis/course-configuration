package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import nl.rug.digitallab.configuration.helpers.handleUnexpectedToken
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatterBuilder

/**
 * Custom deserializer for [Instant] entities, representing
 * date-time in UTC. Our JSON schema allows for passing either a
 * full ISO date-time string (parsable by Instant), or a custom
 * string without a timezone specified. In the latter case, we
 * need custom behaviour to parse appropriately.
 *
 * @param wrappedDeserializer The original deserializer for [Instant]
 */
class InstantDeserializer (
    wrappedDeserializer: JsonDeserializer<*>
) : WrappingDeserializer<Instant>(wrappedDeserializer, Instant::class) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Instant {
        if(p.currentToken != JsonToken.VALUE_STRING)
            ctxt.handleUnexpectedToken(Instant::class, p)

        return if("T" in p.valueAsString) {
            wrappedDeserializer.deserialize(p, ctxt) as Instant
        } else {
            val dateTimeFormatter = DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd HH:mm[:ss]").toFormatter()
            val localDateTime = LocalDateTime.parse(p.valueAsString, dateTimeFormatter)

            // We currently use Amsterdam as default timezone, but this probably needs to become
            // a configurable timezone down the line, either from the tenant or from the course
            // configuration itself! We also might want to consider using different timezones
            // for CLI use and server use?
            val timezone = ZoneId.of("Europe/Amsterdam")
            localDateTime.atZone(timezone).toInstant()
        }
    }

    override fun withDeserializer(deserializer: JsonDeserializer<*>) =
        InstantDeserializer(deserializer)
}
