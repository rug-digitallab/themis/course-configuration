package nl.rug.digitallab.configuration.inheritance.merging

import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger

/**
 * This interface can be implemented by the companion object of a type
 * to provide a "default merge implementation" for all instances of that
 * type.
 *
 * Kotlin does not have the concept of "static interface functions" or
 * anything similar; but the next best thing is to let the companion
 * object implement the interface. This achieves a very similar effect,
 * minus some generics limitations.
 */
interface Mergeable<T> {
    val merger: Merger<T>
}

/**
 * Selects the "default merge implementation" for the given type [T]
 * based on the passed interface implementation. For implementations
 * of the interface on a companion object, the containing class name
 * can be passed directly:
 *
 * ```kotlin
 * class Foo {
 *     companion object : Mergeable<Foo> { ... }
 * }
 *
 * // Then call
 * valuesOfType(Foo)
 * ```
 *
 * This function is also the main example of the limitations of the
 * "companion object implementing an interface" approach: if the main
 * type [T] could have implemented [Mergeable] directly with a static
 * method of some sorts, it would have been possible to access the
 * merger generically through [T] without an instance [type].
 *
 * @param type Implementation of the [Mergeable] interface providing the [Merger]
 */
fun <T> MergingEnvironment.valuesOfType(type: Mergeable<T>): Merger<T>
    = type.merger
