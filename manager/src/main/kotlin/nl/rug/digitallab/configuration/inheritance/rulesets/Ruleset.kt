package nl.rug.digitallab.configuration.inheritance.rulesets

import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.models.Property

/**
 * Abstract class representing a set of transformation or validation
 * rules for Configuration resolving. This is useful for packaging
 * and separating the wide range of validation and modification logic
 */
abstract class Ruleset<RootT> {
    context(ConfigurationLogger)
    protected abstract fun applyImplementation(root: Property<RootT>): Property<RootT>

    context(ConfigurationLogger)
    fun apply(root: Property<RootT>): Property<RootT> {
        configDebug(this@Ruleset::class, "Starting to apply ruleset ${this@Ruleset::class.simpleName}")

        val result = applyImplementation(root)

        configDebug(this@Ruleset::class, "Finished applying ruleset ${this@Ruleset::class.simpleName}")

        return result
    }
}
