package nl.rug.digitallab.configuration.jackson

import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.PathType
import com.networknt.schema.SchemaValidatorsConfig
import jakarta.enterprise.inject.Produces
import jakarta.inject.Inject
import jakarta.inject.Singleton
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.configuration.config.ResourceMappingConfig
import java.util.Locale
import kotlin.io.path.Path

typealias SchemaFactoryCustomizer = (JsonSchemaFactory.Builder) -> Unit

/**
 * Apply a custom configuration to the schema validator
 */
class SchemaValidationConfig {
    @Inject
    private lateinit var resourceMapping: ResourceMappingConfig

    @Singleton
    @Produces
    fun produceSchemaValidatorsConfig(): SchemaValidatorsConfig {
        return SchemaValidatorsConfig.builder()
            // According to the JSON Schema spec, a "format" annotation does not have to be validated
            // by default. We want to have it enabled by default. The "formal" way to do it would be
            // to define our own custom "meta schema" where we enable this feature, but that is very
            // cumbersome and probably won't lead to better editor support (quite the opposite).
            .formatAssertionsEnabled(true)

            // We want the validation output to be in English - both since all our products will
            // (for now) be in the English language, and for consistent unit test output. Without
            // setting this, the default will be whatever the machine locale is, leading to
            // unpredictable results.
            .locale(Locale.ENGLISH)

            // We choose the path type to be JSONPath (RFC 9535) as this is a more flexible standard
            // than the alternative (JSON Pointer). In addition, it uses periods to separate path
            // elements, rather than '/'. This helps distinguish a JSON path from a normal file path.
            .pathType(PathType.JSON_PATH)
            .build()
    }

    @Singleton
    @Produces
    fun produceSchemaFactoryCustomizer() : SchemaFactoryCustomizer = { builder ->
        // We map prefixes so that we map the default URI references in the schemas,
        // which refer to a domain name where the schemas will eventually be publicly
        // available, to our local drive, so we actually use our local (dev) schemas.
        builder.schemaMappers {
            it.mapPrefix(
                "https://digitallab.rug.nl/",
                getResource("${resourceMapping.schemas}/").toURI().toString(),
            )
        }
    }
}
