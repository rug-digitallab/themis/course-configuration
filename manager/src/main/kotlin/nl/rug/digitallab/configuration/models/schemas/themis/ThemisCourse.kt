package nl.rug.digitallab.configuration.models.schemas.themis

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.MapProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Profile
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Test

@optics
@JsonSchema("./themis/course.schema.json")
data class ThemisCourse (
    // Profiles and tests have no distinction between emptiness and
    // absence - the inherited set will always be expanded upon.
    @JsonProperty(defaultValue = "{}") val profiles: MapProperty<Profile>,
    @JsonProperty(defaultValue = "{}") val tests: MapProperty<Test>,

    // These are filled outside the parsing process
    // Kotlin can't deduce the list type when writing emptyList().prop
    @JsonIgnore val assignments: ListProperty<Assignment> = Property(emptyList()),
    @JsonIgnore val folders: ListProperty<Folder> = Property(emptyList()),

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("themis").toProp()
) : ITraversable {
    override fun toNodeString(): String = "Themis settings for course"

    companion object
}
