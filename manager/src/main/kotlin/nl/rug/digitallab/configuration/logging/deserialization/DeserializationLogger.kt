package nl.rug.digitallab.configuration.logging.deserialization

import com.fasterxml.jackson.core.JsonLocation
import nl.rug.digitallab.configuration.logging.Logger
import nl.rug.digitallab.configuration.models.Property

/**
 * Interface specifying the tracing functions that can be used during
 * deserialization of entities and models. This can be used to specifically
 * trace where properties are read from ([traceProperty]), or when a
 * default value was used because no other value was specified ([traceDefault]).
 */
interface DeserializationLogger : Logger {
    /**
     * Trace that the specified property was read from a particular location,
     * during Jackson deserialization. This way, users can deduce from where
     * a particular value was read.
     *
     * @param property The property that was read
     * @param location The location from where the property was read
     */
    fun traceProperty(property: Property<*>, location: JsonLocation)

    /**
     * Trace that the specified property was instantiated from a default
     * value, during Jackson deserialization. This way, users can deduce
     * that the given property was not specified in any file.
     *
     * @param property The property that was instantiated
     */
    fun traceDefault(property: Property<*>)
}
