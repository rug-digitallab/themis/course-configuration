package nl.rug.digitallab.configuration.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import nl.rug.digitallab.configuration.models.Property

/**
 * Simple serializer for [Property] instances, complementing the custom
 * deserializer. We will not export the property identity itself, we will
 * simply write the contents of the property directly - as if the property
 * was not there. This way, our internal use of properties remains an
 * implementation detail.
 */
class PropertySerializer : JsonSerializer<Property<*>>() {
    override fun serialize(value: Property<*>, gen: JsonGenerator, serializers: SerializerProvider) {
        gen.writeObject(value.get())
    }
}
