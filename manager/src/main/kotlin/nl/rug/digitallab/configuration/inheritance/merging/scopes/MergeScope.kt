package nl.rug.digitallab.configuration.inheritance.merging.scopes

import nl.rug.digitallab.configuration.Cloning.clone
import nl.rug.digitallab.configuration.inheritance.merging.MergingEnvironment
import nl.rug.digitallab.configuration.inheritance.merging.MergingEnvironmentMarker
import nl.rug.digitallab.configuration.logging.merging.MergingLogger
import nl.rug.digitallab.configuration.models.Property

/**
 * This class captures the "scope" in which a merge operation takes place:
 * it has an inherited and a current value to its disposal.
 *
 * The use of this class prevents confusion between inherited and
 * current values. If we were to use a normal function signature, it can be
 * very confusing to distinguish inherited and current values, as that can then
 * only be done based on their positions - especially in lambdas.
 *
 * Furthermore, this class ensures any tracing log entries will be populated
 * and stored appropriately.
 *
 * Merge "scopes" are specific to the current context (in terms of inherited
 * and current values), and log entries will be tied to that context. When
 * writing merging logic, it is often needed to change this context, for example
 * when merging a nested property or when changing the value type to be merged.
 *
 * For such scenarios, two methods are available:
 * - [scopeMerge] will narrow the current merge scope to the provided properties.
 *              Any tracing entries will also be linked to the new scope.
 * - [mergeValues] is for changing the type of the values to be merged, without
 *              changing the "context" - so tracing entries will appear as before.
 *
 * @param inherited The inherited value in the merge
 * @param current The current ("local") value in the merge
 *
 * @see [scopeMerge]
 * @see [mergeValues]
 */
@MergingEnvironmentMarker
abstract class MergeScope<out ItemT>(
    val inherited: ItemT,
    val current: ItemT,
) : MergingEnvironment, MergingLogger {
    /**
     * This method will narrow the scope of the current merge to the
     * provided properties. This is used for "nested mergers": for
     * example, when merging maps, upon a duplicate entry the entries
     * themselves are merged - this is a new merging scope, since the
     * values to be merged change and the tracing context changes.
     *
     * @param inherited The inherited property to be merged
     * @param current The current property to be merged
     * @param merger The [Merger] implementation to use
     * @return The resulting [T] after merging
     */
    abstract fun <T> scopeMerge(inherited: Property<T>, current: Property<T>, merger: Merger<T>): T

    /**
     * This method will change the type of the values to be merged,
     * without changing the context or scope. This can be used when
     * part of the merging logic should be "deferred" to another type.
     * For example, when merging two lists, the lists are transformed
     * into maps, and then the maps are merged with the existing logic.
     * In this case, we change the type of the values to be merged,
     * but we are still in the same context.
     *
     * @param inherited The inherited value to be merged
     * @param current The current value to be merged
     * @param merger The [Merger] implementation to use
     * @return The resulting [T] after merging
     */
    fun <T> mergeValues(inherited: T, current: T, merger: Merger<T>): T =
        // Due to type safety, we have to create a new merge scope - however,
        // we use a simple delegating scope that will delegate all tracing calls
        // to the parent scope - this way, the context does not change, and this
        // can serve as a default implementation for all scopes.
        merger(DelegatingMergeScope(inherited, current, this))
}

/**
 * A [Merger] represents a method implementing the merge process
 * of two entities of type `T`. The method is passed a [MergeScope],
 * carrying two fields `inherited` and `current`, of type `T`.
 * The implementation of the merger should return an appropriate
 * new, merged value, and is responsible for properly tracking any
 * merge decisions taken.
 *
 * The interface [MergingEnvironment] carries some methods that
 * might aid in the implementation of a merger; in particular, it
 * is possible to call `mergeByProperty`, for a property-level
 * merge process.
 *
 * Of particular importance is the proper management of property
 * identities. Any value that is taken from outside the merge
 * scope, or from the `inherited` value, should be properly cloned
 * before use in the merged result. This is done by calling the
 * `clone()` extension method.
 *
 * @see [MergingEnvironment]
 * @see [nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.mergeByProperty]
 * @see [nl.rug.digitallab.configuration.Cloning.clone]
 */
typealias Merger<T> = MergeScope<T>.() -> T
