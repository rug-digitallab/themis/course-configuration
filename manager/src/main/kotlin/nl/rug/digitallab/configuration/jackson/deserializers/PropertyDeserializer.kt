package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.ContextualDeserializer
import com.fasterxml.jackson.databind.util.AccessPattern
import nl.rug.digitallab.configuration.helpers.reportBadDefinition
import nl.rug.digitallab.configuration.jackson.findInjectable
import nl.rug.digitallab.configuration.logging.deserialization.DeserializationLogger
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp

/**
 * Custom deserializer for [Property] instances. Since [Property]s are
 * considered to be implementation details of the configuration model,
 * deserializing them should be "transparent": the JSON/YAML source
 * should look as if there are no [Property]s.
 *
 * Therefore, this deserializer will simply defer all deserialization
 * calls to the deserializers of the underlying types. In addition,
 * this deserializer will perform tracing for these values: every time
 * a [Property] is instantiated, its source location from JSON/YAML will
 * be tracked and logged for tracing.
 *
 * This deserializer also handles default values - normally, a default
 * value would be specified by providing a default value in the primary
 * constructor of the entity, but this would not allow us to do proper
 * tracing of default values. By removing the default value from the
 * constructor, and instead providing an annotation with the default
 * value, we can pick it up here in the [getAbsentValue] method, and
 * add appropriate tracing.
 *
 * Tracing is handled by obtaining a [DeserializationLogger] from the
 * injectables in the [DeserializationContext]. To use the tracing,
 * a logger instance should be provided to Jackson as an injectable value.
 */
class PropertyDeserializer() : JsonDeserializer<Property<*>>(), ContextualDeserializer {
    private lateinit var valueType: JavaType
    private var property: BeanProperty? = null

    // Secondary constructor to be used when contextualizing this deserializer
    constructor(valueType: JavaType, property: BeanProperty?) : this() {
        this.valueType = valueType
        this.property = property
    }

    // Indicate that the null "value" is not constant, but is a new one every time
    override fun getNullAccessPattern() = AccessPattern.DYNAMIC

    override fun getNullValue(ctxt: DeserializationContext): Property<*> {
        val location = ctxt.parser.currentTokenLocation()
        val result = null.toProp()

        ctxt.findLogger()?.traceProperty(result, location)

        return result
    }

    // Upon an absent value, we will defer to the default value as specified in the
    // JsonProperty annotation. If this is not specified, that is an error.
    override fun getAbsentValue(ctxt: DeserializationContext): Property<*> {
        val defaultString = property?.metadata?.defaultValue
            ?: ctxt.reportBadDefinition(
                Property::class,
                "No default value specified for property ${property?.fullName}, it is required"
            )

        // The default value is represented by a JSON string. We should parse this JSON to
        // obtain our default value. We have to first parse the string to a tree, then read
        // that tree into an instance.
        val tree = ctxt.parser.codec.factory.createParser(defaultString).readValueAsTree<JsonNode>()

        val defaultValue = (ctxt.parser.codec as ObjectMapper).treeToValue<Any>(tree, valueType)
        // The following code is the "better" way of doing it without magic casts, but
        // there is a bug in Jackson preventing this from working properly for "null" values.
        // This will be fixed in 2.19: https://github.com/FasterXML/jackson-databind/issues/4934
        // TODO update once Jackson 2.19 is released and incorporated into Quarkus (#52)
        //val defaultValue = ctxt.readTreeAsValue<Any>(tree, valueType)

        val result = defaultValue.toProp()

        ctxt.findLogger()?.traceDefault(result)

        return result
    }

    override fun createContextual(ctxt: DeserializationContext, property: BeanProperty?): JsonDeserializer<*> {
        // "property" might not be available for us to find the inner type, for example
        // when deserializing a List of Properties. However, the DeserializationContext
        // will always have this
        val wrapperType = ctxt.contextualType
        val valueType = wrapperType.containedType(0)
        return PropertyDeserializer(valueType, property)
    }

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Property<*> {
        val location = p.currentTokenLocation()
        val result = ctxt.readValue<Any>(p, valueType).toProp()

        ctxt.findLogger()?.traceProperty(result, location)

        return result
    }

    // Helper function to deal with typing
    private fun DeserializationContext.findLogger(): DeserializationLogger? = this.findInjectable()
}
