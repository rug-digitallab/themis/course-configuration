package nl.rug.digitallab.configuration.inheritance.strategies

import nl.rug.digitallab.configuration.inheritance.Traversal
import nl.rug.digitallab.configuration.inheritance.strategies.ValidationStrategy.Companion.create
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.strategies.StrategyLogger
import nl.rug.digitallab.configuration.models.Property

/**
 * Strategy implementation for validating property values. Given
 * a decision function, this strategy will determine whether all
 * visited properties are valid or not.
 *
 * Typical usage will not implement this abstract class directly,
 * but will make use of the [create] or [withValidation] helper functions.
 *
 * @param PropertyT The type of values that will be inspected in this strategy
 */
abstract class ValidationStrategy<RootT, PropertyT> : Strategy<RootT, PropertyT, ValidationStrategy.Accumulator<PropertyT>>() {
    override val strategyName: String
        get() = ValidationStrategy::class.simpleName.orEmpty()

    /**
     * The decision function used to decide whether a property
     * value is valid or not.
     *
     * @sample [isNotBlank] for [String] properties
     *
     * @param value The current value
     * @return Whether this value is valid
     */
    context(StrategyLogger<PropertyT>)
    protected abstract fun isValid(value: PropertyT): Boolean

    /**
     * This function will be called for each property that is visited in the
     * traversal. It will check whether the given value is valid and update
     * the accumulator accordingly.
     *
     * It will always return the current value, to prevent updates to the
     * object.
     *
     * @param value The current value
     * @param accumulator The current accumulator state
     */
    context(StrategyLogger<PropertyT>)
    override fun visit(value: PropertyT, accumulator: Accumulator<PropertyT>): PropertyT {
        val validity = isValid(value)
        accumulator.currentState = accumulator.currentState && validity

        strategyDebug("Value $value is ${if (validity) "" else "not "}valid")

        return value
    }

    /**
     * Entrypoint to start running the [ValidationStrategy]. For a given
     * [target], it will run the traversal and check validity of all
     * visited properties along the way. Returns whether all visited properties
     * are valid.
     *
     * @param target The root element to start validation for
     * @return Whether all visited properties are valid
     */
    context(ConfigurationLogger)
    fun validate(target: Property<RootT>): Boolean {
        // Our accumulator stores the current state on the "way down",
        // but also combines all information on the "way up" again.
        // The final validation result will be in this accumulator.
        val accumulator = Accumulator<PropertyT>(true)
        execute(target, accumulator)

        return accumulator.currentState
    }

    /**
     * Simple [Strategy.Accumulator] implementation, keeping track of
     * the validity during the validation process.
     */
    data class Accumulator<PropertyT>(var currentState: Boolean) :
            Strategy.Accumulator<Accumulator<PropertyT>, PropertyT>() {
        override fun fork(): Accumulator<PropertyT> = copy()
        override fun merge(child: Accumulator<PropertyT>) {
            currentState = currentState && child.currentState
        }
    }

    companion object {
        /**
         * Helper function that will create an instance of a [ValidationStrategy]
         * with the specified decision function and traversal. It is the recommended
         * starting point for simple [ValidationStrategy] usages.
         *
         * @param validationFunction Decides whether the given value is valid
         * @param traversal The traversal to perform the strategy on
         * @return A [ValidationStrategy] instance
         */
        fun <RootT, PropertyT> create(
            validationFunction: context(StrategyLogger<PropertyT>) (PropertyT) -> Boolean,
            traversal: Traversal<RootT, PropertyT>
        ): ValidationStrategy<RootT, PropertyT> = object : ValidationStrategy<RootT, PropertyT>() {
            context(StrategyLogger<PropertyT>)
            override fun isValid(value: PropertyT) = validationFunction(this@StrategyLogger, value)
            override val traversal = traversal
        }
    }
}

/**
 * Creates an instance of [ValidationStrategy] with the current
 * [Traversal] and the specified validation function.
 *
 * @param validationFunction Decides whether the given value is valid
 * @return A [ValidationStrategy] instance
 */
fun <RootT, PropertyT> Traversal<RootT, PropertyT>.withValidation(
    validationFunction: context(StrategyLogger<PropertyT>) (PropertyT) -> Boolean
) = create(validationFunction, this)
