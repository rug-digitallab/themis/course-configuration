package nl.rug.digitallab.configuration.logging.deserialization

import com.fasterxml.jackson.core.JsonLocation
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.Logger
import nl.rug.digitallab.configuration.logging.entries.TracingEntry
import nl.rug.digitallab.configuration.models.Property

/**
 * Simple [DeserializationLogger] implementation that will relay
 * all tracing entries to the provided [ConfigurationLogger].
 */
context(ConfigurationLogger)
class RelayingDeserializationLogger : DeserializationLogger, Logger by this {
    override fun traceProperty(property: Property<*>, location: JsonLocation) {
        log(TracingEntry.Read(property.identity, location.toPropertySourceLocation(), property.get()))
    }

    override fun traceDefault(property: Property<*>) {
        log(TracingEntry.Default(property.identity, property.get()))
    }
}
