package nl.rug.digitallab.configuration.logging.entries

import nl.rug.digitallab.configuration.logging.Logger
import java.util.UUID

/**
 * [LogEntry] subtype specifically for tracing purposes. All subtypes of
 * [TracingEntry] represent a particular "action" taken on a particular
 * property, during configuration processing. This base type carries fields
 * [location], [newValue], and [message], serving as the basic data that
 * should be provided by all tracing entries. Specific subtypes can (and
 * should) of course provide more detailed information.
 */
sealed interface TracingEntry<T> : LogEntry<UUID> {
    override val type get() = Logger.Type.EXPLAIN
    val newValue: T

    /**
     * [TracingEntry] type for an inheritance action. When merging an
     * inherited and current value, the inherited value was used. This
     * logs which value was inherited, from where, and what the current
     * value was, in addition to the new value.
     *
     * __NOTE__ that the [newValue] cannot be determined automatically,
     * as inheriting a value typically involves cloning the inherited
     * value, thus providing a different identity to the value. So while
     * [inheritedValue] and [newValue] will be semantically equal, their
     * properties will have different identities.
     */
    data class Inherit<T>(
        override val location: UUID,
        val localValue: T,
        val inheritedFrom: UUID,
        val inheritedValue: T,
        override val newValue: T,
        override val message: String,
    ): TracingEntry<T>

    /**
     * [TracingEntry] type for an override action. When merging an
     * inherited and current value, the current value was used, "overriding"
     * the inherited value. This class logs which value was used to
     * override, and which value would have been inherited and from
     * where.
     *
     * __NOTE__ that the [newValue] can be determined automatically,
     * as the current value is simply maintained.
     */
    data class Override<T>(
        override val location: UUID,
        val localValue: T,
        val inheritedFrom: UUID,
        val inheritedValue: T,
        override val message: String,
    ): TracingEntry<T> {
        override val newValue = localValue
    }

    /**
     * [TracingEntry] type for a merge action. When merging an
     * inherited and current value, the two were merged in a
     * non-trivial way (i.e. not an override and not an inherit).
     * This class logs which values were "current" and inherited,
     * and what the merged value is. The [message] should be a
     * sufficiently-detailed explanation of what happened during
     * the merge.
     */
    data class Merge<T>(
        override val location: UUID,
        val localValue: T,
        val inheritedFrom: UUID,
        val inheritedValue: T,
        override val newValue: T,
        override val message: String,
    ): TracingEntry<T>

    /**
     * [TracingEntry] type for an in-place transformation of data.
     * This class logs the old and new values, and the [message]
     * should be a sufficiently-detailed explanation of why the
     * value changed.
     */
    data class Transform<T>(
        override val location: UUID,
        val oldValue: T,
        override val newValue: T,
        override val message: String,
    ): TracingEntry<T>

    /**
     * [TracingEntry] type for a read-from-file, indicating that
     * the property was read from the given file at the given
     * source location.
     */
    data class Read<T, LocationT>(
        override val location: UUID,
        val sourceLocation: LocationT,
        override val newValue: T,
    ): TracingEntry<T> {
        override val message = "Read from file"
    }

    /**
     * [TracingEntry] type for a default value instantiation,
     * indicating that no value was found and that consequently,
     * a default value was instantiated.
     */
    data class Default<T>(
        override val location: UUID,
        override val newValue: T,
    ): TracingEntry<T> {
        override val message = "No value was provided, using default value instead"
    }

    /**
     * [TracingEntry] type for a clone operation, indicating that
     * the current property was cloned from some other location. The
     * reason for the clone can be indicated, but will typically be
     * unknown. Typically, the clone was performed as part of some other
     * merge/inherit operation at a higher level, so log analysis should
     * find the "reason" for the clone at higher levels in the
     * configuration tree.
     */
    data class Clone<T>(
        override val location: UUID,
        override val newValue: T,
        val clonedLocation: UUID,
        override val message: String,
    ): TracingEntry<T>
}
