package nl.rug.digitallab.configuration.logging

import nl.rug.digitallab.configuration.logging.entries.LogEntry

/**
 * A [Logger] provides logging capabilities to functions that deal
 * with configuration processing and management. We do not use
 * "native" logging functionality, as a large part of the logging
 * during configuration should be captured and stored in a structured
 * way, as to provide insight to the user in the configuration processing.
 *
 * Implementations of this interface might decide to just dump all
 * entries to a native logger, for example during unit testing. Other
 * implementations might store a list of all entries to later pretty-
 * print them to the console of the user.
 */
interface Logger {
    /**
     * Simplest log function, to directly put a complete log entry into
     * the logs. Typically, more specialized log functions are available
     * that will call this function internally.
     *
     * @param entry The log entry to insert into the logs
     */
    fun log(entry: LogEntry<*>)

    enum class Type {
        /**
         * An error log entry indicates that a non-recoverable issue has
         * occurred during configuration processing - something has gone
         * drastically wrong. As soon as such an error occurs, no
         * further processing should be done.
         *
         * For recoverable issues, use [WARNING] instead.
         */
        ERROR,

        /**
         * A warning log entry indicates that a recoverable issue has
         * occurred during configuration processing - something unexpected
         * took place that might have undesirable or unintended effects
         * on the user configuration. These entries allow the user to
         * correct their configuration if necessary.
         *
         * For unrecoverable issues, use [ERROR] instead.
         */
        WARNING,

        /**
         * A debug log entry is mainly intended for developers, explaining
         * technical aspects of the code (flow) and aiding debugging. These
         * items are normally not shown to the user.
         */
        DEBUG,

        /**
         * An explanation log entry is intended to explain specific decisions
         * taken during configuration processing. These items serve to help
         * the user understand how their configuration is transformed and
         * modified during processing.
         *
         * A rule of thumb is to log an explanation entry for every modification
         * of user-configurable fields in the configuration tree.
         */
        EXPLAIN,
    }
}
