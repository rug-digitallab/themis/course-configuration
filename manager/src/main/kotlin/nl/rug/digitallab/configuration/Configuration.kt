package nl.rug.digitallab.configuration

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.configuration.inheritance.rulesets.PathConstructorRuleset
import nl.rug.digitallab.configuration.inheritance.rulesets.ProfileRuleset
import nl.rug.digitallab.configuration.inheritance.rulesets.VisibilityRuleset
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.entries.LogEntry
import nl.rug.digitallab.configuration.logging.entries.TracingEntry
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.schemas.Course
import nl.rug.digitallab.configuration.parsing.CourseParser
import java.nio.file.Path

@ApplicationScoped
class Configuration {
    @Inject
    private lateinit var parser: CourseParser

    fun fromFiles(location: Path): Property<Course>? {
        val logger = Logger()
        var course: Property<Course>

        with(logger) {
            configDebug(this@Configuration::class, "Start of configuration parsing")

            course = parser.parseCourse(location) ?: return null

            configDebug(this@Configuration::class, "Finished parsing of configuration")
            configDebug(this@Configuration::class, "Start of configuration ruleset processing")

            course = VisibilityRuleset().apply(course)
            course = ProfileRuleset().apply(course)
            course = PathConstructorRuleset().apply(course)

            configDebug(this@Configuration::class, "Finished ruleset processing")
        }

        val configByProperty = logger.entries.filterIsInstance<TracingEntry<*>>().groupBy { it.location }

        return course
    }

    /**
     * Simple [ConfigurationLogger] implementation that just stores
     * all log entries in a big list for now.
     */
    private class Logger : ConfigurationLogger {
        val entries: MutableList<LogEntry<*>> = mutableListOf()

        override fun log(entry: LogEntry<*>) { entries.add(entry) }
    }
}
