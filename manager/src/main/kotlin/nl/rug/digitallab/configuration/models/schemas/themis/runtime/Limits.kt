package nl.rug.digitallab.configuration.models.schemas.themis.runtime

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.Mergeable
import nl.rug.digitallab.configuration.inheritance.merging.overrideOrInherit
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import java.time.Duration

@optics
@JsonSchema("./themis/runtime/limits.schema.json")
data class Limits (
    // For each limit, null means to inherit the limit, while an
    // explicit value overrides the inherited value.
    @JsonProperty(defaultValue = "null") val time: Property<Duration?>,
    @JsonProperty(defaultValue = "null") val cores: Property<Float?>,
    @JsonProperty(defaultValue = "null") val processes: Property<Int?>,
    @JsonProperty(defaultValue = "null") val memory: Property<ByteSize?>,
    @JsonProperty(defaultValue = "null") val disk: Property<ByteSize?>,
    @JsonProperty(defaultValue = "null") val output: Property<ByteSize?>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("\$limits").toProp(),
) : ITraversable {
    override fun toNodeString(): String = "Limits"

    companion object : Mergeable<Limits> {
        override val merger = mergeByProperty<Limits> {
            time        merge overrideOrInherit()
            cores       merge overrideOrInherit()
            processes   merge overrideOrInherit()
            memory      merge overrideOrInherit()
            disk        merge overrideOrInherit()
            output      merge overrideOrInherit()

            path        use current
        }
    }
}
