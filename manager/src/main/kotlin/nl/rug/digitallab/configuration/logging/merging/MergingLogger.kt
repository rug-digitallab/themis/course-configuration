package nl.rug.digitallab.configuration.logging.merging

import nl.rug.digitallab.configuration.logging.Logger

/**
 * Interface specifying the logging functions available during merging.
 * These are specific to merging, as providing inherit, override, and
 * merge items - in addition to in-place transform.
 *
 * Implementations of this interface should keep track of the context
 * in which the merge is happening, to augment the log messages with
 * this context.
 */
interface MergingLogger : Logger {
    /**
     * Log a tracing entry for an inheritance action, serving as
     * explanation of what happened during merger execution. This
     * message is user-facing and will be used to debug or analyse
     * the final configuration.
     *
     * @param message The explanation of why a value was inherited
     */
    fun traceInherit(message: String)

    /**
     * Log a tracing entry for an override action, serving as
     * explanation of what happened during merger execution. This
     * message is user-facing and will be used to debug or analyse
     * the final configuration.
     *
     * The "override" in this context means that a value was _not_
     * inherited, but instead overridden (by a local value).
     *
     * @param message The explanation of why a value was overridden
     */
    fun traceOverride(message: String)

    /**
     * Log a tracing entry for a complex merge action, serving as
     * explanation of what happened during merger execution. This
     * message is user-facing and will be used to debug or analyse
     * the final configuration.
     *
     * This is meant for non-trivial merge actions, so not an override
     * or inheritance. The message should be a sufficiently-detailed
     * explanation of what happened during the merge.
     *
     * @param message The explanation of why a value was inherited
     */
    fun traceMerge(message: String)

    /**
     * Log a tracing entry for an in-place modification, serving as
     * "explanation" of what happened to properties during execution.
     * This message is user-facing and will be used to debug or
     * analyse the final configuration.
     *
     * @param message The explanation of why the value changed
     */
    fun traceTransform(message: String)
}
