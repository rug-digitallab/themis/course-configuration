package nl.rug.digitallab.configuration.parsing

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import arrow.optics.copy
import jakarta.inject.Inject
import jakarta.inject.Singleton
import nl.rug.digitallab.configuration.exceptions.SchemaValidationException
import nl.rug.digitallab.configuration.helpers.has
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.deserialization.DeserializationLogger
import nl.rug.digitallab.configuration.logging.deserialization.RelayingDeserializationLogger
import nl.rug.digitallab.configuration.logging.entries.SchemaValidationLogEntry
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.copyProperty
import nl.rug.digitallab.configuration.models.Property.Companion.insideProperty
import nl.rug.digitallab.configuration.models.Property.Companion.notNull
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.Property.Companion.set
import nl.rug.digitallab.configuration.models.schemas.Course
import nl.rug.digitallab.configuration.models.schemas.themis
import nl.rug.digitallab.configuration.models.schemas.themis.*
import java.io.UnsupportedEncodingException
import java.nio.file.Path
import kotlin.io.path.isDirectory
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.nameWithoutExtension

/**
 * Class handling the parsing and interpretation of an entire configuration
 * directory. It parses all configuration files inside the given directory,
 * and puts them all in one big [Course] structure.
 */
@Singleton
class CourseParser {
    @Inject
    private lateinit var parser: EntityParser

    /**
     * Given a path to a directory, this function will parse the navigate
     * directory tree and parse all relevant configuration files.
     *
     * @param location Path of the directory to parse
     * @return The parsed course, or null when no course is found
     */
    context(ConfigurationLogger)
    fun parseCourse(location: Path): Property<Course>? {
        if(!location.isDirectory())
            return null

        // Find the main course configuration file
        val entries = location.listDirectoryEntries()
        val courseFile = entries.find { it.nameWithoutExtension == "course" }
            ?: return null

        configDebug(location, "Parsing course configuration files")

        // Parse the course file
        var course = parseAndValidateFile<Course>(courseFile)?.toProp()
            ?: return null

        // Set service-specific properties
        if(course().services() has Course.Service.THEMIS && course().themis() != null) {
            configDebug(location, "Course has Themis service, finding assignments and folders")

            val subEntries = parseLocation(location)
            course = course.copyProperty {
                insideProperty(Course.themis.notNull) {
                    ThemisCourse.assignments set subEntries.assignments
                    ThemisCourse.folders set subEntries.folders
                }
            }
        }

        return course
    }

    /**
     * Given a path to a directory, tries to interpret this directory
     * as a [Folder], parsing the appropriate configuration files and
     * any nested [Folder]s and [Assignment]s
     */
    context(ConfigurationLogger)
    private fun parseFolder(location: Path): Folder? {
        if(!location.isDirectory())
            return null

        // Find the folder configuration file
        val entries = location.listDirectoryEntries()
        val folderFile = entries.find { it.nameWithoutExtension == "folder" }
            ?: return null

        configDebug(location, "Parsing folder configuration files")

        // Parse the folder file
        var folder = parseAndValidateFile<Folder>(folderFile)
            ?: return null

        // Set additional properties and descendants
        val subEntries = parseLocation(location)
        folder = folder.copy {
            Folder.assignments set subEntries.assignments
            Folder.folders set subEntries.folders
        }

        return folder
    }

    /**
     * Given a path to a directory, tries to interpret this directory
     * as an [Assignment], parsing the appropriate configuration files
     * and any auxiliary resources
     */
    context(ConfigurationLogger)
    private fun parseAssignment(location: Path): Assignment? {
        if (!location.isDirectory())
            return null

        // Find the assignment configuration file
        val entries = location.listDirectoryEntries()
        val assignmentFile = entries.find { it.nameWithoutExtension == "assignment" }
            ?: return null

        configDebug(location, "Parsing assignment configuration files")

        // Parse the assignment file
        return parseAndValidateFile<Assignment>(assignmentFile)
    }

    /**
     * Given a path to a directory, tries to interpret this directory
     * as either a [Folder] or an [Assignment], depending on the files
     * present. It then returns the appropriate type.
     */
    context(ConfigurationLogger)
    private fun parseDirectory(location: Path): Either<Folder, Assignment>? {
        if(!location.isDirectory())
            return null

        configDebug(location, "Trying to parse folder or course in directory")

        val entries = location.listDirectoryEntries()
        if(entries.any { it.nameWithoutExtension == "assignment" })
            return parseAssignment(location)?.right()

        if(entries.any { it.nameWithoutExtension == "folder" })
            return parseFolder(location)?.left()

        return null
    }

    /**
     * Given a path to a directory, tries to compile a list of
     * sub-folders and sub-assignments located in subfolders of
     * the given directory.
     */
    context(ConfigurationLogger)
    private fun parseLocation(location: Path): CourseLevel {
        if(!location.isDirectory())
            return CourseLevel()

        configDebug(location, "Parsing child folders and assignments")

        return location.listDirectoryEntries()
            .mapNotNull { parseDirectory(it) }
            .fold(CourseLevel()) { level, result ->
                result.onLeft { level.folders.add(it.toProp()) }
                result.onRight { level.assignments.add(it.toProp()) }
                return@fold level
            }
    }

    /**
     * Wrapper function around [EntityParser.parseAndValidateFile] that will
     * handle logging of the various exceptions that can occur during entity
     * parsing and validation.
     */
    context(ConfigurationLogger)
    private inline fun <reified T : Any> parseAndValidateFile(file: Path) : T? {
        try {
            return with(object : DeserializationLogger by RelayingDeserializationLogger() {}) {
                parser.parseAndValidateFile<T>(file)
            }
        } catch (ex: Exception) {
            when (ex) {
                is SchemaValidationException -> ex.validationResult.forEach {
                    log(
                        SchemaValidationLogEntry(
                            location = file,
                            validationMessage = it,
                        )
                    )
                }

                is UnsupportedEncodingException -> configLogException(
                    location = file,
                    exception = ex,
                    message = "File extension ${ex.message} is not supported",
                )

                // Other exception types should not happen - if they do, we cannot give
                // a detailed, user-friendly error message, so we keep the error generic.
                else -> configLogException(
                    location = file,
                    exception = ex,
                    message = "Internal validation error occurred"
                )
            }

            return null
        }
    }

    private data class CourseLevel (
        val folders: MutableList<Property<Folder>> = mutableListOf(),
        val assignments: MutableList<Property<Assignment>> = mutableListOf(),
    )
}
