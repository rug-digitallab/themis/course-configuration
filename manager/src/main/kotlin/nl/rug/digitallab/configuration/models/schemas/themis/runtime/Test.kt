package nl.rug.digitallab.configuration.models.schemas.themis.runtime

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.Mergeable
import nl.rug.digitallab.configuration.inheritance.merging.mapsWith
import nl.rug.digitallab.configuration.inheritance.merging.overrideOrInherit
import nl.rug.digitallab.configuration.inheritance.merging.valuesOfType
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.MapProperty
import nl.rug.digitallab.configuration.models.NullableListProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp

@optics
@JsonSchema("./themis/runtime/test.schema.json")
data class Test (
    // Tests only ever occur inside a map, or are provided implicitly through files.
    // Consequently, this name will be taken from the map key or file name.
    @JsonIgnore
    val name: Property<String> = "".toProp(),

    // There is no semantic difference between absence and emptiness of these
    // items, since each setting is specified separately. Therefore, this
    // field is not nullable.
    @JsonProperty(defaultValue = "{}") val visibility: Property<TestVisibility>,

    // For these, null means to inherit the property, while a value
    // (also empty string) overrides the inherited value.
    @JsonProperty(defaultValue = "null") val hint: Property<String?>,
    @JsonProperty(defaultValue = "null") val note: Property<String?>,
    @JsonProperty(defaultValue = "null") val group: Property<String?>,

    // For the runtime overrides, there is no distinction between absence and
    // emptiness. Clearing entries from these maps is achieved by explicitly
    // specifying null for a key.
    @JsonProperty(defaultValue = "{}") val runtimeOverrides: MapProperty<Stage>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("").toProp(), // to be updated once the name is known
) : ITraversable {
    override fun toNodeString(): String = "Test \"$name\""

    @optics
    data class TestVisibility (
        @JsonProperty(defaultValue = "null") val name: Property<Boolean?>,
        @JsonProperty(defaultValue = "null") val hint: Property<Boolean?>,
        @JsonProperty(defaultValue = "null") val status: Property<Boolean?>,
        @JsonProperty(defaultValue = "null") val resources: NullableListProperty<String>,
        @JsonProperty(defaultValue = "null") val limits: Property<Boolean?>,
    ) {
        companion object : Mergeable<TestVisibility> {
            override val merger = mergeByProperty<TestVisibility> {
                name        merge overrideOrInherit()
                hint        merge overrideOrInherit()
                status      merge overrideOrInherit()
                resources   merge overrideOrInherit()
                limits      merge overrideOrInherit()
            }
        }
    }

    companion object : Mergeable<Test> {
        override val merger = mergeByProperty<Test> {
            name        use current

            visibility  merge valuesOfType(TestVisibility)

            hint        merge overrideOrInherit()
            note        merge overrideOrInherit()
            group       merge overrideOrInherit()

            runtimeOverrides merge mapsWith(valuesOfType(Stage))

            path        use current
        }
    }
}
