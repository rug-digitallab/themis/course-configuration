package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.BeanProperty
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.ContextualDeserializer
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import nl.rug.digitallab.configuration.helpers.*

/**
 * A custom deserializer for [ReferenceOr] items. These represent
 * either string-form references to an entity, or are the entity itself.
 * We need to check which is the case and return either [Value] or
 * [Reference] as appropriate.
 *
 * *Note* that this is different from [StringOrObjectDeserializer],
 * as the latter is a [WrappingDeserializer] that can accept *string
 * representations of* an entity, while in the case of [ReferenceOr]
 * we deal with *references to* entities.
 */
class ReferenceOrDeserializer : StdDeserializer<ReferenceOr<*>>(ReferenceOr::class.java), ContextualDeserializer {
    private lateinit var valueDeserializer : JsonDeserializer<*>

    /**
     * Obtains a deserializer for entities of the "contained" type of [ReferenceOr],
     * to use later on when actually deserializing entities.
     */
    override fun createContextual(ctxt: DeserializationContext, property: BeanProperty): JsonDeserializer<*> {
        if(property.type.rawClass != ReferenceOr::class.java)
            ctxt.reportBadDefinition(ReferenceOr::class, "Incorrect class")

        valueDeserializer = ctxt.findContextualValueDeserializer(property.type.containedType(0), property)
        return this
    }

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): ReferenceOr<*> = when(p.currentToken) {
        // We "abuse" type erasure here. Reference does not care about the type parameter, and since we don't
        // know the target type at this point, our only choice is to instantiate using Any. At runtime, these
        // types are removed anyway, and it's all the same.
        JsonToken.VALUE_STRING -> Reference<Any>(ref = p.valueAsString)
        JsonToken.START_OBJECT -> Value(valueDeserializer.deserialize(p, ctxt))
        else -> ctxt.handleUnexpectedToken(ReferenceOr::class, p)
    }
}