package nl.rug.digitallab.configuration.models

import nl.rug.digitallab.configuration.inheritance.merging.Mergeable
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import java.nio.file.InvalidPathException

/**
 * This class serves as a unique locator for entities and fields within
 * a course configuration. The path format consists of three parts:
 * [entities], [elements], and a [field]. [entities] are separated by
 * a normal '/', optionally followed by a '$' and the [elements] that
 * are separated by ':'. Then, finally, the [field] is separated by a '.'.
 *
 * This format is also used within the JSON configuration to refer to
 * other values and instances, and is meant to provide a unified way
 * to refer to resources within the configuration - both in code and
 * in config.
 *
 * Examples include:
 * - "course/week1/helloworld$environments:compilation.files"
 * - "$stages:Compilation:actions:gcc.command"
 *
 * Helper (operator) functions allow easy concatenation of two paths,
 * as follows:
 * - "course"/"week1"/"helloworld"
 * - "$stages" % "Compilation" % "actions" % "gcc" .. "command"
 */
data class EntityPath(
    val entities: List<String> = emptyList(),
    val elements: List<String> = emptyList(),
    val field: String,
) {
    constructor(path: EntityPath) : this(path.entities, path.elements, path.field)
    constructor(path: String) : this(fromString(path))

    private fun startDegree(): Int = when(true) {
        entities.isNotEmpty() -> 1
        elements.isNotEmpty() -> 2
        field.isNotBlank() -> 3
        else -> 4
    }

    private fun endDegree(): Int = when(true) {
        field.isNotBlank() -> 3
        elements.isNotEmpty() -> 2
        entities.isNotEmpty() -> 1
        else -> 0
    }

    fun isPrefixOf(other: EntityPath): Boolean = endDegree() <= other.startDegree()
    fun isSuffixOf(other: EntityPath): Boolean = other.isPrefixOf(this)

    fun prefix(prefix: EntityPath): EntityPath {
        if (!prefix.isPrefixOf(this))
            throw InvalidPathException(prefix.toString(), "Given path is not a valid prefix of ${toString()}")

        return EntityPath(
            entities = prefix.entities + entities,
            elements = prefix.elements + elements,
            field = prefix.field.ifBlank { field }
        )
    }
    fun prefix(prefix: String): EntityPath = prefix(EntityPath(prefix))

    fun suffix(suffix: EntityPath): EntityPath = suffix.prefix(this)
    fun suffix(suffix: String): EntityPath = suffix(EntityPath(suffix))


    operator fun div(suffix: EntityPath): EntityPath = suffix(suffix)
    operator fun div(suffix: String): EntityPath = suffix(suffix)
    operator fun rem(suffix: EntityPath): EntityPath = suffix(suffix)
    operator fun rem(suffix: String): EntityPath = suffix("$$suffix")
    operator fun rangeTo(suffix: EntityPath): EntityPath = suffix(suffix)
    operator fun rangeTo(suffix: String): EntityPath = suffix("$.$suffix")

    fun toPathString(): String = "${entities.joinToString("/")}$${elements.joinToString(":")}.$field"
    override fun toString(): String = "Path(${toPathString()})"

    companion object : Mergeable<EntityPath> {
        fun fromString(path: String): EntityPath {
            if(path.count { it == '$' } > 1)
                throw InvalidPathException(path, "Multiple $-characters are not allowed in a path")

            val entityPath = path.substringBefore("$")
            val elementPath = path.substringAfter("$", "").substringBefore(".")
            val field = path.substringAfter(".", "")

            return EntityPath(
                entityPath.split("/").filter { it.isNotBlank() },
                elementPath.split(":").filter { it.isNotBlank() },
                field
            )
        }

        val empty = EntityPath(field = "")

        override val merger: Merger<EntityPath> = { inherited / current }
    }
}

operator fun String.div(suffix: String): EntityPath = EntityPath(this) / suffix
operator fun String.rem(suffix: String): EntityPath = EntityPath(this) % suffix
operator fun String.rangeTo(suffix: String): EntityPath = EntityPath(this) .. suffix
