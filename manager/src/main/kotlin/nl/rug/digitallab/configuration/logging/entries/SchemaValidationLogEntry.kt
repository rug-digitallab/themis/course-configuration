package nl.rug.digitallab.configuration.logging.entries

import com.networknt.schema.ValidationMessage
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.Logger
import java.nio.file.Path

/**
 * [LogEntry] implementation for logging schema validation
 * errors. The log type will always be [ConfigurationLogger.Type.ERROR],
 * and the log message will by default be obtained from the
 * provided [ValidationMessage]
 */
data class SchemaValidationLogEntry (
    override val location: Path,
    val validationMessage: ValidationMessage,
    override val message: String = validationMessage.message,
) : LogEntry<Path> {
    override val type: Logger.Type = Logger.Type.ERROR
}
