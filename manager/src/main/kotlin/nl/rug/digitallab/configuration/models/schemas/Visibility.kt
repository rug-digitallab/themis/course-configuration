package nl.rug.digitallab.configuration.models.schemas

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.Cloning.clone
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.doMergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.Mergeable
import nl.rug.digitallab.configuration.inheritance.merging.max
import nl.rug.digitallab.configuration.inheritance.merging.min
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.models.Property
import java.time.Instant

@optics
@JsonSchema("./visibility.schema.json")
data class Visibility (
    @JsonProperty(value = "visible")
    val visibilityType: Property<Type>,

    // Start and end are only relevant when the type is TIMED.
    @JsonProperty(defaultValue = "null") val start: Property<Instant?>,
    @JsonProperty(defaultValue = "null") val end: Property<Instant?>,
) {
    enum class Type {
        // Note that the order is important here as it is used for determining
        // the "strictness" of each visibility type. NEVER < TIMED < ALWAYS
        NEVER, TIMED, ALWAYS
    }

    companion object : Mergeable<Visibility?> {
        override val merger: Merger<Visibility?> = merge@{
            // When the inherited value is null, we keep the current value (even if it is null)
            if(inherited == null) {
                traceOverride("Nothing to inherit, keeping local value")
                return@merge current
            }

            // When the current value is null, we simply inherit
            if(current == null) {
                traceInherit("Inheriting $inherited - No local value specified")
                return@merge inherited.clone()
            }

            // When the parent visibility is stricter than the current,
            // we simply inherit
            if(inherited.visibilityType() < current.visibilityType()) {
                traceInherit("Inheriting $inherited - Inherited visibility is stricter than local value")
                return@merge inherited.clone()
            }

            // When both the inherited and current values are timed,
            // we ensure that we choose the shortest time window,
            // such that visibility never "widens"
            if(inherited.visibilityType() == Type.TIMED && current.visibilityType() == Type.TIMED) {
                val merged = doMergeByProperty {
                    "Both the inherited and current visibility are 'timed'".asExplanation {
                        visibilityType set Type.TIMED
                    }

                    // The min and max functions already provide tracing descriptions
                    start           use max()
                    end             use min()
                }

                traceMerge("Merging into $current - Selecting shortest visibility window of both inherited and local values")
                return@merge merged
            }

            traceOverride("Using local $current - Local visibility is stricter than inherited value")
            return@merge current
        }
    }
}
