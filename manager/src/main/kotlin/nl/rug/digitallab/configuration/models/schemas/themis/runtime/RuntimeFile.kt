package nl.rug.digitallab.configuration.models.schemas.themis.runtime

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.Mergeable
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import java.util.EnumSet

@optics
@JsonSchema("./themis/runtime/file.schema.json")
data class RuntimeFile (
    // The source of a file is required
    val source: Property<String>,

    // To prevent conflicts with the "entity path" below, we rename
    // this property in code compared to the JSON schema.
    // A null here means to deduce the target path automatically. As
    // explained in the schema, we will then base this on "source"
    @JsonProperty("path")
    val targetPath: Property<String> = source().toProp(), // TODO proper path deduction (#4)

    // There is no inheritance on these properties, and they have
    // a clear default value.
    @JsonProperty(defaultValue = "ORDER") val priority: Property<Priority>,
    @JsonProperty(defaultValue = "{}") val permissions: Property<Permissions>,

    @JsonIgnore
    @JsonProperty("unmapped")
    override val path: Property<EntityPath> = EntityPath("\$file").toProp(),
) : ITraversable {
    override fun toNodeString(): String = "File from \"$source\""

    enum class Priority {
        OVERRIDABLE, ORDER, OVERRIDE
    }

    // Nulls indicate unspecified properties
    data class Permissions (
        @JsonProperty(defaultValue = "null") val ownerUser: Property<Int?>,
        @JsonProperty(defaultValue = "null") val ownerGroup: Property<Int?>,

        @JsonProperty(defaultValue = "null") val user : Property<EnumSet<PermissionFlags>?>,
        @JsonProperty(defaultValue = "null") val group: Property<EnumSet<PermissionFlags>?>,
        @JsonProperty(defaultValue = "null") val world: Property<EnumSet<PermissionFlags>?>,
    )

    enum class PermissionFlags {
        READ, WRITE, EXEC
    }

    companion object : Mergeable<RuntimeFile> {
        override val merger: Merger<RuntimeFile> = { current }
    }
}
