package nl.rug.digitallab.configuration.jackson

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler

/**
 * An implementation of a [DeserializationProblemHandler] that will
 * ignore all unknown properties starting with a dot. In the schemas,
 * such properties are intended to store any data, for use in YAML
 * anchors.
 *
 * This behaviour is achieved through this "problem handler", in
 * particular by implementing the "unknown property" handler. This
 * will check the property name, and ignore it when it is such a
 * template property.
 */
object TemplatePropertyHandler : DeserializationProblemHandler() {
    override fun handleUnknownProperty(
        ctxt: DeserializationContext?,
        p: JsonParser,
        deserializer: JsonDeserializer<*>?,
        beanOrClass: Any?,
        propertyName: String,
    ): Boolean {
        // Template properties start with a dot; if we find that, we skip
        // the property and report that we handled the unknown property.
        if(propertyName.startsWith(".")) {
            p.skipChildren()
            return true
        }

        // If we do not handle the property, Jackson will delegate to
        // other problem handlers and might eventually trigger a failure
        // depending on the [DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTY]
        // setting. For Digital Lab, this is typically enabled.
        return false
    }
}
