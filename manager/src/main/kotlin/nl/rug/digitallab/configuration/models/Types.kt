package nl.rug.digitallab.configuration.models

typealias ItemMap<ItemT> = Map<String, Property<ItemT?>>
typealias MapProperty<ItemT> = Property<ItemMap<ItemT>>

typealias ItemList<ItemT> = List<Property<ItemT>>
typealias ListProperty<ItemT> = Property<ItemList<ItemT>>

typealias NullableListProperty<ItemT> = Property<ItemList<ItemT>?>
