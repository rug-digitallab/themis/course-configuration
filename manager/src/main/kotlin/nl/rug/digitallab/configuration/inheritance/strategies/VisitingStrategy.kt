package nl.rug.digitallab.configuration.inheritance.strategies

import nl.rug.digitallab.configuration.inheritance.Traversal
import nl.rug.digitallab.configuration.inheritance.strategies.VisitingStrategy.Companion.create
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.strategies.StrategyLogger
import nl.rug.digitallab.configuration.models.Property

/**
 * Strategy implementation for visiting and modifying property
 * values. Given a visiting function, this strategy will only
 * allow in-place modifications to values, without any data
 * carried through between entities and visits.
 *
 * Typical usage will not implement this abstract class directly,
 * but will make use of the [create] or [withVisit] helper functions.
 *
 * @param PropertyT The type of values that will be visited
 *                 in this strategy
 */
abstract class VisitingStrategy<RootT, PropertyT> : Strategy<RootT, PropertyT, VisitingStrategy.Accumulator<PropertyT>>() {
    override val strategyName: String
        get() = VisitingStrategy::class.simpleName.orEmpty()

    /**
     * The visiting function used to modify the current values
     *
     * It is the responsibility of this function to make the right calls
     * to log the actions taken and decisions made.
     *
     * @param value The current value for this property
     * @return The modified value
     */
    context(StrategyLogger<PropertyT>)
    protected abstract fun visit(value: PropertyT): PropertyT

    /**
     * This function will be called for each property that is visited in the
     * traversal. It will always call the [visit] function and return its result.
     *
     * @param value The current value of the property
     * @param accumulator The current accumulator state
     */
    context(StrategyLogger<PropertyT>)
    override fun visit(value: PropertyT, accumulator: Accumulator<PropertyT>): PropertyT {
        val result = visit(value)

        strategyDebug("Modified value into $result")

        return result
    }

    /**
     * Entrypoint to start running the [VisitingStrategy]. For a given
     * [target], it will run the traversal and visit all selected
     * properties along the way.
     *
     * @param target The root element to start traversal from
     * @return The modified root element after traversal and modifications
     */
    context(ConfigurationLogger)
    fun apply(target: Property<RootT>): Property<RootT>
        = execute(target, Accumulator())

    /**
     * "Dud" [Strategy.Accumulator] implementation, storing no particular
     * additional data.
     */
    class Accumulator<PropertyT> :
            Strategy.Accumulator<Accumulator<PropertyT>, PropertyT>() {
        override fun fork(): Accumulator<PropertyT> = this /* no-op */
        override fun merge(child: Accumulator<PropertyT>) { /* no-op */ }
    }

    companion object {
        /**
         * Helper function that will create an instance of a [VisitingStrategy]
         * with the specified visit function and traversal. It is the recommended
         * starting point for simple [VisitingStrategy] usages.
         *
         * It is the responsibility of the visit function to make the right calls
         * to trace the actions taken and decisions made.
         *
         * @param visitFunction The function to modify current property values
         * @param traversal The traversal to do visiting on
         * @return A [VisitingStrategy] instance
         */
        fun <RootT, PropertyT> create(
            visitFunction: context(StrategyLogger<PropertyT>) (value: PropertyT) -> PropertyT,
            traversal: Traversal<RootT, PropertyT>,
        ): VisitingStrategy<RootT, PropertyT> = object : VisitingStrategy<RootT, PropertyT>() {
            context(StrategyLogger<PropertyT>)
            override fun visit(value: PropertyT): PropertyT = visitFunction(this@StrategyLogger, value)
            override val traversal = traversal
        }
    }
}

/**
 * Creates an instance of [VisitingStrategy] with the current
 * [Traversal] and the specified visit function.
 *
 * It is the responsibility of the visit function to make the right calls
 * to trace the actions taken and decisions made.
 *
 * @param visitFunction The function to modify current property values
 * @return A [VisitingStrategy] instance
 */
fun <RootT, PropertyT> Traversal<RootT, PropertyT>.withVisit(
    visitFunction: context(StrategyLogger<PropertyT>) (value: PropertyT) -> PropertyT,
) = create(visitFunction, this)
