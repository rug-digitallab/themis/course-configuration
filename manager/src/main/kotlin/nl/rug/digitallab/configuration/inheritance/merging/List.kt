package nl.rug.digitallab.configuration.inheritance.merging

import arrow.optics.Lens
import nl.rug.digitallab.configuration.helpers.get
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.models.ItemList
import nl.rug.digitallab.configuration.models.Property.Companion.filterNotNull

/**
 * Merge function implementing a lists merger that will merge
 * items by their "key":
 * - By default, all items from the inherited list are used
 * - Items that are specified in the current list with a new key are added
 * - Items that are specified in the current list with an existing key
 *     have their values merged using [mergeItems]
 *
 * Logging and tracing is as in [mapsWith]
 *
 * @param keySelector The property in the list item to be used as key
 * @param mergeItems The method used to merge two items with the same key
 */
inline fun <reified ItemT, KeyT> MergingEnvironment.listsByKeyWith(
    keySelector: Lens<ItemT, KeyT>,
    noinline mergeItems: Merger<ItemT>,
) : Merger<ItemList<ItemT>> = merge@{
    // This is implemented by transforming the list to a map based on the
    // keys, and then running a normal map merge.
    val inheritedMap = inherited.associateBy { it()[keySelector].toString() }
    val currentMap = current.associateBy { it()[keySelector].toString() }
    
    val merged = mergeValues(inheritedMap, currentMap, mapsWith(mergeItems))

    return@merge merged.values.filterNotNull()
}
