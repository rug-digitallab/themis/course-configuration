package nl.rug.digitallab.configuration.inheritance.merging.scopes

import nl.rug.digitallab.configuration.logging.entries.LogEntry
import nl.rug.digitallab.configuration.models.Property

/**
 * Simple [MergeScope] implementation that will not retain any
 * (tracing) log entries. This is useful for testing purposes,
 * or when there is a specific need to "mute" merging traces
 * in some context.
 *
 * @param inherited The inherited value in the merge
 * @param current The current ("local") value in the merge
 */
class NoLogsMergeScope<ItemT>(inherited: ItemT, current: ItemT) : MergeScope<ItemT>(inherited, current) {
    override fun log(entry: LogEntry<*>) = Unit

    override fun traceInherit(message: String) = Unit
    override fun traceOverride(message: String) = Unit
    override fun traceMerge(message: String) = Unit
    override fun traceTransform(message: String) = Unit

    override fun <T> scopeMerge(inherited: Property<T>, current: Property<T>, merger: Merger<T>): T =
        mergeValues(inherited(), current(), merger)

    companion object {
        fun <T> Merger<T>.muted(): Merger<T> = { with(NoLogsMergeScope(inherited, current)) { this@muted() } }
    }
}
