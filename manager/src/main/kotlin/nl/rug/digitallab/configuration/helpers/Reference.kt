package nl.rug.digitallab.configuration.helpers

import arrow.optics.Every
import arrow.optics.Traversal

/**
 * A class to represent an object that either refers to an entity, or
 * contains the target entity itself. An example usecase would be in
 * the configuration of runtime limits on an action, where we can either
 * define limits directly, or specify a reference to other limit
 * definitions. In JSON/YAML, this would behave similar to YAML anchors.
 *
 * This is a sealed class, to implement an effective "sum type" that can
 * be either a [Value] or a [Reference]. This enables Kotlin to verify
 * we always cover all possible cases.
 *
 * @param T The target entity type
 */
sealed class ReferenceOr<T>

/**
 * A string-based reference to an entity - no interpretation of the
 * reference string is done.
 *
 * *Note* that in an ideal world, this class would not carry a type,
 * which could be achieved by making the [T] in [ReferenceOr]
 * contravariant, but that will break JSON parsing and is very tricky
 * in JVM due to type erasure.
 *
 * @param T The target entity type
 * @param ref The string reference
 */
data class Reference<T> (val ref: String) : ReferenceOr<T>()

/**
 * A simple wrapper around a value of type [T]
 *
 * @param T The value type
 * @param instance The value
 */
data class Value<T> (val instance : T) : ReferenceOr<T>()

/**
 * [Traversal] for a [ReferenceOr], that focuses on all and only on [Value]s.
 *
 * @return [Traversal] implementation
 */
fun <T> Every.referenceOr(): Traversal<ReferenceOr<T>, T> =
    object : Traversal<ReferenceOr<T>, T> {
        override fun modify(source: ReferenceOr<T>, map: (focus: T) -> T): ReferenceOr<T> = when(source) {
            is Reference<T> -> source
            is Value<T> -> Value(map(source.instance))
        }

        override fun <R> foldMap(initial: R, combine: (R, R) -> R, source: ReferenceOr<T>, map: (focus: T) -> R): R {
            return when(source) {
                is Reference<T> -> initial
                is Value<T> -> combine(initial, map(source.instance))
            }
        }
    }

/**
 * Extend a given traversal to some [ReferenceOr] to focus into [Value] instances
 */
val <A, B> Traversal<A, ReferenceOr<B>>.every: Traversal<A, B>
    get() = this.compose(Every.referenceOr())
