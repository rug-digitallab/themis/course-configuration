package nl.rug.digitallab.configuration.logging.merging

import nl.rug.digitallab.configuration.inheritance.merging.scopes.MergeScope
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.logging.Logger
import nl.rug.digitallab.configuration.logging.entries.TracingEntry
import nl.rug.digitallab.configuration.logging.merging.RelayingMergeScope.Companion.withTracing
import nl.rug.digitallab.configuration.models.Property
import java.util.UUID

/**
 * Special [MergeScope] implementation, relaying all tracing log entries
 * to the given [Logger]. The class cannot be instantiated directly,
 * and users should use [withTracing] to do so.
 *
 * For tracing purposes, this class keeps track of the locations of the
 * inherited and current values, and therefore this can only be used
 * when merging two properties.
 */
context(Logger)
class RelayingMergeScope<ItemT> private constructor(
    // We have this primary constructor to define properties for the locations.
    // We do not store the properties themselves, as the values are already
    // available from the supertype.
    inheritedValue: ItemT,
    private val inheritedLocation: UUID,
    currentValue: ItemT,
    private val currentLocation: UUID,
) : MergeScope<ItemT>(inheritedValue, currentValue), Logger by this {
    // Secondary constructor to simplify instantiation
    private constructor(
        inheritedProperty: Property<ItemT>,
        currentProperty: Property<ItemT>,
    ) : this(
        inheritedProperty.get(),
        inheritedProperty.identity,
        currentProperty.get(),
        currentProperty.identity,
    )

    // We cannot log entries immediately, since the final value
    // of the item, after the merging is complete, needs to be stored
    // along with the log entry. Therefore, we keep track of a list
    // of log entry "constructors" that only miss the final value.
    private val logEntryCreators = mutableListOf<(ItemT) -> TracingEntry<ItemT>>()

    override fun traceInherit(message: String) {
        logEntryCreators.add {
            TracingEntry.Inherit(
                currentLocation,
                current,
                inheritedLocation,
                inherited,
                it,
                message,
            )
        }
    }

    override fun traceOverride(message: String) {
        logEntryCreators.add {
            TracingEntry.Override(
                currentLocation,
                current,
                inheritedLocation,
                inherited,
                message,
            )
        }
    }

    override fun traceMerge(message: String) {
        logEntryCreators.add {
            TracingEntry.Merge(
                currentLocation,
                current,
                inheritedLocation,
                inherited,
                it,
                message,
            )
        }
    }

    override fun traceTransform(message: String) {
        logEntryCreators.add {
            TracingEntry.Transform(
                currentLocation,
                current,
                it,
                message,
            )
        }
    }

    override fun <T> scopeMerge(inherited: Property<T>, current: Property<T>, merger: Merger<T>): T {
        return withTracing(inherited, current, merger)
    }

    companion object {
        /**
         * This method is used to instantiate the [RelayingMergeScope] for the
         * given context: some [inherited] and [current] properties. This method
         * will also execute the provided [merger], using the [RelayingMergeScope]
         * that was instantiated. This results in a merged value, with tracing logs
         * transparently added to the provided [Logger].
         *
         * @receiver The [Logger] to relay all logs to
         * @param inherited The inherited property for merging
         * @param current The current property for merging
         * @param merger The merger to execute in the provided context
         * @return The result of applying the [merger] to both provided values.
         */
        context(Logger)
        fun <ItemT> withTracing(inherited: Property<ItemT>, current: Property<ItemT>, merger: Merger<ItemT>): ItemT {
            val scope = RelayingMergeScope(inherited, current)
            val result = merger(scope)

            // For now, we assume there to never be more than a single log entry per "action"
            // This assumption might no longer hold in the future, but this works as a nice
            // sanity check during development for now.
            assert(scope.logEntryCreators.size <= 1) { "Expecting no more than 1 log entry per merge scope" }
            scope.logEntryCreators.forEach { log(it(result)) }

            return result
        }
    }
}
