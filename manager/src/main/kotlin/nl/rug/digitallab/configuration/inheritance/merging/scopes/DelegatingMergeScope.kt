package nl.rug.digitallab.configuration.inheritance.merging.scopes

import nl.rug.digitallab.configuration.logging.merging.MergingLogger
import nl.rug.digitallab.configuration.models.Property

/**
 * Simple [MergeScope] implementation that will delegate any
 * (tracing) log entries to the provided "parent" scope. This is
 * useful mainly for [mergeValues], to change the merging types
 * without changing the scope and context.
 *
 * @param inherited The inherited value in the merge
 * @param current The current ("local") value in the merge
 * @param delegate The merge scope to delegate any logs to
 */
class DelegatingMergeScope<ItemT>(
    inherited: ItemT,
    current: ItemT,
    private val delegate: MergeScope<*>
) : MergeScope<ItemT>(inherited, current), MergingLogger by delegate {
    override fun <T> scopeMerge(inherited: Property<T>, current: Property<T>, merger: Merger<T>): T =
        delegate.scopeMerge(inherited, current, merger)
}
