package nl.rug.digitallab.configuration.inheritance.merging

import nl.rug.digitallab.configuration.helpers.ReferenceOr
import nl.rug.digitallab.configuration.helpers.Value
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger

/**
 * Merge function implementing a merger on [ReferenceOr] items,
 * operating only on [Value] instances.
 * - When either current or inherited values are not an instance of
 *      [Value], the merge will fail
 * - When both current and inherited values are an instance of [Value],
 *      the [mergeItems] method is used to merge the values.
 *
 * This method will create appropriate tracing log entries in case
 * of an error. However, in case both inherited and current values are
 * instances of [Value], producing an appropriate trace log entry is
 * the responsibility of [mergeItems].
 *
 * @param mergeItems Method used to merge two [Value] instances
 * @return [Merger] implementation for [ReferenceOr]
 */
fun <ItemT> MergingEnvironment.referencesWith(
    mergeItems: Merger<ItemT>,
): Merger<ReferenceOr<ItemT>> = merge@{
    if(current !is Value) {
        // TODO make this into an error (#17)
        traceInherit("Found an unresolved reference during value merging, ignoring specified value")
        return@merge inherited
    }

    if(inherited !is Value) {
        traceOverride("Found an unresolved reference during value merging, ignoring specified value")
        return@merge current
    }

    return@merge Value(mergeValues(inherited.instance, current.instance, mergeItems))
}
