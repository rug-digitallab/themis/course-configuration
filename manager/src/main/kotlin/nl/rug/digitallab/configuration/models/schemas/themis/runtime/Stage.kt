package nl.rug.digitallab.configuration.models.schemas.themis.runtime

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.Mergeable
import nl.rug.digitallab.configuration.inheritance.merging.mapsWith
import nl.rug.digitallab.configuration.inheritance.merging.overrideOrInherit
import nl.rug.digitallab.configuration.inheritance.merging.valuesOfType
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.MapProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp

@optics
@JsonSchema("./themis/runtime/stage.schema.json")
data class Stage (
    // Since Stages only ever occur inside a map, this name will be taken from the map key
    @JsonIgnore
    val name: Property<String> = "".toProp(),

    // For these, null means to inherit the property, while an explicit
    // value (also empty strings) overrides the inherited value.
    @JsonProperty(defaultValue = "null") val order: Property<Int?>,
    @JsonProperty(defaultValue = "null") val scheduling: Property<Scheduling?>,
    @JsonProperty(defaultValue = "null") val shouldContinue: Property<String?>,

    @JsonProperty(defaultValue = "null") val testGroup: Property<String?>,
    @JsonProperty(defaultValue = "null") val testStrategy: Property<TestStrategy?>,

    // There is no semantic difference between absence and emptiness of these
    // conditions, since each condition is specified separately. So this it not
    // nullable.
    @JsonProperty(defaultValue = "{}") val statusConditions: Property<StatusConditions>,

    // Actions have no distinction between emptiness and absence - clearing
    // entries from this map is achieved by explicitly specifying null for a key
    @JsonProperty(defaultValue = "{}")
    val actions: MapProperty<Action>,

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath("").toProp(),
) : ITraversable {
    override fun toNodeString(): String = "Stage \"$name\""

    enum class Scheduling {
        ONCE, TESTS, CLEANUP
    }

    enum class TestStrategy {
        PARALLEL, SEQUENCE
    }

    @optics
    data class StatusConditions (
        @JsonProperty(defaultValue = "null") val compilationError: Property<String?>,
        @JsonProperty(defaultValue = "null") val wrongOutput: Property<String?>,
        @JsonProperty(defaultValue = "null") val runtimeError: Property<String?>,
    ) {
        companion object : Mergeable<StatusConditions> {
            override val merger = mergeByProperty<StatusConditions> {
                compilationError merge overrideOrInherit()
                wrongOutput merge overrideOrInherit()
                runtimeError merge overrideOrInherit()
            }
        }
    }

    companion object : Mergeable<Stage> {
        override val merger = mergeByProperty<Stage> {
            name                use current

            order               merge overrideOrInherit()
            scheduling          merge overrideOrInherit()
            shouldContinue      merge overrideOrInherit()

            testGroup           merge overrideOrInherit()
            testStrategy        merge overrideOrInherit()

            statusConditions    merge valuesOfType(StatusConditions)

            actions             merge mapsWith(valuesOfType(Action))

            path                use current
        }
    }
}
