package nl.rug.digitallab.configuration.helpers

import java.util.EnumSet

/**
 * This file contains some helpers to make working with [EnumSet]s a bit more
 * convenient in Kotlin. It uses a lot of Kotlin magic.
 */

/**
 * An empty EnumSet of the specified type [T]
 *
 * *Note* that this is an inline function, so we can enable reified generics,
 * which means that we do not have to deal with type erasure in this context.
 * This allows us to call T::class.java, which normally would not be possible.
 *
 * @param T The enum type
 */
inline fun <reified T : Enum<T>> none(): EnumSet<T> = EnumSet.noneOf(T::class.java)

/**
 * An EnumSet with just one element
 *
 * @param T The enum type
 * @param item The single enum value
 */
fun <T : Enum<T>> just(item: T): EnumSet<T> = EnumSet.of(item)

/**
 * Function to add an enum value to a given [EnumSet]
 *
 * *Note* that this is an infix function to allow for more readable
 * notation that looks like "values and Enum.NEW"
 *
 * @param other The new enum value to add
 */
infix fun <T : Enum<T>> EnumSet<T>.and(other: T): EnumSet<T> = apply { add(other) }

/**
 * Function to combine two enum values into an [EnumSet]
 *
 * *Note* that this is an infix function to allow for more readable
 * notation that looks like "Enum.FIRST and Enum.SECOND"
 */
infix fun <T : Enum<T>> T.and(other: T): EnumSet<T> = EnumSet.of(this, other)

/**
 * Function to map all elements of an [EnumSet] from enum type
 * [T] to enum type [U] using the provided [mapping]
 *
 * @param mapping Function to convert enum values of type [T] to type [U]
 */
fun <T : Enum<T>, U : Enum<U>> EnumSet<T>.convert(mapping: (T) -> U): EnumSet<U>
    = EnumSet.copyOf(this.map(mapping))

/**
 * Function to check whether the left-hand [EnumSet] has
 * at least all the values as given in the right-hand [EnumSet].
 *
 * *Note* that this is an infix function to allow for more readable
 * notation that looks like "values hasAllOf check"
 */
infix fun <T : Enum<T>> EnumSet<T>.hasAllOf(check: EnumSet<T>): Boolean = containsAll(check)

/**
 * Function to check whether the left-hand [EnumSet] has
 * at least the given right-hand enum value.
 *
 * *Note* that this is an infix function to allow for more readable
 * notation that looks like "values has Enum.VAL"
 */
infix fun <T : Enum<T>> EnumSet<T>.has(check: T): Boolean = contains(check)

/**
 * Function to check whether the left-hand [EnumSet] has
 * exactly all the values as given in the right-hand [EnumSet].
 *
 * *Note* that this is an infix function to allow for more readable
 * notation that looks like "values isAllOf check"
 */
infix fun <T : Enum<T>> EnumSet<T>.isAllOf(check: EnumSet<T>): Boolean = this == check

/**
 * Function to check whether the left-hand [EnumSet] has
 * exactly and only the given right-hand enum value.
 *
 * *Note* that this is an infix function to allow for more readable
 * notation that looks like "values isJust Enum.VAL"
 */
infix fun <T : Enum<T>> EnumSet<T>.isJust(check: T): Boolean = isAllOf(just(check))