package nl.rug.digitallab.configuration.models.schemas.grading

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.models.NullableListProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import java.math.BigDecimal

@JsonSchema("./grading/rubric/criterion.schema.json")
data class Criterion (
    val name: Property<String>,
    @JsonProperty(defaultValue = "\"\"") val description: Property<String>,

    @JsonProperty(defaultValue = "true") val allowOverride: Property<Boolean>,

    @JsonProperty(defaultValue = "null") val buckets: NullableListProperty<Bucket>,
    @JsonProperty(defaultValue = "null") val manualPoints: Property<BigDecimal?>,
    @JsonProperty(defaultValue = "null") val constantPoints: Property<BigDecimal?>,
    @JsonProperty(defaultValue = "null") val externalPoints: Property<PointsSource?>,
) {
    @JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "source",
    )
    sealed interface PointsSource {
        val scalePointsTo: Property<BigDecimal?>
    }

    // The type name corresponds to the enum value in the JSON schema
    @JsonTypeName("themis")
    data class ThemisPointsSource (
        val assignmentPath: Property<String>,
        override val scalePointsTo: Property<BigDecimal?> = null.toProp(),
    ) : PointsSource
}
