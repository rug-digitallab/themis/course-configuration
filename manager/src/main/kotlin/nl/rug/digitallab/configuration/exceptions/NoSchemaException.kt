package nl.rug.digitallab.configuration.exceptions

/**
 * Exception documenting that the given entity does not have a
 * schema linked to it, such that deserialization is not possible.
 */
class NoSchemaException(val schemaName: String) : Exception()