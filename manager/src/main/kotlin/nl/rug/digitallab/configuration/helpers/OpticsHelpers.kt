package nl.rug.digitallab.configuration.helpers

import arrow.optics.Lens
import arrow.optics.Optional
import arrow.optics.Traversal

// Helper operators that allow us to ease the notation when using Optics.
// Instead of having to write `lens.get(instance)`, we can now write
// `instance[lens]`, as if you are "indexing" the entity with your lens.
// This should be more intuitive and easier to read.

/**
 * Apply the given [lens] on the current object. This is some syntactic
 * sugar for `lens.get(this)`, and eases interpretation of the code.
 *
 * @param lens The optic to use on the current object
 * @return An instance of [B] as returned by the optic
 */
operator fun <A, B> A.get(lens: Lens<in A, out B>): B
    = lens.get(this)

/**
 * Apply the given [optional] on the current object. This is some syntactic
 * sugar for `optional.getOrNull(this)`, and eases interpretation of the code.
 *
 * @param optional The optic to use on the current object
 * @return An instance of [B] or `null`, as returned by the optic
 */
operator fun <A, B> A.get(optional: Optional<in A, out B>): B?
    = optional.getOrNull(this)

/**
 * Apply the given [traversal] on the current object. This is some syntactic
 * sugar for `traversal.getAll(this)`, and eases interpretation of the code.
 *
 * @param traversal The optic to use on the current object
 * @return Instances of [B] as returned by the optic
 */
operator fun <A, B> A.get(traversal: Traversal<in A, out B>): List<B>
    = traversal.getAll(this)
