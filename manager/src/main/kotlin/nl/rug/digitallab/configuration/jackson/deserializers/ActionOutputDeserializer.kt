package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.databind.JsonDeserializer
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Action

/**
 * Custom deserializer for [Action.Output] items that accepts
 * either full objects, or just strings that represent only
 * the file path.
 *
 * @param wrappedDeserializer The original deserializer for [Action.Output]
 */
class ActionOutputDeserializer (
    wrappedDeserializer: JsonDeserializer<*>
) : StringOrObjectDeserializer<Action.Output>(wrappedDeserializer, Action.Output::class, Action::Output) {
    override fun withDeserializer(deserializer: JsonDeserializer<*>) =
        ActionOutputDeserializer(deserializer)
}
