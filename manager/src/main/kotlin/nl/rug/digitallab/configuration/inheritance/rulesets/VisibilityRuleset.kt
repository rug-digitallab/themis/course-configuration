package nl.rug.digitallab.configuration.inheritance.rulesets

import nl.rug.digitallab.configuration.inheritance.merging.valuesOfType
import nl.rug.digitallab.configuration.inheritance.strategies.withMerge
import nl.rug.digitallab.configuration.inheritance.strategies.withValidation
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.schemas.Course
import nl.rug.digitallab.configuration.models.schemas.Visibility
import nl.rug.digitallab.configuration.models.schemas.themis.Assignment
import nl.rug.digitallab.configuration.models.schemas.themis.Folder
import nl.rug.digitallab.configuration.models.schemas.themis.visibility
import nl.rug.digitallab.configuration.models.schemas.traverseThemis
import nl.rug.digitallab.configuration.models.schemas.visibility

/**
 * This ruleset will propagate visibility settings through the configuration.
 * It will make sure that visibility will never "widen" when parents have set
 * a stricter visibility.
 */
class VisibilityRuleset : Ruleset<Course>() {

    context(ConfigurationLogger)
    override fun applyImplementation(root: Property<Course>): Property<Course> {
        // Put the immutable function parameter in a local mutable variable
        var course = root

        val visibilityTraversal = traverseThemis(
            courseBuilder = { inspect(Course.visibility) },
            folderBuilder = { inspect(Folder.visibility) },
            assignmentBuilder = { inspect(Assignment.visibility) },
        )

        // We visit all Visibility instances and merge inherited and
        // current values in such a way that visibility can only become
        // stricter as we go "down" the tree
        course = visibilityTraversal
            .withMerge { valuesOfType(Visibility) }
            .apply(course)

        // Validate that all visibilities now have an explicit, valid value
        // TODO make better validation here to ensure ALWAYS/NEVER do not have times (#17)
        val visibilitiesValid = visibilityTraversal.withValidation { it != null }.validate(course)

        // TODO: We should still have some better error handling here, relates to proper Ruleset management (#17)
        if(!visibilitiesValid)
            throw Exception("Invalid!")

        return course
    }
}
