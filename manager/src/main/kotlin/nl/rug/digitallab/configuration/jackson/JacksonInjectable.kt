package nl.rug.digitallab.configuration.jackson

import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.InjectableValues
import com.fasterxml.jackson.databind.JsonMappingException

/**
 * Helper function to obtain an injectable resource by classname.
 * Use the [addValue] method to add an instance of the class to
 * the injectable context, to ensure consistent key naming.
 *
 * @param T The type of the class to obtain
 * @return The instance of the class, if found - otherwise null
 */
inline fun <reified T> DeserializationContext.findInjectable(): T? = try {
    this.findInjectableValue(
        T::class.qualifiedName,
        null,
        null,
    ) as? T
} catch (_: JsonMappingException) {
    null
}

/**
 * Helper function to add an injectable resource to the dictionary.
 * Use the [findInjectable] method on a [DeserializationContext] to
 * retrieve the injected instances.
 *
 * @param T The type of the class to inject
 * @param instance The instance to inject
 * @return The set of injectable values, to allow for call chaining
 */
inline fun <reified T> InjectableValues.Std.addValue(instance: T): InjectableValues.Std =
    apply { addValue(T::class.qualifiedName, instance) }
