package nl.rug.digitallab.configuration.models

import arrow.optics.*
import nl.rug.digitallab.configuration.helpers.get
import nl.rug.digitallab.configuration.models.Property.Companion.lift
import nl.rug.digitallab.configuration.models.Property.Companion.unsafeCast
import nl.rug.digitallab.configuration.models.Property.Companion.with
import java.util.Objects
import java.util.UUID

/**
 * This is a generic container class that holds values of type [T].
 * Each instance of this class represents a configuration "property",
 * and carries the "identity" of such a property. This allows us to
 * give a unique identity to each and every property and field in our
 * configuration model. It allows us to transparently trace any changes
 * to configuration settings.
 *
 * A property will obtain a unique [identity] (UUID) upon first
 * instantiation, which will not change afterwards, unless a new
 * property is instantiated. This class carries data class semantics,
 * so it is immutable. To change the value of a property, the user
 * should either use [set] or [map] - these will return an updated
 * property, but with the same identity.
 *
 * Equality of properties is purely dependent on its value, ignoring
 * the identities. The same holds for the hashcode and toString()
 * implementations.
 *
 * A lot of effort has been put into making this type integrate
 * with normal Kotlin language features as transparently as possible.
 * To obtain the current value of a property, the user can use
 * [get], but can also simply [invoke] the property ("foo()"). This
 * improves readability of code using properties. Similarly, any
 * lens to a property can be "invoked" to obtain a lens to the value
 * of a property.
 *
 * The generic type [T] is covariant, to allow for downcasting
 * instances of this class - so `Property<T>` is an instance of
 * `Property<T?>` but not the other way around. "Unsafe" type
 * changes (in the contravariant direction) can be done using
 * [unsafeCast], which will return `null` if the cast is not valid.
 *
 * For users of Optics, this class provides a [lift] method,
 * which "lifts" any lens from `A` to `B` to a new lens from
 * `Property<A>` to `Property<B>` - similar to monads in functional
 * programming. This lift is available through [with]. In
 * addition, this class provides custom extensions of the [Copy]
 * DSL, operating on a property directly, to ease syntax.
 *
 * @param value The current value of the property
 * @param identity The unique identity of this property
 */
class Property<out T> private constructor(
    private val value: T,
    val identity: UUID,
) {
    // This is the acceptable public constructor
    constructor(
        value: T
    ) : this(value, UUID.randomUUID())

    //region Basic property accessors (get/set)

    /**
     * Obtain the current value of the property, also available through [invoke]
     *
     * @return Value of the property
     */
    fun get(): T = value

    /**
     * Returns whether the current property has a value (in case [T] is nullable)
     *
     * @return Whether the property has a value
     */
    fun hasValue(): Boolean = value != null

    /**
     * Obtain the current value of the property by "invoking" it as if it
     * is a function, this simplifies syntax and improves legibility when
     * used appropriately.
     *
     * @return Current value of the property
     */
    operator fun invoke(): T = get()

    /**
     * Set a new value for this property, while maintaining its identity. This
     * will return a new instance of [Property], following data class semantics!
     *
     * __TIP__: Instead of using both [get] and [set], use [map]
     *
     * @return New property instance with updated value but same identity
     */
    fun <V> set(newValue: V) = Property(newValue, identity)

    /**
     * Set a new value for this property, by using the mapping function [f].
     * This will return a new instance of [Property], following data class
     * semantics, but will use the original identity.
     *
     * @return New property instance with updated value but same identity
     */
    fun <V> map(f: (T) -> V): Property<V> = Property(f(value), identity)

    //endregion
    //region Representations of a property

    // These methods are all simply delegated to the value
    override fun toString() = value.toString()
    override fun equals(other: Any?): Boolean {
        if(other !is Property<*>)
            return false

        return Objects.equals(value, other.value)
    }
    override fun hashCode() = value.hashCode()

    //endregion

    companion object {
        //region Property accessor extensions

        /**
         * Simple way to wrap some value in a property without having to use
         * the constructor explicitly, thus saving on brackets and improving
         * readability of code.
         *
         * Note that this might not work in all cases - notably, Kotlin does
         * not do proper type inference on call chains. So the following
         * might not work as you expect:
         * ```kotlin
         * val works: Property<List<String>> = Property(emptyList())
         * val nope: Property<List<String>> = emptyList().prop
         * ```
         *
         * In these cases, it is (unfortunately) still needed to use the
         * constructor explicitly.
         *
         * https://youtrack.jetbrains.com/issue/KT-17115/Type-inference-issue-with-call-chains-with-generic-inputs-e.g.-comparator-builders
         */
        fun <T> T.toProp(): Property<T> = Property(this)

        /**
         * Obtain the current value of the property by "invoking" it as if it
         * is a function, this simplifies syntax and improves legibility when
         * used appropriately. Returns null when the property or its value is
         * null.
         *
         * @return Current instance of the property or null
         */
        operator fun <T> Property<T>?.invoke(): T? = this?.get()

        /**
         * Helper method to perform an "unsafe cast" on a property value. This
         * will try to cast the current value of the property to type [B], but
         * if that is not allowed, will return null.
         *
         * @return Property of new type when the cast is possible, otherwise null
         */
        inline fun <reified A, B> Property<B>.unsafeCast(): Property<A>?
            = if(get() is A) this.map { it as A } else null

        /**
         * Helper method to convert the current property in a non-nullable one
         * by "lifting" the null-case out of the property box. So, if the current
         * value is null, this will return null, and otherwise this will return
         * the original property but now with a non-nullable type.
         *
         * @return Property of non-nullable type, or null
         */
        inline val <reified T> Property<T?>.withValueOrNull: Property<T>?
            get() = this.unsafeCast()

        /**
         * Helper method to remove all properties with `null` value from a list,
         * and convert the list type to contain properties with non-nullable [T].
         *
         * @return List of properties of non-nullable type
         */
        inline fun <reified T> Iterable<Property<T?>>.filterNotNull(): List<Property<T>> =
            this.mapNotNull { it.withValueOrNull }

        //endregion
        //region Optics integrations: basic lenses

        // This is a simple lens to focus on the value of a property; this is private
        // since only DSL chain functions are exposed publicly (below) - those are
        // more user-friendly.
        private fun <T> PLens.Companion.property(): Lens<Property<T>, T> = Lens(
            get = Property<T>::get,
            set = Property<T>::set,
        )

        /**
         * Obtain a lens to the current value of the property by "invoking"
         * the lens, as if it is a function. This simplifies syntax and
         * improves legibility when used appropriately.
         *
         * @return New [Lens] focusing on the value of the property
         */
        operator fun <A, B> Lens<A, Property<B>>.invoke()      = this + Lens.property()

        /**
         * Obtain a lens to the current value of the property by "invoking"
         * the lens, as if it is a function. This simplifies syntax and
         * improves legibility when used appropriately.
         *
         * @return New [Optional] focusing on the value of the property
         */
        operator fun <A, B> Optional<A, Property<B>>.invoke()  = this + Lens.property()

        /**
         * Obtain a lens to the current value of the property by "invoking"
         * the lens, as if it is a function. This simplifies syntax and
         * improves legibility when used appropriately.
         *
         * @return New [Traversal] focusing on the values of the properties
         */
        operator fun <A, B> Traversal<A, Property<B>>.invoke() = this + Lens.property()

        /**
         * Obtain a lens to the current value of the property from a lens
         * to the property itself.
         */
        val <A, B> Lens<A, Property<B>>.value            get() = this + Lens.property()

        /**
         * Obtain a lens to the current value of the property from a lens
         * to the property itself.
         */
        val <A, B> Optional<A, Property<B>>.value        get() = this + Lens.property()

        /**
         * Obtain a lens to the current value of the property from a lens
         * to the property itself.
         */
        val <A, B> Traversal<A, Property<B>>.value       get() = this + Lens.property()

        //endregion
        //region Optics integrations: "lifting"

        // These are the lift instances for Lens, Optional, and Traversal
        // We need three separate ones to keep the current optic type:
        // when composing a Lens with a Traversal, you will always get a
        // Traversal, the more "general" type. This is not desirable,
        // as we do not want to change the user optic type when lifting.
        private fun <A, B> Lens<A, B>.lift(): Lens<Property<A>, Property<B>> = Lens(
            get = { prop -> prop.map { it[this] } },
            set = { prop, value -> prop.map { this.set(it, value()) } },
        )

        private fun <A, B> Optional<A, B>.lift(): Optional<Property<A>, Property<B>> = POptional(
            getOrModify = { prop -> this
                .getOrModify(prop())
                .mapLeft { Property(it, prop.identity) }
                .map { Property(it, prop.identity) }
            },
            set = { prop, value -> prop.map { this.set(it, value()) } },
        )

        private fun <A, B> Traversal<A, B>.lift() = object : Traversal<Property<A>, Property<B>> {
            override fun <C> foldMap(initial: C, combine: (C, C) -> C, source: Property<A>, map: (focus: Property<B>) -> C): C
                = this@lift.foldMap(initial, combine, source.get()) { map(Property(it, source.identity)) }

            override fun modify(source: Property<A>, map: (focus: Property<B>) -> Property<B>): Property<A> {
                // The logic here is as follows:
                // We have a traversal from a single A to potentially many Bs, and on each
                // of those Bs we want to apply the map function - but that only
                // operates on properties _containing_ a B.
                // So what we do is, we call the modify function of the traversal
                // we are lifting, passing a lambda that takes the instance of B,
                // and wraps it in a Property with the same identity as the source
                // of this function. We then unbox it after calling map, to return
                // this as a new instance of B.
                return source.map { this@lift.modify(it) { value -> map(Property(value, source.identity)).get() } }
            }
        }

        /**
         * Given an optic from [B] to [C], this will apply the optic inside the property,
         * to obtain an optic from `Property<B>` to `Property<C>` and compose it with
         * the current optic.
         *
         * @param inside The optic to apply inside the property
         * @return Optic from [A] to a property of type [C]
         */
        fun <A, B, C> Lens<A, Property<B>>.with(inside: Lens<B, C>): Lens<A, Property<C>>
            = this + inside.lift()

        /**
         * Given an optic from [B] to [C], this will apply the optic inside the property,
         * to obtain an optic from `Property<B>` to `Property<C>` and compose it with
         * the current optic.
         *
         * @param inside The optic to apply inside the property
         * @return Optic from [A] to a property of type [C]
         */
        fun <A, B, C> Optional<A, Property<B>>.with(inside: Optional<B, C>): Optional<A, Property<C>>
            = this + inside.lift()

        /**
         * Given an optic from [B] to [C], this will apply the optic inside the property,
         * to obtain an optic from `Property<B>` to `Property<C>` and compose it with
         * the current optic.
         *
         * @param inside The optic to apply inside the property
         * @return Optic from [A] to a property of type [C]
         */
        fun <A, B, C> Traversal<A, Property<B>>.with(inside: Traversal<B, C>): Traversal<A, Property<C>>
            = this + inside.lift()

        //endregion
        //region Optics integrations: nullability

        /**
         * Simple [Optional] optic to go from a property of a nullable type, to
         * a property of a non-nullable type. This can be composed with other
         * optics as desired.
         *
         * @return Optic mapping nullable properties to non-nullable properties
         */
        private fun <T> notNull(): Optional<Property<T?>, Property<T>>
            = Optional.notNull<T>().lift()

        /**
         * Given an optic to a property of nullable [B], returns an [Optional]
         * optic to a property of non-nullable [B].
         */
        val <A, B> Lens<A, Property<B?>>.notNull: Optional<A, Property<B>>
            get() = this + notNull()

        /**
         * Given an optic to a property of nullable [B], returns an [Optional]
         * optic to a property of non-nullable [B].
         */
        val <A, B> Optional<A, Property<B?>>.notNull: Optional<A, Property<B>>
            get() = this + notNull()

        /**
         * Given an optic to a property of nullable [B], returns a [Traversal]
         * optic to a property of non-nullable [B].
         */
        val <A, B> Traversal<A, Property<B?>>.notNull: Traversal<A, Property<B>>
            get() = this + notNull()

        //endregion
        //region Optics integrations: Copy DSL

        /**
         * Sets the value of the property pointed to by the current optic to
         * the new value [b].
         */
        context(Copy<A>)
        infix fun <A, B> Traversal<A, Property<B>>.set(b: B) = this() set b

        /**
         * Transforms the value of the property pointed to by the current
         * optic using the function [f].
         */
        context(Copy<A>)
        infix fun <A, B> Traversal<A, Property<B>>.transformProperty(f: (B) -> B) = this() transform f

        /**
         * Allows the user to narrow the scope of the Copy DSL. Inside the
         * DSL method [f], the Copy operation is now scoped to values
         * pointed to by [field].
         *
         * Unfortunately, it is not possible to alias the original `inside`
         * method name, as then the overload cannot be determined. This is
         * because lambda types do not participate in overload resolution,
         * so for a [field] with target type `Property<String>`, Kotlin is
         * not able to decide whether `B` is `String` or `Property<String>`,
         * even though `B` _could_ be decided by looking at the lambda.
         * In fact, when the lambda is stored in a separate variable and
         * passed by variable, this overload _can_ be chosen.
         *
         * This particular case can be fixed by hacking around and using
         * the `LowPriorityInOverloadResolution` Kotlin-internal annotation,
         * but that won't fix the issue for the [transformProperty] and
         * [copyProperty] methods since those are extension methods.
         */
        context(Copy<A>)
        fun <A, B> Copy<A>.insideProperty(field: Traversal<A, Property<B>>, f: Copy<B>.() -> Unit): Unit
            = inside(field(), f)

        /**
         * Use the [Copy] DSL, starting on a property and not on a normal
         * value. This essentially wraps [copy] inside the property.
         *
         * @see copy
         * @return New Property instance with updated value, but same identity
         */
        fun <A : Any> Property<A>.copyProperty(f: Copy<A>.() -> Unit): Property<A>
                = this.map { it.copy(f) }

        /**
         * Use the [Copy] DSL, starting on a property and not on a normal
         * value. This essentially wraps [copy] inside the property.
         *
         * @see copy
         * @return New Property instance with updated value, but same identity
         */
        @JvmName("copyNullableProperty")
        fun <A : Any> Property<A?>.copyProperty(f: Copy<A>.() -> Unit): Property<A?>
                = this.map { it?.copy(f) }

        //endregion
    }
}
