package nl.rug.digitallab.configuration.logging.entries

import nl.rug.digitallab.configuration.logging.Logger

/**
 * The simplest implementation of a [LogEntry], storing no
 * additional or specialised information.
 */
data class SimpleLogEntry<LocationT> (
    override val type: Logger.Type,
    override val location: LocationT,
    override val message: String,
) : LogEntry<LocationT>
