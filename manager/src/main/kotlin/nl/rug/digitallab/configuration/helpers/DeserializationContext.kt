package nl.rug.digitallab.configuration.helpers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import kotlin.reflect.KClass

// Here we wrap calls to various deserializer errors. In the Jackson library,
// these functions are guaranteed to always throw an exception, but since Java has
// no return type "Nothing", Kotlin does not know this. So we make these wrappers
// that will throw a "dummy exception" which will never _actually_ be thrown, that
// just serve to convince Kotlin that this function never returns a value. This all
// makes our call sites in specific deserializers neater.

fun DeserializationContext.handleUnexpectedToken(t: KClass<*>, p: JsonParser): Nothing {
    handleUnexpectedToken(t.java, p)
    throw Exception("This should never happen!")
}

fun <T : Any> DeserializationContext.reportBadDefinition(t: KClass<T>, msg: String): Nothing {
    reportBadDefinition<T>(t.java, msg)
    throw Exception("This should never happen!")
}