package nl.rug.digitallab.configuration.inheritance.strategies

import nl.rug.digitallab.configuration.inheritance.Traversal
import nl.rug.digitallab.configuration.inheritance.TraversalStep
import nl.rug.digitallab.configuration.inheritance.strategies.Strategy.Accumulator
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.strategies.RelayingStrategyLogger
import nl.rug.digitallab.configuration.logging.strategies.StrategyLogger
import nl.rug.digitallab.configuration.models.Property

/**
 * Abstract class implementing the wireframe of a strategy. A strategy
 * represents a specific way of performing a traversal over a configuration
 * tree. For example, the merging strategy traverses the configuration
 * tree and merges the values of the properties it inspects.
 *
 * In other words: a [Traversal] specifies _which_ properties to visit,
 * while the [Strategy] specifies _what_ should be done with the visited
 * values. This abstract base class implements the logic to traverse the
 * [Traversal], calling the visiting behaviour from the implementation.
 *
 * Implementations should provide an implementation of the [Strategy.Accumulator],
 * an object that is passed to every inspection step to allow [Strategy]
 * implementations to keep track of data while traversing the configuration.
 *
 * Implementations __must__ implement either [visit] or [visitProperty] to
 * implement the strategy logic.
 *
 * Implementations are expected to expose their own "entry point" function
 * to kickstart execution, that will call [execute] in this class. This way,
 * users of the various strategies do not need to deal with the [Accumulator]
 * directly, and can use a specialised entry point for that specific strategy.
 *
 * Additionally, implementations are expected to use appropriate "strategy*"
 * functions, from the [StrategyLogger] interface, to add debugging information.
 * Furthermore, appropriate tracing entries should be logged.
 *
 * @param [PropertyT] The type of the properties inspected by this strategy
 * @param [AccumulatorT] The type of the accumulator implementation. It should
 *      inherit from [Strategy.Accumulator].
 */
abstract class Strategy<RootT, PropertyT, AccumulatorT : Accumulator<AccumulatorT, PropertyT>> {
    /**
     * The traversal to be used for this strategy. This is kept as an abstract
     * property, to allow specific implementations to either specify a fixed
     * traversal, or put it in their constructor or other helper functions.
     */
    protected abstract val traversal: Traversal<RootT, PropertyT>

    /**
     * Name of the current strategy - typically the subclass name, but since
     * typical implementations will use anonymous objects at runtime, accessing
     * the current type name won't easily work.
     */
    protected abstract val strategyName: String

    /**
     * __Implementations must override either this function, or [visitProperty]__
     *
     * This function will be called for each property that is visited in the
     * traversal. The current [value] of the property, as well as the current
     * [accumulator] are passed to this function.
     *
     * The value returned will be stored as the new value of the property. Any
     * changes to the [accumulator] should be done in-place, as the instance
     * is re-used.
     *
     * Any debug or tracing logging is expected to be done by this function.
     *
     * @param value The current value of the visited property
     * @param accumulator The current state of the accumulator
     * @return The new property value
     */
    context(StrategyLogger<PropertyT>)
    protected open fun visit(value: PropertyT, accumulator: AccumulatorT): PropertyT {
        throw NotImplementedError("Implementations of Strategy should implement visit() or override visitProperty()")
    }

    /**
     * __Implementations must override either this function, or [visit]__
     *
     * This function will be called for each property that is visited in the
     * traversal. The current [property], as well as the current [accumulator]
     * are passed to this function.
     *
     * The resulting property will replace the original property, including
     * identity. Therefore, implementations of this method are responsible for
     * appropriately handling property identity.
     *
     * Any debug or tracing logging is expected to be done by this functino.
     *
     * @param property The currently-visited property
     * @param accumulator The current state of the accumulator
     * @return The new property
     */
    context(StrategyLogger<PropertyT>)
    protected open fun visitProperty(property: Property<PropertyT>, accumulator: AccumulatorT): Property<PropertyT> =
        property.map { visit(it, accumulator) }

    /**
     * This function performs a single traversal step. It takes the root element,
     * and then modifies each item in the "items" navigation list. Since this
     * loop is done in one line using the [Every.modify] function, the "confusing
     * structure" of the traversal is hidden, and it seems like were just modifying
     * single instances of [ItemT] in this function.
     *
     * For every such instance of [ItemT], we will first make a copy of the [accumulator],
     * to ensure there is no "pollution" between branches (i.e., the [accumulator] works
     * depth-first, and is not re-used between branches). Then, we visit the specified
     * property (if any), and store some debugging information. We then recursively call
     * this function for every extra traversal step to take.
     *
     * @param root The current item to visit items in
     * @param step The remaining traversal to perform
     * @param accumulator The current state of the accumulator
     * @return [RootT] The modified/updated instance after traversal
     */
    context(ConfigurationLogger)
    private fun <RootT, ItemT> executeStep(
        root: RootT,
        step: TraversalStep<RootT, ItemT, PropertyT>,
        accumulator: AccumulatorT,
    ): RootT = step.items.modify(root) { itemProperty ->
        // We instantiate a specialised logging interface that will
        // "capture" the current execution context (entity and accumulator).
        // This allows the strategy implementation to only provide string
        // values for the logs, which will be amended with the context
        // and put in the configuration logs.
        // NOTE: We have to explicitly specify the type parameters here, as
        // otherwise, PropertyT becomes Any and then the visitProperty call
        // further down fails - but the IDE and compiler don't realise this
        // so the result is a compilation crash.
        RelayingStrategyLogger.withContext<ItemT, PropertyT, AccumulatorT>(itemProperty, accumulator) {
            // Since `it` is read-only, we create a writable copy first
            var item = it
            // We fork the accumulator here to visit the current level and all children
            val currentAccumulator = accumulator.fork()

            // If we have a property to visit, we do so: we retrieve the current value,
            // call the implementation function [visitProperty], and then store the new value
            if(step.property != null) {
                // If the lens has no focus - i.e. it navigates some entities that are null,
                // so there is no target - we skip as well. Note that this is different from
                // a nullable target itself being null, which is a valid scenario that we do
                // visit.
                val focus = step.property.getOrModify(item)

                focus.onRight { property ->
                    item = step.property.set(item, visitProperty(property, currentAccumulator))
                }

                focus.onLeft {
                    strategyDebug("Did not visit any property since the lens had no focus")
                }
            } else {
                strategyDebug("Did not visit any property")
            }

            // We recursively perform all next steps, we use a fold to simplify the code
            // Note that each recursive step will itself fork the accumulator, so it
            // is safe to pass the current instance here without forking here.
            item = step.nextSteps.fold(item) { accItem, nextStep -> executeStep(accItem, nextStep, currentAccumulator) }

            // We merge the accumulator with the parent after all modifications at this level
            accumulator.merge(currentAccumulator)

            return@withContext item
        }
    }

    /**
     * This function performs a full traversal of the current strategy.
     *
     * @param root The root element to start traversal on
     * @param accumulator The initial state of the accumulator
     */
    context(ConfigurationLogger)
    protected fun execute(root: Property<RootT>, accumulator: AccumulatorT): Property<RootT> {
        configDebug(this@Strategy::class, "Starting to apply traversal with strategy $strategyName")

        val result = executeStep(root, traversal, accumulator)

        configDebug(this@Strategy::class, "Finished applying traversal with strategy $strategyName")
        return result
    }

    /**
     * Instances of this object are used to keep track of the state during a traversal.
     * They should be implemented for each strategy to keep track of the right data for
     * that strategy.
     *
     * We specify two lifecycle functions - fork and merge, which are called before and
     * after (recursively) visiting a child, respectively.
     *
     * @param T The Accumulator implementation type
     * @param P The type of properties the strategy will visit
     */
    abstract class Accumulator<T : Accumulator<T, P>, P> {
        /**
         * This function will be called for every child that is visited. This allows
         * the strategy to prevent re-use of accumulators between branches - for example
         * for the simple merging strategy. This can be done by returning a copy of
         * the current instance. The returned value will be used in child levels.
         *
         * @return The accumulator to pass to the child visit
         */
        abstract fun fork(): T

        /**
         * This function will be called after every child visit and allows for
         * merging the accumulator state at the end of the child visit with the
         * state of the parent level. This allows the strategy to accumulate
         * information back to the traversal root - for example, for validation.
         *
         * @param child The accumulator state returned by the child visit
         */
        abstract fun merge(child: T)
    }
}
