package nl.rug.digitallab.configuration.exceptions

import com.networknt.schema.ValidationMessage

/**
 * Exception documenting that schema validation errors occurred
 * when validating an entity.
 */
class SchemaValidationException (
    val validationResult: Set<ValidationMessage>,
) : Exception()