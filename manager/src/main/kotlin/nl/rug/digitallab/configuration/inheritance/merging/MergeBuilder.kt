package nl.rug.digitallab.configuration.inheritance.merging

import arrow.optics.Lens
import nl.rug.digitallab.configuration.Cloning.clone
import nl.rug.digitallab.configuration.helpers.get
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.doMergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.MergeBuilder.Companion.mergeByProperty
import nl.rug.digitallab.configuration.inheritance.merging.scopes.MergeScope
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.inheritance.merging.scopes.NoLogsMergeScope.Companion.muted
import nl.rug.digitallab.configuration.models.Property
import kotlin.experimental.ExperimentalTypeInference

/**
 * Builder API to simplify implementing a [Merger] on a data type
 * by facilitating merging on a per-property basis.
 *
 * This Builder API offers a variety of helper methods to easily
 * and nearly declaratively implement merging behaviour for data
 * classes with Optics enabled. The Builder is initiated through
 * the [mergeByProperty] or [doMergeByProperty] functions, after
 * which a [Merger] can be written in the following style:
 * ```kotlin
 * @optics
 * data class Foo(val first: String, val second: String?) { ... }
 *
 * val merger: Merger<Foo> = mergeByProperty {
 *     Foo.first use inherit
 *     Foo.second merge overrideOrInherit()
 * }
 * ```
 *
 * The builder will first create a "base" instance on top of which
 * the modifications are applied. By default, this is the "current"
 * value from the [MergeScope], but it can be any instance of the
 * target type.
 *
 * @receiver [MergeScope] The current merging scope in which the
 *              merge should take place.
 * @param initial Producer of the "base" instance for merging
 * @param explanation Optional explanation text that might be used for
 *              [set] or [use] to give context to the tracing logs.
 */
context(MergeScope<ItemT>)
@OptIn(ExperimentalTypeInference::class)
class MergeBuilder<ItemT> private constructor(
    initial: MergeScope<ItemT>.() -> ItemT,
    val explanation: String? = null,
) {
    // Intermediate result variable, first initialised to the specified initial value
    private var mergedResult = initial(this@MergeScope)

    /**
     * Helper method to apply the given [merger] to the inherited and current
     * instances of type [P] pointed to by [lens].
     *
     * **NOTE**: Please do not use this method directly, and rather use [set],
     * [use], or [merge] instead. These will aid readability of the merge code.
     *
     * @param lens Lens to the property to apply the [merger] to
     * @param merger The merging logic to apply to the given property
     */
    fun <P> onProperty(lens: Lens<ItemT, Property<P>>, merger: Merger<P>) {
        val mergedProperty = scopeMerge(inherited[lens], current[lens], merger)
        mergedResult = lens.modify(mergedResult) { it.set(mergedProperty) }
    }

    /**
     * Assigns the provided value to the selected property
     *
     * @param value The value to assign to the selected property
     */
    infix fun <P> Lens<ItemT, Property<P>>.set(value: P) {
        onProperty(this) {
            traceTransform(explanation ?: "Changed value to $value")
            return@onProperty value
        }
    }

    /**
     * Uses the given model instance (of type [ItemT]) as source
     * for the selected property. The selected property will be
     * read from the given model instance and then set to the
     * merged result.
     *
     * *NOTE*: It is required to pass either `current` or `inherited`
     * as the instance of [ItemT]. Other instances are not
     * supported.
     *
     * This method will create appropriate tracing log entries.
     * In case the instance to be used is `inherited`, then the
     * value read from the instance will be cloned, during which
     * appropriate cloning tracing entries will be created in the log.
     *
     * @param value Instance of the model, either `current` or `inherited`
     */
    inline infix fun <reified P> Lens<ItemT, Property<P>>.use(value: ItemT) {
        val isCurrent = value == current
        val isInherited = value == inherited

        require(isCurrent || isInherited) { "The 'use' method requires passing either the current or the inherited instance" }

        onProperty(this) {
            if(isCurrent) {
                traceOverride(explanation ?: "Used current value")
                return@onProperty current
            } else {
                traceInherit(explanation ?: "Inherited value")
                return@onProperty inherited.clone()
            }
        }
    }

    /**
     * Uses the given [Merger] implementation to "choose" between the
     * inherited and current instances of the selected property.
     * The selected property will be read from both inherited and
     * current, and will be passed to [merger]. The result
     * will then be set on the merged result.
     *
     * This method notably differs from [merge]: this method suppresses
     * any merge tracing entries, since we do not consider the [merger]
     * to be a new "scope". Therefore, [merger] should not be a complex
     * method; it should only be a decision function between the current
     * and inherited values.
     *
     * @param merger The decision function for [P] instances
     */
    infix fun <P> Lens<ItemT, Property<P>>.use(merger: Merger<P>) = onProperty(this, merger.muted())

    /**
     * Uses the given [Merger] implementation to merge the
     * inherited and current instances of the selected property.
     * The chosen property will be read from both inherited and
     * current, and will be passed to [merger]. The result
     * will then be set on the merged result.
     *
     * @param merger The merge function for [P] instances
     */
    infix fun <P> Lens<ItemT, Property<P>>.merge(merger: Merger<P>) = onProperty(this, merger)

    /**
     * Use the given string as an explanation for the actions taken
     * inside [builder]. This can provide extra context in the trace
     * logs, to aid debugging and analysing the configuration.
     *
     * This context is only used for [set] and [use] merge actions,
     * since [merge] actions are supposed to carry proper logging
     * already.
     *
     * @receiver The [String] with explanation
     * @param builder The merge actions to apply with this explanation
     */
    fun String.asExplanation(@BuilderInference builder: context(MergeBuilder<ItemT>) MergeScope<ItemT>.() -> Unit) {
        // We create a new builder with the explanation as scope. We pass
        // the current (intermediate) merged result as the initial value
        // of this new builder, and use its result to continue work in
        // the parent builder.
        val mergeBuilder = MergeBuilder({ mergedResult }, this)
        builder(mergeBuilder, this@MergeScope)
        mergedResult = mergeBuilder.mergedResult
    }

    companion object {
        /**
         * Initiates a merge builder context to produce a [Merger] with the
         * following behaviour:
         *
         * Given an initial [ItemT] instance, produced by [initial], the operations
         * inside [builder] will be applied in order, and the "merged result" will
         * be returned as the result of the [Merger].
         *
         * By default, the initial value is taken to be the "current" value from the
         * [MergeScope] passed to the [Merger].
         *
         * This method will write a merge tracing entry for the current item,
         * with a generic message indicating that a property-wise merge has taken
         * place.
         *
         * @see [MergeBuilder]
         *
         * @param initial Producer of an initial value
         * @param builder Merge operations to perform
         * @return [Merger] implementation for [ItemT]
         */
        fun <ItemT> mergeByProperty(
            @BuilderInference initial: MergeScope<ItemT>.() -> ItemT = { current },
            @BuilderInference builder: context(MergeBuilder<ItemT>) MergeScope<ItemT>.() -> Unit,
        ): Merger<ItemT> = merge@{
            val mergeBuilder = MergeBuilder(initial)
            builder(mergeBuilder, this)
            traceMerge("Merged inherited and current items by their properties")
            return@merge mergeBuilder.mergedResult
        }

        /**
         * Initiates a merge builder context to produce _and run_ a [Merger] in the
         * current [MergeScope] with the following behaviour:
         *
         * Given an initial [ItemT] instance, produced by [initial], the operations
         * inside [builder] will be applied in order, and the "merged result" will
         * be returned as the result of the [Merger].
         *
         * By default, the initial value is taken to be the "current" value from the
         * [MergeScope] passed to the [Merger].
         *
         * This method will write a merge tracing entry for the current item,
         * with a generic message indicating that a property-wise merge has taken
         * place.
         *
         * @see [MergeBuilder]
         *
         * @receiver The current [MergeScope] to perform the merge in
         * @param initial Producer of an initial value
         * @param builder Merge operations to perform
         * @return Resulting [ItemT] after merge
         */
        fun <ItemT : Any> MergeScope<ItemT>.doMergeByProperty(
            @BuilderInference initial: MergeScope<ItemT>.() -> ItemT = { current },
            @BuilderInference builder: context(MergeBuilder<ItemT>) MergeScope<ItemT>.() -> Unit,
        ): ItemT = mergeByProperty(initial, builder)()

        /**
         * Initiates a merge builder context to produce _and run_ a [Merger] in the
         * current [MergeScope] with the following behaviour:
         *
         * Given an initial [ItemT] instance, produced by [initial], the operations
         * inside [builder] will be applied in order, and the "merged result" will
         * be returned as the result of the [Merger].
         *
         * By default, the initial value is taken to be the "current" value from the
         * [MergeScope] passed to the [Merger].
         *
         * This method will write a merge tracing entry for the current item,
         * with a generic message indicating that a property-wise merge has taken
         * place.
         *
         * @see [MergeBuilder]
         *
         * @receiver The current [MergeScope] to perform the merge in
         * @param initial Producer of an initial value
         * @param builder Merge operations to perform
         * @return Resulting [ItemT] after merge
         */
        @JvmName("doMergeByNullableProperty")
        fun <ItemT : Any> MergeScope<ItemT?>.doMergeByProperty(
            @BuilderInference initial: MergeScope<ItemT>.() -> ItemT = { current },
            @BuilderInference builder: context(MergeBuilder<ItemT>) MergeScope<ItemT>.() -> Unit,
        ): ItemT? {
            if(inherited == null || current == null) return null

            val merger = mergeByProperty(initial, builder)
            return mergeValues(inherited, current, merger)
        }
    }
}
