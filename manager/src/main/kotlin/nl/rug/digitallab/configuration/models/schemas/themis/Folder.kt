package nl.rug.digitallab.configuration.models.schemas.themis

import arrow.optics.optics
import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.OptBoolean
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.inheritance.ITraversable
import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.ListProperty
import nl.rug.digitallab.configuration.models.MapProperty
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import nl.rug.digitallab.configuration.models.schemas.Visibility
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Profile
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Test
import java.time.Instant

@optics
@JsonSchema("./themis/folder.schema.json")
data class Folder (
    // Required property
    val name: Property<String>,

    // For these, null means to inherit the property, while an explicit
    // value (also empty string) overrides the inherited value.
    @JsonProperty(defaultValue = "null") val visibility: Property<Visibility?>,
    @JsonProperty(defaultValue = "null") val softDeadline: Property<Instant?>,
    @JsonProperty(defaultValue = "null") val hardDeadline: Property<Instant?>,

    // Profiles and tests have no distinction between emptiness and
    // absence - the inherited set will always be expanded upon.
    @JsonProperty(defaultValue = "{}") val profiles: MapProperty<Profile>,
    @JsonProperty(defaultValue = "{}") val tests: MapProperty<Test>,

    // The slug is implicitly determined from the directory name,
    // and is provided using Jackson "injection"
    @JacksonInject(value = "directory", useInput = OptBoolean.FALSE)
    val slug: Property<String>,

    // These are filled outside the parsing process
    @JsonIgnore val assignments: ListProperty<Assignment> = Property(emptyList()),
    @JsonIgnore val folders: ListProperty<Folder> = Property(emptyList()),

    @JsonIgnore
    override val path: Property<EntityPath> = EntityPath(slug()).toProp(),
) : ITraversable {
    override fun toNodeString(): String = "Folder \"$name\""

    companion object
}
