package nl.rug.digitallab.configuration.logging.entries

import nl.rug.digitallab.configuration.logging.Logger

/**
 * Simple [LogEntry] capturing information about an exception
 * that occurred during configuration processing.
 */
data class ExceptionLogEntry<LocationT> (
    override val type: Logger.Type,
    override val location: LocationT,
    val exception: Exception,
    override val message: String = exception.message.orEmpty(),
) : LogEntry<LocationT>
