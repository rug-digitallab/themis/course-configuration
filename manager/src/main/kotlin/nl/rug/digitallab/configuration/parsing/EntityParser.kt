package nl.rug.digitallab.configuration.parsing

import com.fasterxml.jackson.databind.InjectableValues
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.networknt.schema.JsonSchema
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.SchemaValidatorsConfig
import com.networknt.schema.SpecVersionDetector
import com.networknt.schema.ValidationMessage
import jakarta.inject.Inject
import jakarta.inject.Singleton
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.configuration.config.ResourceMappingConfig
import nl.rug.digitallab.configuration.exceptions.NoSchemaException
import nl.rug.digitallab.configuration.exceptions.SchemaValidationException
import nl.rug.digitallab.configuration.jackson.SchemaFactoryCustomizer
import nl.rug.digitallab.configuration.jackson.addValue
import nl.rug.digitallab.configuration.logging.deserialization.DeserializationLogger
import nl.rug.digitallab.configuration.models.Property.Companion.toProp
import java.io.FileNotFoundException
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.name
import kotlin.io.path.pathString
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import nl.rug.digitallab.configuration.annotations.JsonSchema as JsonSchemaAnnotation

/**
 * Class handling the parsing and validation of JSON/YAML documents into
 * appropriate Kotlin/Java types and models.
 *
 * Given a filename and target type, this class will find the schema
 * belonging to the target type, validate the file against this schema,
 * and deserialize the file into the target type.
 *
 * *Note* that this class is injectable through Jakarta
 */
@Singleton
class EntityParser {
    @Inject
    private lateinit var yamlMapper: YAMLMapper

    @Inject
    private lateinit var schemaFactoryCustomizer: SchemaFactoryCustomizer

    @Inject
    private lateinit var schemaValidatorsConfig: SchemaValidatorsConfig

    @Inject
    private lateinit var resourceMapping: ResourceMappingConfig

    // We use a schema cache to prevent reopening and parsing
    // the schemas over and over.
    private val schemaCache: MutableMap<Path, JsonSchema> = mutableMapOf()

    /**
     * Given the path of a schema file, obtain a parsed [JsonSchema]
     * instance. This instance is retrieved either from cache,
     * or built on the fly.
     *
     * @param file The schema file
     */
    private fun schemaFromFile(file: Path): JsonSchema =
        schemaCache.getOrElse(file) {
            val schemaNode = yamlMapper.readTree(file.toFile())
            JsonSchemaFactory
                // We need to autodetect the metaschema version
                .getInstance(SpecVersionDetector.detect(schemaNode), schemaFactoryCustomizer)
                // and then we can obtain a schema instance
                .getSchema(schemaNode, schemaValidatorsConfig)
                // Finally, we store it in the cache
                .also { schemaCache[file] = it }
        }

    /**
     * Perform schema validation on the given JSON node and target type.
     * This function will retrieve the schema associated with the target type
     * as given through the [JsonSchemaAnnotation], and perform validation.
     *
     * @param node The JSON node/tree to validate
     * @param type The target type
     */
    private fun <T: Any> validateNode(node: JsonNode, type: KClass<T>): Set<ValidationMessage> {
        val schemaAnnotation = type.findAnnotation<JsonSchemaAnnotation>()
            ?: throw NoSchemaException("Type ${type.qualifiedName}")

        val schemaPath = getResource(resourceMapping.schemas).asPath().resolve(schemaAnnotation.ref)
        if(!schemaPath.exists())
            throw NoSchemaException(schemaPath.pathString)

        val schema = schemaFromFile(schemaPath)
        return schema.validate(node)
    }

    /**
     * Validates and then parses the given file into the target entity
     * of type [T]. The type [T] needs to be annotated with
     * [JsonSchemaAnnotation] to define the schema to use for validation.
     *
     * @param file The input file to validate and parse
     * @param T The target entity type
     */
    context(DeserializationLogger)
    inline fun <reified T : Any> parseAndValidateFile(file: Path): T
        = parseAndValidateFile(file, T::class)

    /**
     * Validates and then parses the given file into the target entity
     * of type [T]. The type [T] needs to be annotated with
     * [JsonSchemaAnnotation] to define the schema to use for validation.
     *
     * @param file The input file to validate and parse
     * @param type The target entity type
     */
    context(DeserializationLogger)
    fun <T : Any> parseAndValidateFile(file: Path, type: KClass<T>): T {
        if(!file.exists())
            throw FileNotFoundException(file.pathString)

        // We read from the JSON file in two steps: first, we construct the
        // JSON Tree for validation purposes, after which we will convert the
        // tree to an instance of our target entity type.
        val jsonNode = yamlMapper.readTree(file.toFile())
        val validationResult = validateNode(jsonNode, type)

        // We probably want to revise error handling at a later stage
        // depending on how this code is integrated to the CLI and the
        // web service.
        if (validationResult.isNotEmpty())
            throw SchemaValidationException(validationResult)

        // We prepare the "injection" metadata for Jackson, this allows
        // schemas to consume information about the parsing context.
        // For now, we only provide the directory name of the file being
        // parsed.
        yamlMapper.injectableValues = InjectableValues.Std(mapOf(
            "directory" to file.parent.name.toProp(),
            "file" to file.name.toProp(),
        )).addValue<DeserializationLogger>(this@DeserializationLogger)

        // And at last, we parse the tree into the target type
        return yamlMapper.readValue(file.toFile(), type.java)
    }
}
