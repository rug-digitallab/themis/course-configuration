package nl.rug.digitallab.configuration.inheritance

import nl.rug.digitallab.configuration.models.EntityPath
import nl.rug.digitallab.configuration.models.Property

/**
 * Interface representing traversable entities.
 *
 * Any entity to be used in a [Traversal] should implement this
 * interface. It provides a function used for logging/tracing
 * purposes, but most importantly it provides an internal
 * [path] property, allowing entities to be uniquely identified
 * by a human-readable path.
 */
interface ITraversable {
    val path: Property<EntityPath>
    fun toNodeString(): String
}
