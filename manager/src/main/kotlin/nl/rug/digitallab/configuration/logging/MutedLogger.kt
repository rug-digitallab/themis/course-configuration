package nl.rug.digitallab.configuration.logging

import nl.rug.digitallab.configuration.logging.entries.LogEntry

/**
 * Simple logger implementation that simply throws away all log entries,
 * used for noisy code sections for which detailed logging is not useful,
 * or for which the normal logs might not be correct.
 */
class MutedLogger : ConfigurationLogger {
    override fun log(entry: LogEntry<*>) { /* no-op */ }
}
