package nl.rug.digitallab.configuration.jackson.deserializers

import com.fasterxml.jackson.databind.BeanDescription
import com.fasterxml.jackson.databind.DeserializationConfig
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.addDeserializer
import com.fasterxml.jackson.module.kotlin.addSerializer
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.configuration.helpers.ReferenceOr
import nl.rug.digitallab.configuration.jackson.serializers.PropertySerializer
import nl.rug.digitallab.configuration.models.Property
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.Action
import nl.rug.digitallab.configuration.models.schemas.themis.runtime.RuntimeFile
import java.time.Instant

/**
 * Simple module defining all custom deserializers we are using
 * in this configuration project. Implemented as an object so there
 * is only one instance natively, simplifying usage.
 */
object DigitalLabDeserializers : SimpleModule() {
    init {
        addDeserializer(ReferenceOr::class, ReferenceOrDeserializer())
        addDeserializer(Property::class, PropertyDeserializer())
        addSerializer(Property::class, PropertySerializer())

        setDeserializerModifier(Modifier)
    }

    /**
     * Some of the deserializers simply wrap an existing deserializer to adapt
     * and modify their behaviour. These can only be registered through a
     * BeanDeserializerModifier that will wrap the default deserializer as
     * desired here.
     */
    object Modifier : BeanDeserializerModifier() {
        override fun modifyDeserializer(
            config: DeserializationConfig,
            beanDesc: BeanDescription,
            deserializer: JsonDeserializer<*>
        ): JsonDeserializer<*> = when(beanDesc.beanClass) {
            Action.Output::class.java   -> ActionOutputDeserializer(deserializer)
            ByteSize::class.java        -> ByteSizeDeserializer(deserializer)
            RuntimeFile::class.java     -> FileDeserializer(deserializer)
            Instant::class.java         -> InstantDeserializer(deserializer)
            else -> super.modifyDeserializer(config, beanDesc, deserializer)
        }
    }
}
