package nl.rug.digitallab.configuration.inheritance.strategies

import nl.rug.digitallab.configuration.inheritance.Traversal
import nl.rug.digitallab.configuration.inheritance.merging.scopes.Merger
import nl.rug.digitallab.configuration.inheritance.merging.MergingEnvironment
import nl.rug.digitallab.configuration.inheritance.merging.overrideOrInherit
import nl.rug.digitallab.configuration.inheritance.strategies.MergingStrategy.Companion.create
import nl.rug.digitallab.configuration.logging.ConfigurationLogger
import nl.rug.digitallab.configuration.logging.strategies.StrategyLogger
import nl.rug.digitallab.configuration.logging.merging.RelayingMergeScope
import nl.rug.digitallab.configuration.models.Property
import kotlin.experimental.ExperimentalTypeInference

/**
 * Strategy implementation for merging property values. Given
 * a merging function, this strategy will combine inherited
 * and current property values by merging them into a new value,
 * which will then be passed on further down the traversal.
 *
 * Typical usage will not implement this abstract class directly,
 * but will make use of the [create] or [withMerge] helper functions.
 *
 * @param PropertyT The type of values that will be visited and merged
 *                 in this strategy.
 */
abstract class MergingStrategy<RootT, PropertyT> : Strategy<RootT, PropertyT, MergingStrategy.Accumulator<PropertyT>>() {
    override val strategyName get() = MergingStrategy::class.simpleName.orEmpty()

    /**
     * Merge function to be used for combining inherited and current values
     */
    protected abstract val merger: Merger<PropertyT>

    context(StrategyLogger<PropertyT>)
    override fun visitProperty(property: Property<PropertyT>, accumulator: Accumulator<PropertyT>): Property<PropertyT> {
        // If no property is set yet, we will take this as the initial element in the merge
        if(accumulator.property == null) {
            accumulator.property = property
            return property
        }

        // We call the provided merger with a [RelayingMergeScope], which relays any tracing log
        // entries to the [StrategyLogger] we have to our disposal in this function. This merge scope
        // will capture the inherited and current properties, making them available to the merger.
        val mergeResult = RelayingMergeScope.withTracing(accumulator.property!!, property, merger)
        val mergedProperty = property.set(mergeResult)

        accumulator.property = mergedProperty

        // We do not log any tracing entries here - that is the responsibility of the merge function,
        // here we only put a debug log entry
        strategyDebug("Merged values into $mergeResult")

        return mergedProperty
    }

    /**
     * Entrypoint to start running the [MergingStrategy]. For a given
     * [target], it will run the traversal and merge the values at each
     * visited property along the way. The first visited property is used
     * as the initial value in the merge, so it will always remain untouched.
     *
     * @param target The root element to start the traversal from
     * @return The modified root element after merging
     */
    context(ConfigurationLogger)
    fun apply(target: Property<RootT>): Property<RootT> {
        return execute(target, Accumulator(null))
    }

    /**
     * Simple [Strategy.Accumulator] implementation, keeping track of
     * the current value during the merging process.
     */
    data class Accumulator<PropertyT>(
        var property: Property<PropertyT>?,
    ) : Strategy.Accumulator<Accumulator<PropertyT>, PropertyT>() {
        override fun fork(): Accumulator<PropertyT> = copy()
        override fun merge(child: Accumulator<PropertyT>) { /* no-op */ }
    }

    companion object {
        /**
         * Helper function that will create an instance of a [MergingStrategy]
         * with the specified merge function and traversal. It is the recommended
         * starting point for simple [MergingStrategy] usages.
         *
         * @param mergeFunction The function to merge inherited and current property values,
         *                      the first parameter is the inherited value.
         * @param traversal The traversal to do merging on
         * @return A [MergingStrategy] instance
         */
        context(MergingEnvironment)
        fun <RootT, PropertyT> create(
            mergeFunction: Merger<PropertyT>,
            traversal: Traversal<RootT, PropertyT>,
        ): MergingStrategy<RootT, PropertyT> = object : MergingStrategy<RootT, PropertyT>() {
            override val traversal = traversal
            override val merger = mergeFunction
        }
    }
}

/**
 * Creates an instance of [MergingStrategy] with the current
 * [Traversal] and the specified merge function.
 *
 * @param mergerGenerator A function that provides a merger instance
 *                        to use for this strategy.
 * @return A [MergingStrategy] instance
 */
@OptIn(ExperimentalTypeInference::class)
@OverloadResolutionByLambdaReturnType
fun <RootT, PropertyT> Traversal<RootT, PropertyT>.withMerge(
    mergerGenerator: context(MergingEnvironment) () -> Merger<PropertyT>,
) = with(MergingEnvironment) {
    create(mergerGenerator(this), this@withMerge)
}

/**
 * Created an instance of [MergingStrategy] with the current
 * [Traversal] that will only perform inherits or overrides.
 * The decision of inheriting or overriding is made by the
 * [shouldOverride] decision function.
 *
 * The behaviour is identical to the [overrideOrInherit]
 * merge function.
 *
 * @param overrideReason Message explaining why the inherited value
 *      was overridden. This message will follow the word "because".
 * @param inheritReason Message explaining why the inherited value
 *      was chosen. This message will follow the word "because".
 * @param shouldOverride Decision function deciding whether the
 *      given "current" value should override any inherited value.
 * @return A [MergingStrategy] instance
 */
inline fun <RootT, reified PropertyT> Traversal<RootT, PropertyT>.withOverride(
    overrideReason: String,
    inheritReason: String,
    crossinline shouldOverride: (PropertyT) -> Boolean,
) = with(MergingEnvironment) {
    create(overrideOrInherit(overrideReason, inheritReason, shouldOverride), this@withOverride)
}
