package nl.rug.digitallab.configuration.models.schemas.grading

import com.fasterxml.jackson.annotation.JsonProperty
import nl.rug.digitallab.configuration.annotations.JsonSchema
import nl.rug.digitallab.configuration.models.ListProperty

@JsonSchema("./grading/course.schema.json")
data class GradingCourse (
    @JsonProperty(defaultValue = "[]")
    val rubrics: ListProperty<Rubric>,
)
