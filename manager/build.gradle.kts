plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
    id("com.google.devtools.ksp")
}

repositories {
    maven("https://oss.sonatype.org/content/repositories/snapshots")
}

dependencies {
    val jsonSchemaValidatorVersion: String by project
    val arrowVersion: String by project
    val commonKotlinVersion: String by project
    val pprintVersion: String by project
    val commonsIoVersion: String by project
    val commonQuarkusVersion: String by project

    implementation("commons-io:commons-io:$commonsIoVersion")
    implementation("nl.rug.digitallab.common.quarkus:jackson:$commonQuarkusVersion")

    implementation("io.quarkus:quarkus-jackson")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation(platform("io.arrow-kt:arrow-stack:$arrowVersion"))

    implementation("com.networknt:json-schema-validator:$jsonSchemaValidatorVersion")

    implementation("io.arrow-kt:arrow-core")
    implementation("io.arrow-kt:arrow-functions")
    implementation("io.arrow-kt:arrow-optics")
    ksp("io.arrow-kt:arrow-optics-ksp-plugin:$arrowVersion")

    testImplementation("io.quarkus:quarkus-jacoco")
    testImplementation("nl.rug.digitallab.common.kotlin:approval-tests:$commonKotlinVersion")

    // Pretty printing library for use in approval testing
    testImplementation("io.exoquery:pprint-kotlin:$pprintVersion")
}

// The particular dereferencing feature from SnakeYAML is only available
// in the latest snapshot. We force this version to be used, since Jackson
// specifies an older version.
configurations.all {
    resolutionStrategy {
        force("org.yaml:snakeyaml:2.4")
    }
}


// We need to inform the IDE about the generated sources from our KSP plugin.
// The KSP plugin is used to autogenerate helper functions for the Arrow Optics
// library.
kotlin {
    sourceSets.main {
        kotlin.srcDir("build/generated/ksp/main/kotlin")
    }

    sourceSets.test {
        kotlin.srcDir("build/generated/ksp/test/kotlin")
    }

    compilerOptions {
        languageVersion.set(org.jetbrains.kotlin.gradle.dsl.KotlinVersion.KOTLIN_1_9)
        apiVersion.set(org.jetbrains.kotlin.gradle.dsl.KotlinVersion.KOTLIN_1_9)
    }
}

// Sadly, Quarkus already does things with KSP, so including it ourselves results
// in a conflict with a circular task dependency. To resolve this, we break
// the circular chain at the "generateCode" tasks.
// Source: https://github.com/quarkusio/quarkus/issues/29698
project.afterEvaluate {
    getTasksByName("quarkusGenerateCode", true).forEach { task ->
        task.setDependsOn(task.dependsOn.filterIsInstance<Provider<Task>>().filter { it.get().name != "processResources" })
    }
    getTasksByName("quarkusGenerateCodeDev", true).forEach { task ->
        task.setDependsOn(task.dependsOn.filterIsInstance<Provider<Task>>().filter { it.get().name != "processResources" })
    }
}
