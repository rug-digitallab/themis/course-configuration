{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://digitallab.rug.nl/themis/runtime/limits.schema.json",
    "title": "Runtime Limits configuration",
    "description": "Set of limits imposed at runtime on a single action in a specific environment. Details on the effect of each limit differ per environment. Themis will have default values for all these that might differ per environment, and in addition Themis will impose system-wide maxima on all these values.",

    "type": "object",
    "properties": {
        "time": {
            "description": "Limit on the amount of time the action can run. Integer values are in ms, time duration strings are also accepted. This is a limit on walltime, not a measure of cpu usage.",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited. The value 'default' will restore the environment defaults.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "oneOf": [
                {
                    "type": "integer",
                    "minimum": 0
                },
                {
                    "type": "string",
                    "format": "duration"
                },
                {
                    "type": "string",
                    "const": "default"
                }
            ]
        },
        "cores": {
            "description": "The number of cpu cores that can be used by the action, fractional numbers are possible",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited. The value 'default' will restore the environment defaults.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "oneOf": [
                {
                    "type": "number",
                    "minimum": 0.1
                },
                {
                    "type": "string",
                    "const": "default"
                }
            ]
        },
        "processes": {
            "description": "The number of processes that an action is allowed to spawn",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited. The value 'default' will restore the environment defaults.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "oneOf": [
                {
                    "type": "integer",
                    "minimum": 1
                },
                {
                    "type": "string",
                    "const": "default"
                }
            ]
        },
        "memory": {
            "description": "The maximum amount of memory that an action can use. Integer values are interpreted in KB, string values are also accepted",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited. The value 'default' will restore the environment defaults.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "oneOf": [
                {
                    "$ref": "/themis/runtime/filesize.schema.json"
                },
                {
                    "type": "string",
                    "const": "default"
                }
            ]
        },
        "disk": {
            "description": "The maximum amount of disk writes that an action can perform. Integer values are interpreted in KB, string values are also accepted.",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited. The value 'default' will restore the environment defaults.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "oneOf": [
                {
                    "$ref": "/themis/runtime/filesize.schema.json"
                },
                {
                    "type": "string",
                    "const": "default"
                }
            ]
        },
        "output": {
            "description": "The maximum amount of output an action can write to stdout and stderr. Integer values are interpreted in KB, string values are also accepted.",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited. The value 'default' will restore the environment defaults.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "oneOf": [
                {
                    "$ref": "/themis/runtime/filesize.schema.json"
                },
                {
                    "type": "string",
                    "const": "default"
                }
            ]
        }
    },

    "unevaluatedProperties": false
}
