{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://digitallab.rug.nl/themis/runtime/test.schema.json",
    "title": "Test configuration",
    "description": "Configuration for a single test of an assignment",

    "type": "object",
    "properties": {
        "note": {
            "description": "A short internal note to be displayed to teachers only, helping them understand what makes this test special.",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "type": "string"
        },
        "hint": {
            "description": "A hint to be displayed to students, helping them understand what makes this test special. The hint is not displayed when a student has not yet reached this test.",
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "type": "string"
        },
        "visibility": {
            "description": "Specifies which aspects of the test are visible to the student.",
            "inheritsFrom": "Visibility settings from ancestors will be inherited and merged with the specified values.",
            "inheritsTo": "Descendants will inherit these settings, but can override them.",

            "type": "object",
            "properties": {
                "name": {
                    "description": "Whether the test name should be visible to students.",

                    "type": "boolean",
                    "default": true
                },
                "hint": {
                    "description": "Whether the hint should be visible to students.",

                    "type": "boolean",
                    "default": true
                },
                "status": {
                    "description": "Whether the status should be visible to students.",

                    "type": "boolean",
                    "default": true
                },
                "resources": {
                    "description": "A list of test-specific resources (such as inputs, outputs, and files) that should be visible to students.",

                    "type": "array",
                    "item": {
                        "type": "string"
                    }
                },
                "limits": {
                    "description": "Whether all limits should be visible to students. When set to false (default), only 'custom' limits will be shown. When set to true, also system default limits are shown.",

                    "type": "boolean",
                    "default": false
                }
            }
        },

        "group": {
            "description": "The name of the test group this test is part of. Different test groups can represent different types of tests; all tests in a test group are always executed together.",
            
            "inheritsFrom": "When unspecified, the value from the ancestor will be inherited.",
            "inheritsTo": "Descendants will inherit this value, but can override it.",

            "type": "string",
            "default": "default"
        },
        
        "runtimeOverrides": {
            "description": "Test-specific settings to change the execution of the tests. The settings defined here will override the settings for the testing stage of the specific profile. Test-specific settings are provided per profile.",
            "inheritsFrom": "Test-specific settings from ancestors will be inherited. For specified profiles, the given test-specific settings will be merged with inherited settings. Specifying 'null' for a profile will remove its test-specific settings.",
            "inheritsTo": "Descendants will inherit these settings, but can modify them.",

            "type": "object",
            "patternProperties": {
                ".": {
                    "oneOf": [
                        { "$ref": "/themis/runtime/stage.schema.json" },
                        { "type": "null" }
                    ]
                }
            }
        }
    },

    "unevaluatedProperties": false
}
